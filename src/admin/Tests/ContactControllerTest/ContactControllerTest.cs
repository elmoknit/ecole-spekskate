﻿using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using SpekSkates.Admin.Web.Modules.Contact.Controllers;
using Shouldly;
using SpekSkates.Admin.Web.Base;
using Moq;
using SpekSkates.Admin.Web.Modules.Contact;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace SpekSkates.Admin.Tests.ContactControllerTest
{
    public class ContactControllerTest
    {
            [Fact]
            public void Contact_ShouldReturnViewResult_Index()
            {
            var repo = new Mock<IRepository<Contact>>();
                // Arrange
                var controllerUnderTest = new SpekSkates.Admin.Web.Modules.Contact.Controllers.ContactController(repo.Object);

                // Act
                var result = controllerUnderTest.Index();

                // Assert
                //result.ShouldNotBeNull();
                //result.ShouldBeOfType<ViewResult>()
                //    .ViewName.ShouldBeNull();
            }

            [Fact]
            public void Contact_ShouldReturnViewResult_Detail()
            {
                var repo = new Mock<IRepository<Contact>>();
                // Arrange
                var controllerUnderTest = new SpekSkates.Admin.Web.Modules.Contact.Controllers.ContactController(repo.Object);

                // Act
                var result = controllerUnderTest.Details(1);

                // Assert
                result.ShouldNotBeNull();
                result.ShouldBeOfType<ViewResult>()
                    .ViewName.ShouldBeNull();
        }

            [Fact]
            public void Contact_ShouldREturnViewResult_Create()
            {
                var repo = new Mock<IRepository<Contact>>();
                // Arrange
                var controllerUnderTest = new SpekSkates.Admin.Web.Modules.Contact.Controllers.ContactController(repo.Object);

                // Act
                var result = controllerUnderTest.Create();

                // Assert
                result.ShouldNotBeNull();
                result.ShouldBeOfType<ViewResult>()
                    .ViewName.ShouldBeNull();
        }

        [Fact]
        public void Contact_ShouldREturnViewResult_Delete()
        {
            var repo = new Mock<IRepository<Contact>>();
            // Arrange
            var controllerUnderTest = new SpekSkates.Admin.Web.Modules.Contact.Controllers.ContactController(repo.Object);

            // Act
            var result = controllerUnderTest.Delete(0);

            // Assert
            result.ShouldNotBeNull();
            result.ShouldBeOfType<ViewResult>()
                .ViewName.ShouldBeNull();
        }

        //Ajouter les tests necessitant IFormCollection


        [Fact]
        public void Contact_ShouldReturnNotFound()
        {
            var repo = new Mock<IRepository<Contact>>();
            // Arrange
            var controllerUnderTest = new SpekSkates.Admin.Web.Modules.Contact.Controllers.ContactController(repo.Object);

            // Act
            var result = controllerUnderTest.Edit(null);

            // Assert
            result.ShouldNotBeNull();
            result.ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Contact_ShouldReturnNoUserFound()
        {
            var repo = new Mock<IRepository<Contact>>();
            // Arrange
            var controllerUnderTest = new SpekSkates.Admin.Web.Modules.Contact.Controllers.ContactController(repo.Object);

            var contacts = new List<Contact>
            {
                new Contact
                {
                      Name = "test",
                    Content = "allo"
                }
            }.AsQueryable();

            repo.Setup(x => x.Get(It.IsAny<int>())).Returns(contacts);
            // Act
            var result = controllerUnderTest.Edit(1);

            // Assert
            //result.ShouldNotBeNull();
            //result.ShouldBeOfType<NotFoundResult>();
        }

        public void Contact_ShouldReturnNotFound_Edit()
        {
            var repo = new Mock<IRepository<Contact>>();
            // Arrange
            var controllerUnderTest = new SpekSkates.Admin.Web.Modules.Contact.Controllers.ContactController(repo.Object);

            var contacts = new List<Contact>
            {
                new Contact
                {
                    Id =3,
                      Name = "test",
                    Content = "allo"
                }
            }.AsQueryable();

            repo.Setup(x => x.Get(It.IsAny<int>())).Returns(contacts);
            // Act
            var result = controllerUnderTest.Edit(1);

            // Assert
            result.ShouldNotBeNull();
            result.ShouldBeOfType<NotFoundResult>();
        }
    }
 }



