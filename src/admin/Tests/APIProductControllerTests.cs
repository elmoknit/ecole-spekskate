﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using SpekSkates.Admin.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Newtonsoft.Json;
using SpekSkates.Admin.Web.Modules.Cms;
using SpekSkates.Admin.Web.Modules.Product;
using temp.Controllers;
using SpekSkates.Admin.Web.Modules.Package;

namespace SpekSkates.Tests
{
    public class APIProductControllerTests
    {

        [Fact]
        public void Product_GetProductDetailsShouldReturnOk()
        {
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();

            ApiDetailProduitController apiControllerProduct = new ApiDetailProduitController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                new Product {Id=1, Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }
            }.AsQueryable());

            IActionResult result = apiControllerProduct.GetDetailProduit("Produit1");
            var okResult = result as OkObjectResult;

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;
            var product = data.Value as Product;

            product.Name.ShouldBe("Produit1");
            product.Price.ShouldBe(12.50M);
            product.History.ShouldBe("Histoire");
            product.Description.ShouldBe("description");
        }

        [Fact]
        public void Product_GetProductDetailsShouldReturnNotFound()
        {
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();

            ApiDetailProduitController apiControllerProduct = new ApiDetailProduitController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
     
            }.AsQueryable());

            IActionResult result = apiControllerProduct.GetDetailProduit("prod");

            var okResult = result as NotFoundObjectResult;

            okResult.ShouldNotBeNull();

            okResult.StatusCode.ShouldBe(404);

            var json = okResult.Value as JsonResult;
            json.Value.ShouldBe("Not found");
        }


        [Fact]
        public void Product_GetProductIndexShouldReturnOk()
        {
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IRepository<Package>> mockRepositoryPackage = new Mock<IRepository<Package>>();

            ApiProductController apiControllerProduct = new ApiProductController(mockRepository.Object, mockRepositoryPackage.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                new Product {Id=1, Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }
            }.AsQueryable());

            IActionResult result = apiControllerProduct.GetPageProduit("Produit1", "1");
            var okResult = result as OkObjectResult;

            //PROBLEME, La value de la result retourne une requete au lieu d'un objet, il manque probablement un tolist() dans la fonction GetPageProduit

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;
            var products = JsonConvert.SerializeObject(data.Value);

            products.ShouldContain("Name\":\"Produit1");
            products.ShouldContain("Price\":12.50");
            products.ShouldContain("History\":\"Histoire");
            products.ShouldContain("Description\":\"description");

            //products[0].Name.ShouldBe("Produit1");
            //products[0].Price.ShouldBe(12.50M);
            //products[0].History.ShouldBe("Histoire");
            //products[0].Description.ShouldBe("description");
        }

        [Fact]
        public void Product_GetProductIndexhouldReturnNotFound()
        {
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();

            Mock<IRepository<Package>> mockRepositoryPackage = new Mock<IRepository<Package>>();

            ApiProductController apiControllerProduct = new ApiProductController(mockRepository.Object, mockRepositoryPackage.Object);
            mockRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {

            }.AsQueryable());

            IActionResult result = apiControllerProduct.GetPageProduit("prod", "1");

            var okResult = result as NotFoundObjectResult;


            //PROBLEME, La value de la result retourne une requete au lieu d'un objet, il manque probablement un tolist() dans la fonction GetPageProduit

            okResult.ShouldNotBeNull();

            okResult.StatusCode.ShouldBe(404);

            var json = okResult.Value as JsonResult;
            json.Value.ShouldBe("Not found");
        }

        [Fact]
        public void Product_GetProductCategorieIndexShouldReturnOk()
        {
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();

            Mock<IRepository<Package>> mockRepositoryPackage = new Mock<IRepository<Package>>();

            ApiProductController apiControllerProduct = new ApiProductController(mockRepository.Object, mockRepositoryPackage.Object);
            mockRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                new Product {Id=1, Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1,ProductCategory= new Admin.Web.Modules.ProductCategory.ProductCategory{ Name="allo" }, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }
            }.AsQueryable());

            IActionResult result = apiControllerProduct.GetPageProduit("allo", "1");
            var okResult = result as OkObjectResult;

            //PROBLEME, La value de la result retourne une requete au lieu d'un objet, il manque probablement un tolist() dans la fonction GetPageProduit

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;
            var products = JsonConvert.SerializeObject(data.Value);
            products.ShouldContain("Name\":\"Produit1");
            products.ShouldContain("Price\":12.50");
            products.ShouldContain("History\":\"Histoire");
            products.ShouldContain("Description\":\"description");

            //products[0].Name.ShouldBe("Produit1");
            //products[0].Price.ShouldBe(12.50M);
            //products[0].History.ShouldBe("Histoire");
            //products[0].Description.ShouldBe("description");
        }

        [Fact]
        public void Product_GetProductShapeIndexShouldReturnOk()
        {
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IRepository<Package>> mockRepositoryPackage = new Mock<IRepository<Package>>();

            ApiProductController apiControllerProduct = new ApiProductController(mockRepository.Object, mockRepositoryPackage.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                new Product {Id=1, ProductShape= new Admin.Web.Modules.ProductShape.ProductShape{Name="allo" }, Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1,ProductCategory= new Admin.Web.Modules.ProductCategory.ProductCategory{ Name="cat" }, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }
            }.AsQueryable());

            IActionResult result = apiControllerProduct.GetPageProduit("allo", "1");
            var okResult = result as OkObjectResult;

            //PROBLEME, La value de la result retourne une requete au lieu d'un objet, il manque probablement un tolist() dans la fonction GetPageProduit

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;
            //var products = data.Value as List<Product>;

            var products = JsonConvert.SerializeObject(data.Value);

            products.ShouldContain("Name\":\"Produit1");
            products.ShouldContain("Price\":12.50");
            products.ShouldContain("History\":\"Histoire");
            products.ShouldContain("Description\":\"description");

            //products[0].Name.ShouldBe("Produit1");
            //products[0].Price.ShouldBe(12.50M);
            //products[0].History.ShouldBe("Histoire");
            //products[0].Description.ShouldBe("description");
        }

        [Fact]
        public void Product_GetProductSizeIndexShouldReturnOk()
        {
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IRepository<Package>> mockRepositoryPackage = new Mock<IRepository<Package>>();

            ApiProductController apiControllerProduct = new ApiProductController(mockRepository.Object, mockRepositoryPackage.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                new Product {Id=1, ProductSize= new Admin.Web.Modules.ProductSize.ProductSize{Name="allo" }, ProductShape= new Admin.Web.Modules.ProductShape.ProductShape{Name="shape" }, Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1,ProductCategory= new Admin.Web.Modules.ProductCategory.ProductCategory{ Name="cat" }, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }
            }.AsQueryable());

            IActionResult result = apiControllerProduct.GetPageProduit("allo", "1");
            var okResult = result as OkObjectResult;

            //PROBLEME, La value de la result retourne une requete au lieu d'un objet, il manque probablement un tolist() dans la fonction GetPageProduit

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;
            //var products = data.Value as List<Product>;

            var products = JsonConvert.SerializeObject(data.Value);

            products.ShouldContain("Name\":\"Produit1");
            products.ShouldContain("Price\":12.50");
            products.ShouldContain("History\":\"Histoire");
            products.ShouldContain("Description\":\"description");

            //products[0].Name.ShouldBe("Produit1");
            //products[0].Price.ShouldBe(12.50M);
            //products[0].History.ShouldBe("Histoire");
            //products[0].Description.ShouldBe("description");
        }
    }
}
