﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using SpekSkates.Admin.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web;
using SpekSkates.Admin.Web.Modules.HomePage;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.Slider;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System.IO;
using Microsoft.AspNetCore.Mvc.Rendering;
using SpekSkates.Admin.Web.Modules.ProductCategory;
using SpekSkates.Admin.Web.Modules.ProductShape;
using SpekSkates.Admin.Web.Modules.ProductShape.Controllers;
using SpekSkates.Admin.Web.Modules.ProductShape.ViewModels;

namespace SpekSkates.Admin.Tests
{
    public class ProductShapeControllerTest
    {

        [Fact]
        public void Index_ShouldReturnViewResult()
        {
            Mock<IRepository<ProductShape>> mockRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductShapeController controllerProductShape = new ProductShapeController(mockRepository.Object, mockProductCategory.Object);
            mockRepository.Setup(q => q.GetAll()).Returns(new List<ProductShape>
            {
                new ProductShape() {
                    Name = "Cat1",
                    Id= 1
                },
                new ProductShape() {
                    Name = "Cat2",
                    Id= 2
                }

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerProductShape.Index();
            var model = (List<ProductShape>)result.Model;

            model.Count.ShouldBe(2);
            model[0].Name.ShouldBe("Cat1");
            model[0].Id.ShouldBe(1);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("views/ProductShape/index.cshtml");
        }

        [Fact]
        public void Create_ShouldReturnViewResult()
        {
            Mock<IRepository<ProductShape>> mockRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductShapeController controllerProductShape = new ProductShapeController(mockRepository.Object, mockProductCategory.Object);

            ViewResult result = (ViewResult)controllerProductShape.Create();


            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Create_ShouldCreateAProductShape()
        {
            Mock<IRepository<ProductShape>> mockRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductShapeController controllerProductShape = new ProductShapeController(mockRepository.Object, mockProductCategory.Object);
            mockRepository.Setup(q => q.GetAll()).Returns(new List<ProductShape>
            {
                new ProductShape() {
                    Name = "Cat1",
                    Id= 1
                },
                new ProductShape() {
                    Name = "Cat2",
                    Id= 2
                }

            }.AsQueryable());

            ProductShape productShape = new ProductShape()
            {
                Name = "Cat3",
                Id = 3
            };
            var mv = new ViewModel
            {
                ProductShape = productShape
            };

            IActionResult result = controllerProductShape.Create(mv);

            mockRepository.Verify(d => d.Save(It.IsAny<ProductShape>()), Times.Once());

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Edit_ShouldReturnViewResult()
        {
            Mock<IRepository<ProductShape>> mockRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductShapeController controllerProductShape = new ProductShapeController(mockRepository.Object, mockProductCategory.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductShape>
            {
                new ProductShape() {
                    Name = "Cat1",
                    Id= 5,
                    ProductCategoriesProductShapes = new List<ProductCategoryProductShape>()
                }
            }.AsQueryable());


            ViewResult result = (ViewResult)controllerProductShape.Edit(5);

            var model = (ViewModel)result.Model;

            model.ProductShape.Name.ShouldBe("Cat1");
            model.ProductShape.Id.ShouldBe(5);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Edit_ShouldEditProductShape()
        {
            Mock<IRepository<ProductShape>> mockRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductShapeController controllerProductShape = new ProductShapeController(mockRepository.Object, mockProductCategory.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductShape>
            {
                new ProductShape() {
                    Name = "Cat1",
                    Id= 5,
                    ProductCategoriesProductShapes = new List<ProductCategoryProductShape>()
                }
            }.AsQueryable());

            ViewModel mv = new ViewModel();
            mv.ProductShape = new ProductShape()
            {
                Name = "Cat2",
                Id = 5
            };

            IActionResult result = controllerProductShape.Edit(5, mv);

            mockRepository.Verify(d => d.Update(It.IsAny<ProductShape>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();
        }


        [Fact]
        public void Edit_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<ProductShape>> mockRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductShapeController controllerProductShape = new ProductShapeController(mockRepository.Object, mockProductCategory.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductShape>
            {
                new ProductShape() {
                    Name = "Cat1",
                    Id= 5
                }
            }.AsQueryable());

            controllerProductShape.Edit(101).ShouldBeOfType<NotFoundResult>();
            controllerProductShape.Edit(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Edit_ShouldAddProductCategory()
        {
            Mock<IRepository<ProductShape>> mockRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductShapeController controllerProductShape = new ProductShapeController(mockRepository.Object, mockProductCategory.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductShape>
            {
                new ProductShape() {
                    Name = "Cat1",
                    Id= 5,
                    ProductCategoriesProductShapes = new List<ProductCategoryProductShape>()
                }
            }.AsQueryable());

            ViewModel mv = new ViewModel();
            mv.ProductShape = new ProductShape()
            {
                Name = "Cat2",
                Id = 5,
                ProductCategoriesProductShapes = new List<ProductCategoryProductShape>
                {
                    new ProductCategoryProductShape()
                    {


                    ProductCategory = new ProductCategory()
                    {
                        Id = 1,
                        Name = "Cat1",
                        CategoryType = CategoryType.Board,
                        ProductCategoriesProductSizes = new List<ProductCategoryProductSize>()
                    },
                    ProductCategoryId = 1,
                    ProductShape = new ProductShape
                    {
                        Name = "Cat5",
                        Id = 5,
                        ProductCategoriesProductShapes = new List<ProductCategoryProductShape>()
                    },
                    ProductShapeId = 5
                }
            }

            };
            

            IActionResult result = controllerProductShape.Edit(5, mv);

            mockRepository.Verify(d => d.Update(It.IsAny<ProductShape>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Delete_ShouldReturnViewResult()
        {
            Mock<IRepository<ProductShape>> mockRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductShapeController controllerProductShape = new ProductShapeController(mockRepository.Object, mockProductCategory.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductShape>
            {
                new ProductShape() {
                    Name = "Cat1",
                    Id= 5
                }
            }.AsQueryable());

            ViewResult result = (ViewResult)controllerProductShape.Delete(5);

            var model = (ProductShape)result.Model;
            model.Name.ShouldBe("Cat1");
            model.Id.ShouldBe(5);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Delete_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<ProductShape>> mockRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductShapeController controllerProductShape = new ProductShapeController(mockRepository.Object, mockProductCategory.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductShape>
            {
                new ProductShape() {
                    Name = "Cat1",
                    Id= 5
                }
            }.AsQueryable());

            controllerProductShape.Delete(101).ShouldBeOfType<NotFoundResult>();
            controllerProductShape.Delete(null).ShouldBeOfType<NotFoundResult>();
        }


        [Fact]
        public void Delete_ShouldDeleteAProductShape()
        {
            Mock<IRepository<ProductShape>> mockRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductShapeController controllerProductShape = new ProductShapeController(mockRepository.Object, mockProductCategory.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductShape>
            {
                new ProductShape() {
                    Name = "Cat1",
                    Id= 5
                }
            }.AsQueryable());


            IActionResult result = controllerProductShape.DeleteConfirmed(5);


            result.ShouldBeOfType<RedirectToActionResult>();
        }

    }
}
