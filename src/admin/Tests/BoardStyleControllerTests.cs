﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using SpekSkates.Admin.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web;
using SpekSkates.Admin.Web.Modules.HomePage;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.Slider;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System.IO;
using Microsoft.AspNetCore.Mvc.Rendering;
using SpekSkates.Admin.Web.Modules.BoardStyle;
using SpekSkates.Admin.Web.Modules.BoardStyle.Controllers;
using SpekSkates.Admin.Web.Modules.BoardStyle.ViewModels;

namespace SpekSkates.Admin.Tests
{
    public class BoardStyleControllerTest
    {

        [Fact]
        public void Index_ShouldReturnViewResult()
        {
            Mock<IRepository<BoardStyle>> mockRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Web.Modules.Product.Product>> productRepository = new Mock<IRepository<Web.Modules.Product.Product>>();

            BoardStyleController controllerBoardStyle = new BoardStyleController(mockRepository.Object, mockBlobStorage.Object, productRepository.Object);
            mockRepository.Setup(q => q.GetAll()).Returns(new List<BoardStyle>
            {
                new BoardStyle() {
                    Name = "Cat1",
                    Id= 1,
                    ImagePath = "imagePath"
                },
                new BoardStyle() {
                    Name = "Cat2",
                    Id= 2,
                    ImagePath = "imagePath"
                }

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerBoardStyle.Index();
            var model = (List<BoardStyle>)result.Model;

            model.Count.ShouldBe(2);
            model[0].Name.ShouldBe("Cat1");
            model[0].Id.ShouldBe(1);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("views/BoardStyle/index.cshtml");
        }

        [Fact]
        public void Create_ShouldReturnViewResult()
        {
            Mock<IRepository<BoardStyle>> mockRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Web.Modules.Product.Product>> productRepository = new Mock<IRepository<Web.Modules.Product.Product>>();

            BoardStyleController controllerBoardStyle = new BoardStyleController(mockRepository.Object, mockBlobStorage.Object, productRepository.Object);
            ViewResult result = (ViewResult)controllerBoardStyle.Create();


            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Create_ShouldCreateABoardStyle()
        {
            Mock<IRepository<BoardStyle>> mockRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();


            Mock<IRepository<Web.Modules.Product.Product>> productRepository = new Mock<IRepository<Web.Modules.Product.Product>>();

            BoardStyleController controllerBoardStyle = new BoardStyleController(mockRepository.Object, mockBlobStorage.Object, productRepository.Object);
            mockRepository.Setup(q => q.GetAll()).Returns(new List<BoardStyle>
            {
                new BoardStyle() {
                    Name = "Cat1",
                    Id= 1,
                    ImagePath = "imagePath"
                },
                new BoardStyle() {
                    Name = "Cat2",
                    Id= 2,
                    ImagePath = "imagePath"
                }

            }.AsQueryable());


            BoardStyle boardStyle = new BoardStyle()
            {
                Name = "Cat3",
                Id = 3,
                ImagePath = "imagePath"
            };
            var mv = new ViewModel
            {
                BoardStyle = boardStyle
            };

            IActionResult result = controllerBoardStyle.Create(mv);

            mockRepository.Verify(d => d.Save(It.IsAny<BoardStyle>()), Times.Once());

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Edit_ShouldReturnViewResult()
        {
            Mock<IRepository<BoardStyle>> mockRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();


            Mock<IRepository<Web.Modules.Product.Product>> productRepository = new Mock<IRepository<Web.Modules.Product.Product>>();

            BoardStyleController controllerBoardStyle = new BoardStyleController(mockRepository.Object, mockBlobStorage.Object, productRepository.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<BoardStyle>
            {
                new BoardStyle() {
                    Name = "Cat1",
                    Id= 5,
                    ImagePath = "img"
                }
            }.AsQueryable());
            
         
            ViewResult result = (ViewResult)controllerBoardStyle.Edit(5);

            var model = (ViewModel)result.Model;

            model.BoardStyle.Name.ShouldBe("Cat1");
            model.BoardStyle.Id.ShouldBe(5);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Edit_ShouldEditBoardStyle()
        {
            Mock<IRepository<BoardStyle>> mockRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            Mock<IRepository<Web.Modules.Product.Product>> productRepository = new Mock<IRepository<Web.Modules.Product.Product>>();

            BoardStyleController controllerBoardStyle = new BoardStyleController(mockRepository.Object, mockBlobStorage.Object, productRepository.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<BoardStyle>
            {
                new BoardStyle() {
                    Name = "Cat1",
                    Id= 5,
                    ImagePath = ""
                }
            }.AsQueryable());

            ViewModel mv = new ViewModel();
            mv.BoardStyle = new BoardStyle()
            {
                Name = "Cat2",
                Id = 5
            };
            
            IActionResult result = controllerBoardStyle.Edit(5, mv);

            mockRepository.Verify(d => d.Update(It.IsAny<BoardStyle>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        
        [Fact]
        public void Edit_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<BoardStyle>> mockRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            Mock<IRepository<Web.Modules.Product.Product>> productRepository = new Mock<IRepository<Web.Modules.Product.Product>>();

            BoardStyleController controllerBoardStyle = new BoardStyleController(mockRepository.Object, mockBlobStorage.Object, productRepository.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<BoardStyle>
            {
                new BoardStyle() {
                    Name = "Cat1",
                    Id= 5,
                    ImagePath = ""
                }
            }.AsQueryable());

            controllerBoardStyle.Edit(101).ShouldBeOfType<NotFoundResult>();
            controllerBoardStyle.Edit(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Delete_ShouldReturnViewResult()
        {
            Mock<IRepository<BoardStyle>> mockRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            Mock<IRepository<Web.Modules.Product.Product>> productRepository = new Mock<IRepository<Web.Modules.Product.Product>>();

            BoardStyleController controllerBoardStyle = new BoardStyleController(mockRepository.Object, mockBlobStorage.Object, productRepository.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<BoardStyle>
            {
                new BoardStyle() {
                    Name = "Cat1",
                    Id= 5
                }
            }.AsQueryable());

            ViewResult result = (ViewResult)controllerBoardStyle.Delete(5);

            var model = (ViewModelSupress)result.Model;
            model.BoardStyle.Name.ShouldBe("Cat1");
            model.BoardStyle.Id.ShouldBe(5);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Delete_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<BoardStyle>> mockRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Web.Modules.Product.Product>> productRepository = new Mock<IRepository<Web.Modules.Product.Product>>();

            BoardStyleController controllerBoardStyle = new BoardStyleController(mockRepository.Object, mockBlobStorage.Object, productRepository.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<BoardStyle>
            {
                new BoardStyle() {
                    Name = "Cat1",
                    Id= 5,
                    ImagePath = ""
                }
            }.AsQueryable());

            controllerBoardStyle.Delete(101).ShouldBeOfType<NotFoundResult>();
            controllerBoardStyle.Delete(null).ShouldBeOfType<NotFoundResult>();
        }


        [Fact]
        public void Delete_ShouldDeleteABoardStyle()
        {
            Mock<IRepository<BoardStyle>> mockRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();


            Mock<IRepository<Web.Modules.Product.Product>> productRepository = new Mock<IRepository<Web.Modules.Product.Product>>();

            BoardStyleController controllerBoardStyle = new BoardStyleController(mockRepository.Object, mockBlobStorage.Object, productRepository.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<BoardStyle>
            {
                new BoardStyle() {
                    Name = "Cat1",
                    Id= 5,
                    ImagePath = ""
                }
            }.AsQueryable());


            IActionResult result = controllerBoardStyle.DeleteConfirmed(5);

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void DeleteBlob_ShouldDeleteABlob()
        {
            Mock<IRepository<BoardStyle>> mockRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            Mock<IRepository<Web.Modules.Product.Product>> productRepository = new Mock<IRepository<Web.Modules.Product.Product>>();

            BoardStyleController controllerBoardStyle = new BoardStyleController(mockRepository.Object, mockBlobStorage.Object, productRepository.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<BoardStyle>
            {
                new BoardStyle() {
                    Name = "Cat1",
                    Id= 5,
                    ImagePath = ""
                }
            }.AsQueryable());


            IActionResult result = controllerBoardStyle.Deleteblob("blob", 5);
            mockBlobStorage.Verify(d => d.DeleteAsync("blob"), Times.Once);


            mockRepository.Verify(d => d.Save(It.IsAny<BoardStyle>()), Times.Once());

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void DeleteBlob_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<BoardStyle>> mockRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            Mock<IRepository<Web.Modules.Product.Product>> productRepository = new Mock<IRepository<Web.Modules.Product.Product>>();

            BoardStyleController controllerBoardStyle = new BoardStyleController(mockRepository.Object, mockBlobStorage.Object, productRepository.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<BoardStyle>
            {
                new BoardStyle() {
                    Name = "Cat1",
                    Id= 5,
                    ImagePath = ""
                }
            }.AsQueryable());

            controllerBoardStyle.Deleteblob(null, 5).ShouldBeOfType<NotFoundResult>();
            controllerBoardStyle.Deleteblob("", 5).ShouldBeOfType<NotFoundResult>();

        }

    }
}
