﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using SpekSkates.Admin.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Newtonsoft.Json;
using SpekSkates.Admin.Web.Modules.Cms;
using SpekSkates.Admin.Web.Modules.History;
using temp.Controllers;

namespace SpekSkates.Tests
{
    public class APIHistoryControllerTests
    {

        [Fact]
        public void History_GetDefaultShouldReturnOk()
        {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();

            ApiHistoryController apiControllerHistory = new ApiHistoryController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<History>
            {
                new History { Name = "History", Id = 1, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = false, isDefault= true, isDelete = false }

            }.AsQueryable());

            IActionResult result = apiControllerHistory.Default();
            var okResult = result as OkObjectResult;

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;
            var contact = data.Value as History;

            contact.Name.ShouldBe("History");
            contact.Picture1Path.ShouldBe("path1");
            contact.Picture2Path.ShouldBe("path2");
            contact.Picture3Path.ShouldBe("path3");
            contact.Text.ShouldBe("Text");

        }

        [Fact]
        public void History_GetDefaultShouldReturnNotFound()
        {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();

            ApiHistoryController apiControllerHistory = new ApiHistoryController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<History>
            {

            }.AsQueryable());

            IActionResult result = apiControllerHistory.Default();

            var okResult = result as NotFoundObjectResult;

            okResult.ShouldNotBeNull();

            okResult.StatusCode.ShouldBe(404);

            var json = okResult.Value as JsonResult;
            json.Value.ShouldBe("Not found");
        }

        [Fact]
        public void History_GetCustomShouldReturnOk()
        {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();

            ApiHistoryController apiControllerHistory = new ApiHistoryController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<History>
            {
              new History { Name = "Custom", Id = 1, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());

            IActionResult result = apiControllerHistory.Custom("Custom");

            var okResult = result as OkObjectResult;

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;
            var contact = data.Value as History;

            contact.Name.ShouldBe("Custom");
            contact.Picture1Path.ShouldBe("path1");
            contact.Picture2Path.ShouldBe("path2");
            contact.Picture3Path.ShouldBe("path3");
            contact.Text.ShouldBe("Text");


        }

        [Fact]
        public void History_GetCustomShouldReturnNotFound()
        {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();

            ApiHistoryController apiControllerHistory = new ApiHistoryController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<History>
            {

            }.AsQueryable());

            IActionResult result = apiControllerHistory.Custom("Custom123");
            var okResult = result as NotFoundObjectResult;

            okResult.ShouldNotBeNull();

            okResult.StatusCode.ShouldBe(404);

        }

        [Fact]
        public void History_GetAllPageCustomNameShouldReturnOk()
        {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();

            ApiHistoryController apiControllerHistory = new ApiHistoryController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<History>
            {
                new History { Name = "Custom1", Id = 1, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false },
                new History { Name = "Custom2", Id = 1, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());

            IActionResult result = apiControllerHistory.GetAllPageCustomName();
            var okResult = result as OkObjectResult;

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;

            var pageName = (List<string>)data.Value;
            pageName[0].ShouldBe("Custom1");
            pageName[1].ShouldBe("Custom2");

        }
    }
}
