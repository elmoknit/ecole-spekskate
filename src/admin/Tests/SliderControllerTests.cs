﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using SpekSkates.Admin.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web;
using SpekSkates.Admin.Web.Modules.HomePage;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.Slider;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System.IO;

namespace SpekSkates.Admin.Tests
{
    public class SliderControllerTests
    { 

        [Fact]
        public void Index_ShouldReturnViewResult()
        {
            Mock<IRepository<Slider>> mockRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            SliderController controllerSlider = new SliderController(mockRepository.Object, mockBlobStorage.Object);
            mockRepository.Setup(q => q.GetAll()).Returns(new List<Slider>
            {
                new Slider {
                    Name = "Slider1",
                    //HomePageId = 1,
                    Id= 1,
                    ImagePath = "imagePath1",
                    Link = "link1",
                    Text = "text1"
                },
                new Slider {
                    Name = "Slider2",
                    //HomePageId = 1,
                    Id= 2,
                    ImagePath = "imagePath2",
                    Link = "link2",
                    Text = "text2"
                }

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerSlider.Index();
            var model = (List<Slider>)result.Model;

            model.Count.ShouldBe(2);
            model[0].Name.ShouldBe("Slider1");
            model[0].ImagePath.ShouldBe("imagePath1");
            model[0].Link.ShouldBe("link1");
            model[0].Text.ShouldBe("text1");
            //model[0].HomePageId.ShouldBe(1);
   
            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("views/slider/index.cshtml");
        }

        [Fact]
        public void Create_ShouldReturnViewResult() {
            Mock<IRepository<Slider>> mockRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            SliderController controllerSlider = new SliderController(mockRepository.Object, mockBlobStorage.Object);

            ViewResult result = (ViewResult)controllerSlider.Create();


            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Create_ShouldCreateASlider() {
            Mock<IRepository<Slider>> mockRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            SliderController controllerSlider = new SliderController(mockRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<Slider>
            {
                new Slider {
                    Name = "Slider1",
                   // HomePageId = 1,
                    Id= 1,
                    ImagePath = "imagePath1",
                    Link = "link1",
                    Text = "text1"
                },
                new Slider {
                    Name = "Slider2",
                  //  HomePageId = 1,
                    Id= 2,
                    ImagePath = "imagePath2",
                    Link = "link2",
                    Text = "text2"
                }

            }.AsQueryable());

            Slider slider = new Slider {
                Name = "Slider3",
               // HomePageId = 1,
                Id = 3,
                ImagePath = "imagePath3",
                Link = "link3",
                Text = "text3"
            };
            var file = new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("This is a dummy file")), 0, 0, "Data", "dummy.txt");

            ViewModelDetail mv = new ViewModelDetail{File = file, Slider = slider};
            IActionResult result = controllerSlider.Create(mv);

            mockRepository.Verify(d => d.Update(It.IsAny<Slider>()), Times.Once());

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Edit_ShouldReturnViewResult() {
            Mock<IRepository<Slider>> mockRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            SliderController controllerSlider = new SliderController(mockRepository.Object, mockBlobStorage.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<Slider>
            {
                new Slider {
                    Name = "Slider5",
                   // HomePageId = 1,
                    Id= 5,
                    ImagePath = "imagePath5",
                    Link = "link5",
                    Text = "text5"
                }

            }.AsQueryable());
            ViewResult result = (ViewResult)controllerSlider.Edit(5);

            var model = (ViewModelDetail)result.Model;
            model.Slider.Name.ShouldBe("Slider5");
           // model.Slider.HomePageId.ShouldBe(1);
            model.Slider.ImagePath.ShouldBe("imagePath5");
            model.Slider.Link.ShouldBe("link5");
            model.Slider.Text.ShouldBe("text5");

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Edit_ShouldEditASlider() {
            Mock<IRepository<Slider>> mockRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            SliderController controllerSlider = new SliderController(mockRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(5)).Returns(new List<Slider>
            {
                new Slider {
                    Name = "Slider5",
                    //HomePageId = 1,
                    Id= 5,
                    ImagePath = "imagePath5",
                    Link = "link5",
                    Text = "text5"
                }

            }.AsQueryable());

            Slider slider = new Slider {
                Name = "Slider3",
                //HomePageId = 1,
                Id = 5,
                ImagePath = "imagePath3",
                Link = "link3",
                Text = "text3"
            };
            var file = new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("This is a dummy file")), 0, 0, "Data", "dummy.txt");

            ViewModelDetail mv = new ViewModelDetail { File = file, Slider = slider };
            IActionResult result = controllerSlider.Edit(5,mv);

            mockRepository.Verify(d => d.Update(It.IsAny<Slider>()), Times.Once());

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Edit_ShouldReturnNotFoundResult() {
            Mock<IRepository<Slider>> mockRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            SliderController controllerSlider = new SliderController(mockRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(5)).Returns(new List<Slider>
            {
                new Slider {
                    Name = "Slider5",
                    //HomePageId = 1,
                    Id= 5,
                    ImagePath = "imagePath5",
                    Link = "link5",
                    Text = "text5"
                }

            }.AsQueryable());

            controllerSlider.Edit(101).ShouldBeOfType<NotFoundResult>();
            controllerSlider.Edit(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Delete_ShouldReturnViewResult() {
            Mock<IRepository<Slider>> mockRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            SliderController controllerSlider = new SliderController(mockRepository.Object, mockBlobStorage.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<Slider>
            {
                new Slider {
                    Name = "Slider5",
                    //HomePageId = 1,
                    Id= 5,
                    ImagePath = "imagePath5",
                    Link = "link5",
                    Text = "text5"
                }

            }.AsQueryable());
            ViewResult result = (ViewResult)controllerSlider.Delete(5);

            var model = (Slider)result.Model;
            model.Name.ShouldBe("Slider5");
            //model.HomePageId.ShouldBe(1);
            model.ImagePath.ShouldBe("imagePath5");
            model.Link.ShouldBe("link5");
            model.Text.ShouldBe("text5");

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Delete_ShouldReturnNotFoundResult() {
            Mock<IRepository<Slider>> mockRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            SliderController controllerSlider = new SliderController(mockRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(5)).Returns(new List<Slider>
            {
                new Slider {
                    Name = "Slider5",
                    //HomePageId = 1,
                    Id= 5,
                    ImagePath = "imagePath5",
                    Link = "link5",
                    Text = "text5"
                }

            }.AsQueryable());

            controllerSlider.Delete(101).ShouldBeOfType<NotFoundResult>();
            controllerSlider.Delete(null).ShouldBeOfType<NotFoundResult>();
        }


        [Fact]
        public void Delete_ShouldDeleteASlider() {
            Mock<IRepository<Slider>> mockRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            SliderController controllerSlider = new SliderController(mockRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(5)).Returns(new List<Slider>
            {
                new Slider {
                    Name = "Slider5",
                    //HomePageId = 1,
                    Id= 5,
                    ImagePath = "imagePath5",
                    Link = "link5",
                    Text = "text5"
                }

            }.AsQueryable());


            IActionResult result = controllerSlider.DeleteConfirmed(5);

            mockRepository.Verify(d => d.Delete(5), Times.Once());

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void DeleteBlob_ShouldDeleteABlob() {
            Mock<IRepository<Slider>> mockRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            SliderController controllerSlider = new SliderController(mockRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(5)).Returns(new List<Slider>
            {
                new Slider {
                    Name = "Slider5",
                    //HomePageId = 1,
                    Id= 5,
                    ImagePath = "imagePath5",
                    Link = "link5",
                    Text = "text5"
                }

            }.AsQueryable());


            IActionResult result = controllerSlider.Deleteblob("blob",5);
            mockBlobStorage.Verify(d=>d.DeleteAsync("blob"), Times.Once);


            mockRepository.Verify(d => d.Save(It.IsAny<Slider>()), Times.Once());

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void DeleteBlob_ShouldReturnNotFoundResult() {
            Mock<IRepository<Slider>> mockRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            SliderController controllerSlider = new SliderController(mockRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(5)).Returns(new List<Slider>
            {
                new Slider {
                    Name = "Slider5",
                    //HomePageId = 1,
                    Id= 5,
                    ImagePath = "imagePath5",
                    Link = "link5",
                    Text = "text5"
                }

            }.AsQueryable());


            
            controllerSlider.Deleteblob(null, 5).ShouldBeOfType<NotFoundResult>();
            controllerSlider.Deleteblob("", 5).ShouldBeOfType<NotFoundResult>();

        }
    }
}
