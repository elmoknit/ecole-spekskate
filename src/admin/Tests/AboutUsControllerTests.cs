﻿using System.Collections.Generic;
using Xunit;
using Shouldly;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.About;
using SpekSkates.Admin.Web.Modules.About.Controllers;
using SpekSkates.Admin.Web.Modules.Team.Models;
using SpekSkates.Admin.Web.Modules.About.ViewModels;
using SpekSkates.Admin.Web.Modules.Team.ViewModels;

namespace SpekSkates.Tests
{
    public class AboutUsControllerTests
    {

        [Fact]
        public void Index_ShouldReturnViewResultWithAboutUsDetails()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo= new Mock<IRepository<Employee>>();

            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);
            Employee emp1 = new Employee() { Id = 1, Name = "Jacques" };
            Employee emp2 = new Employee() { Id = 2, Name = "Pierrot" };
            List<Employee> team1 = new List<Employee> { emp1, emp2 };

            mockRepository.Setup(q => q.GetAll()).Returns(new List<AboutUs>
            {
                new AboutUs { Name = "Accueil", IntroductionText = "Introd", Content = "Contenu", ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2", ImagePath3 = "ImagePath3", Team = team1, Id = 1 },
                new AboutUs { Name = "2Accueil", IntroductionText = "2Introd", Content = "2Contenu", ImagePath1 = "2ImagePath1",
                    ImagePath2 = "2ImagePath2", ImagePath3 = "2ImagePath3", Team = team1, Id = 2 },

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerAboutUs.Index();
            var model = (List<AboutUs>)result.Model;

            model.Count.ShouldBe(2);
            model[0].Name.ShouldBe("Accueil");
            model[0].IntroductionText.ShouldBe("Introd");
            model[0].Content.ShouldBe("Contenu");
            model[0].ImagePath1.ShouldBe("ImagePath1");
            model[0].ImagePath2.ShouldBe("ImagePath2");
            model[0].ImagePath3.ShouldBe("ImagePath3");
            model[0].Team[0].Name.ShouldBe(emp1.Name);
            model[0].Team[0].Id.ShouldBe(emp1.Id);
            model[0].Team[1].Name.ShouldBe(emp2.Name);
            model[0].Team[1].Id.ShouldBe(emp2.Id);
            model[0].Id.ShouldBe(1);

            model[1].Name.ShouldBe("2Accueil");
            model[1].IntroductionText.ShouldBe("2Introd");
            model[1].Content.ShouldBe("2Contenu");
            model[1].ImagePath1.ShouldBe("2ImagePath1");
            model[1].ImagePath2.ShouldBe("2ImagePath2");
            model[1].ImagePath3.ShouldBe("2ImagePath3");
            model[1].Id.ShouldBe(2);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("views/about/index.cshtml");
        }

        [Fact]
        public void Create_ShouldReturnViewResult()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();

            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            ViewResult result = controllerAboutUs.Create().ShouldBeOfType<ViewResult>();
        }

        [Fact]
        public void CreatePOST_ShouldReturnRedirectToResultResultIndex()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();

            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            ViewModelAboutUs aboutUs = new ViewModelAboutUs() { AboutUs = new AboutUs { Id = 2, Name = "APropos", Content = "Contenu" } };

            RedirectToActionResult result = controllerAboutUs.Create(aboutUs).ShouldBeOfType<RedirectToActionResult>(); ;
            result.ActionName.ShouldBe("Index");
        }

        [Fact]
        public void CreatePOST_ShouldReturnViewResultCreate()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();

            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            ViewModelAboutUs aboutUs = new ViewModelAboutUs()
            {
                AboutUs = new AboutUs
                {
                    IntroductionText = "Introd",
                    Content = "Contenu",
                    ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2",
                    ImagePath3 = "ImagePath3",
                    Id = 2
                }
            };

            var result = controllerAboutUs.Create(aboutUs).ShouldBeOfType<RedirectToActionResult>();
            result.ActionName.ShouldBe("Index");
        }

        [Fact]
        public void Edit_ShouldReturnViewModelDetails()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();


            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);
            Employee emp1 = new Employee() { Id = 1, Name = "Jacques" };
            Employee emp2 = new Employee() { Id = 2, Name = "Pierrot" };
            List<Employee> team1 = new List<Employee> { emp1, emp2 };

            mockRepository.Setup(q => q.Get(2)).Returns(new List<AboutUs>
            {
                new AboutUs { Name = "Accueil", IntroductionText = "Introd", Content = "Contenu", ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2", ImagePath3 = "ImagePath3",  Team = team1, Id = 2 }

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerAboutUs.Edit(2);
            //var model = (Admin.Web.Modules.HomePage.ViewModels.ViewModelDetail)result.Model;
            ViewModelAboutUs model = (ViewModelAboutUs)result.Model;

            model.AboutUs.Name.ShouldBe("Accueil");
            model.AboutUs.IntroductionText.ShouldBe("Introd");
            model.AboutUs.Content.ShouldBe("Contenu");
            model.AboutUs.ImagePath1.ShouldBe("ImagePath1");
            model.AboutUs.ImagePath2.ShouldBe("ImagePath2");
            model.AboutUs.ImagePath3.ShouldBe("ImagePath3");
            // Les employés ne sont pas liés à la page AboutUs. Donc, la Team est vide 
            // Je suis relativement certain que ça ne causera jamais de problèmes...
            model.AboutUs.Team.Count().ShouldBe(0);
            //model.AboutUs.Team[0].Name.ShouldBe(emp1.Name);
            //model.AboutUs.Team[0].Id.ShouldBe(emp1.Id);
            //model.AboutUs.Team[1].Name.ShouldBe(emp2.Name);
            //model.AboutUs.Team[1].Id.ShouldBe(emp2.Id);
            model.AboutUs.Id.ShouldBe(2);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Edit_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();

            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<AboutUs>
            {
                new AboutUs { Name = "Accueil", IntroductionText = "Introd", Content = "Contenu", ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2", ImagePath3 = "ImagePath3",  Id = 2 }

            }.AsQueryable());

            var result = controllerAboutUs.Edit(1);
            var result2 = controllerAboutUs.Edit(null);
            
            result.ShouldBeOfType<NotFoundResult>();
            result2.ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void EditEmployee_ShouldReturnViewWithEmployeeDetails()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();

            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            Employee employ1 = new Employee { Name = "Employe1", ImagePath = "www.google.qc.org.ca", Id = 2 };

            mockEmployeeRepo.Setup(q => q.Get(2)).Returns(new List<Employee>
            {
                employ1

            }.AsQueryable());

            var mv = new ViewModelEmployee();
            ViewResult result = controllerAboutUs.EditEmployee(2, 1).ShouldBeOfType<ViewResult>();
            ViewModelEmployee model = (ViewModelEmployee)result.Model;

            model.aboutUsId.ShouldBe(1);
            model.Employee.Name.ShouldBe("Employe1");
            model.Employee.Id.ShouldBe(2);
            model.Employee.ImagePath.ShouldBe("www.google.qc.org.ca");
        }

        [Fact]
        public void EditEmployee_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();

            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            Employee employ1 = new Employee { Name = "Employe1", ImagePath = "www.google.qc.org.ca", Id = 2 };

            mockEmployeeRepo.Setup(q => q.Get(2)).Returns(new List<Employee>
            {
                employ1

            }.AsQueryable());

            controllerAboutUs.EditEmployee(3, 1).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void EditEmployeePOST_ShouldReturnActionResultEdit()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            Employee employ1 = new Employee { Name = "Employe1", ImagePath = "www.google.qc.org.ca", Id = 2 };

            mockEmployeeRepo.Setup(q => q.Get(2)).Returns(new List<Employee>
            {
                employ1

            }.AsQueryable());

            mockRepository.Setup(q => q.Get(1)).Returns(new List<AboutUs>
            {
                new AboutUs() {Id=1, Name="TitreSignificatif" }

            }.AsQueryable());

            var mv = new ViewModelEmployee();
            mv.aboutUsId = 1;
            RedirectToActionResult result = controllerAboutUs.EditEmployee(employ1, mv).ShouldBeOfType<RedirectToActionResult>();
            result.ActionName.ShouldBe("Edit");
        }

        [Fact]
        public void EditEmployeePOST_ShouldReturnActionResultIndex()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            Employee employ1 = new Employee { Name = "Employe1", ImagePath = "www.google.qc.org.ca", Id = 2 };

            mockEmployeeRepo.Setup(q => q.Get(2)).Returns(new List<Employee>
            {
                employ1

            }.AsQueryable());

            mockRepository.Setup(q => q.Get(2)).Returns(new List<AboutUs>
            {
                new AboutUs() {Id=2, Name="TitreSignificatif" }

            }.AsQueryable());

            var mv = new ViewModelEmployee();
            mv.aboutUsId = 1;
            RedirectToActionResult result = controllerAboutUs.EditEmployee(employ1, mv).ShouldBeOfType<RedirectToActionResult>();
            result.ActionName.ShouldBe("Index");
        }

        ////Impossible à tester....?
        //[Fact]
        //public void EditEmployee_ShouldUpdateEmployee()
        //{

        //}

        [Fact]
        public void CreateEmployeePOST_ShouldReturnRedirectToActionResultIndex()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            Employee employ1 = new Employee { Name = "Employe1", ImagePath = "www.google.qc.org.ca", Id = 2 };

            ViewModelAboutUs mv = new ViewModelAboutUs();
            var result = controllerAboutUs.CreateEmployee(2, employ1, mv);

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void CreateEmployee_ShouldReturnUneErreurQuelconque()
        {
            //1.ShouldBe(2);
        }

        // impossible à tester?...
        //[Fact]
        //public void AddEmployeePicture_ShouldAddEmployeePicture()
        //{

        //}
        [Fact]
        public void AddEmployeePicture_ShouldReturnNotFound()
        {

            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();

            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            mockEmployeeRepo.Setup(q => q.Get(2)).Returns(new List<Employee>
            {
                new Employee { Name = "Employe1", ImagePath = "www.google.qc.org.ca", Id = 2 }
            }.AsQueryable());
          

            ViewModelAboutUs mv = new ViewModelAboutUs();

            var result = controllerAboutUs.AddEmployeePicture("blobname", 1, mv);
            result.ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void DeleteEmployee_ShouldReturnViewResultIndex()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            Employee employ1 = new Employee { Name = "Employe1", ImagePath = "www.google.qc.org.ca", Id = 2 };

            mockEmployeeRepo.Setup(q => q.Get(2)).Returns(new List<Employee>
            {
                 new Employee () { Name = "Employe1", ImagePath = "www.google.qc.org.ca", Id = 2 }
            }.AsQueryable());

            var result = controllerAboutUs.DeleteEmployee(2, 3).ShouldBeOfType<ViewResult>();
            ViewModelEmployee model = (ViewModelEmployee)result.Model;
            model.aboutUsId.ShouldBe(3);
        }

        [Fact]
        public void DeleteEmployee_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            Employee employ1 = new Employee { Name = "Employe1", ImagePath = "www.google.qc.org.ca", Id = 2 };

            mockEmployeeRepo.Setup(q => q.Get(2)).Returns(new List<Employee>
            {
                 new Employee () { Name = "Employe1", ImagePath = "www.google.qc.org.ca", Id = 2 }
            }.AsQueryable());

            var result = controllerAboutUs.DeleteEmployee(3, 3).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void DeleteEmployeePOST_ShouldReturnRedirectToActionResultEdit()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            Employee employ1 = new Employee { Name = "Employe1", ImagePath = "www.google.org", Id = 2 };

            mockEmployeeRepo.Setup(q => q.Get(2)).Returns(new List<Employee>
            {
                 new Employee () { Name = "Employe1", ImagePath = "www.google.org.", Id = 2 }
            }.AsQueryable());

            ViewModelEmployee mv = new ViewModelEmployee();
            var result = controllerAboutUs.DeleteEmployee(2, mv).ShouldBeOfType<RedirectToActionResult>();
            result.ActionName.ShouldBe("Edit");
        }

        [Fact]
        public void DeleteEmployeePOST_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            Employee employ1 = new Employee { Name = "Employe1", ImagePath = "www.google.org", Id = 2 };

            mockEmployeeRepo.Setup(q => q.Get(2)).Returns(new List<Employee>
            {
                 new Employee () { Name = "Employe1", ImagePath = "www.google.org.", Id = 2 }
            }.AsQueryable());

            ViewModelEmployee mv = new ViewModelEmployee();
            var result = controllerAboutUs.DeleteEmployee(3, mv).ShouldBeOfType<NotFoundResult>();
        }

        //[Fact]
        //public void EditEmployee_ShouldReturnViewWithEmployeeDetails()
        //{

        //}



        //[Fact]
        //public void Edit_ShouldReturnNotFoundResult()
        //{
        //    Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
        //    Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
        //    Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
        //    Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

        //    HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


        //    mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
        //    {
        //        new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", Produit = new List<Product>(), Slider = new List<Slider>()}

        //    }.AsQueryable());

        //    controllerHomePage.Edit(800).ShouldBeOfType<NotFoundResult>();
        //    controllerHomePage.Edit(null).ShouldBeOfType<NotFoundResult>();
        //}

        //[Fact]
        //public void Edit_ShouldChangeHomePage()
        //{
        //    Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
        //    Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
        //    Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
        //    Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

        //    HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


        //    mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
        //    {
        //        new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", Produit = new List<Product>(), Slider = new List<Slider>()}

        //    }.AsQueryable());

        //    var mv = new Admin.Web.Modules.HomePage.ViewModels.ViewModelDetail
        //    {
        //        HomePage = new HomePage { Name = "Accueil3", AboutImagePath = "AboutImagePath3", AboutText = "aboutText3", AboutTitle = "AboutTite3", Id = 2, PersoContent = "persoContent3", PersoImagePath = "PersoImagePath3", Produit = new List<Product>(), Slider = new List<Slider>() }
        //    };

        //    IActionResult result = controllerHomePage.Edit(2, mv);
        //    mockRepository.Verify(d => d.Update(It.IsAny<HomePage>()), Times.AtLeast(1));

        //    result.ShouldBeOfType<RedirectToActionResult>();

        //}

        //[Fact]
        //public void Delete_ShouldReturnNotFoundResult()
        //{
        //    Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
        //    Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
        //    Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
        //    Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

        //    HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


        //    mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
        //    {
        //        new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", Produit = new List<Product>(), Slider = new List<Slider>()}

        //    }.AsQueryable());


        //    controllerHomePage.Delete("blob", 101, "identifiant").ShouldBeOfType<NotFoundResult>();
        //    controllerHomePage.Delete("", 2, "identifiant").ShouldBeOfType<NotFoundResult>();
        //}


        //[Fact]
        //public void Delete_ShouldDeleteABlob()
        //{
        //    Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
        //    Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
        //    Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
        //    Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

        //    HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


        //    mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
        //    {
        //        new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", Produit = new List<Product>(), Slider = new List<Slider>()}

        //    }.AsQueryable());

        //    IActionResult result = controllerHomePage.Delete("blob", 2, "identifiant");

        //    mockRepository.Verify(d => d.Save(It.IsAny<HomePage>()), Times.AtLeast(1));

        //    result.ShouldBeOfType<RedirectToActionResult>();
        //}

        [Fact]
        public void Default_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<AboutUs>
            {
                new AboutUs { Name = "Accueil", IntroductionText = "Introd", Content = "Contenu", ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2", ImagePath3 = "ImagePath3", Id = 2, isDefault = false }

            }.AsQueryable());

            controllerAboutUs.Default(800).ShouldBeOfType<NotFoundResult>();
            controllerAboutUs.Default(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Default_ShouldMakeDefaultAboutUs()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<AboutUs>
            {
                new AboutUs { Name = "Accueil", IntroductionText = "Introd", Content = "Contenu", ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2", ImagePath3 = "ImagePath3",  Id = 2, isDefault = false }

            }.AsQueryable());

            IActionResult result = controllerAboutUs.Default(2);
            mockRepository.Verify(d => d.Update(It.IsAny<AboutUs>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }

        [Fact]
        public void Active_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<AboutUs>
            {
                new AboutUs { Name = "Accueil", IntroductionText = "Introd", Content = "Contenu", ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2", ImagePath3 = "ImagePath3", Id = 2, isActive = false }

            }.AsQueryable());


            controllerAboutUs.Active(800).ShouldBeOfType<NotFoundResult>();
            controllerAboutUs.Active(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Active_ShouldActiveAboutPage()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<AboutUs>
            {
                new AboutUs { Name = "Accueil", IntroductionText = "Introd", Content = "Contenu", ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2", ImagePath3 = "ImagePath3",  Id = 2, isActive = false }

            }.AsQueryable());


            IActionResult result = controllerAboutUs.Active(2);
            mockRepository.Verify(d => d.Update(It.IsAny<AboutUs>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }


        [Fact]
        public void UnActive_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<AboutUs>
            {
                new AboutUs { Name = "Accueil", IntroductionText = "Introd", Content = "Contenu", ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2", ImagePath3 = "ImagePath3",  Id = 2, isActive = true }

            }.AsQueryable());

            controllerAboutUs.Active(800).ShouldBeOfType<NotFoundResult>();
            controllerAboutUs.Active(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Unactive_ShouldUnactiveAboutPage()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<AboutUs>
            {
                new AboutUs { Name = "Accueil", IntroductionText = "Introd", Content = "Contenu", ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2", ImagePath3 = "ImagePath3", Id = 2, isActive = true }

            }.AsQueryable());

            IActionResult result = controllerAboutUs.Active(2);
            mockRepository.Verify(d => d.Update(It.IsAny<AboutUs>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }

        [Fact]
        public void DeleteItem_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<AboutUs>
            {
                new AboutUs { Name = "Accueil", IntroductionText = "Introd", Content = "Contenu", ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2", ImagePath3 = "ImagePath3", Id = 2, isDelete = false }

            }.AsQueryable());

            controllerAboutUs.DeleteItem(800).ShouldBeOfType<NotFoundResult>();
            controllerAboutUs.DeleteItem(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void DeleteItem_ShouldDeleteAboutPage()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Employee>> mockEmployeeRepo = new Mock<IRepository<Employee>>();
            AboutController controllerAboutUs = new AboutController(mockRepository.Object, mockEmployeeRepo.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<AboutUs>
            {
                new AboutUs { Name = "Accueil", IntroductionText = "Introd", Content = "Contenu", ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2", ImagePath3 = "ImagePath3", Id = 2, isDelete = false }

            }.AsQueryable());

            IActionResult result = controllerAboutUs.DeleteItem(2);
            mockRepository.Verify(d => d.Update(It.IsAny<AboutUs>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }

    }
}
