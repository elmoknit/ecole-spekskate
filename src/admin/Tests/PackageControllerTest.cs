﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Shouldly;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Package;
using SpekSkates.Admin.Web.Modules.Package.Controllers;
using SpekSkates.Admin.Web.Modules.Package.Models;
using SpekSkates.Admin.Web.Modules.Package.ViewModels;
using SpekSkates.Admin.Web.Modules.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;
using Xunit;

namespace SpekSkates.Admin.Tests.Manager
{
    public class PackageControllerTest
    {

        [Fact(DisplayName = "Create_ShouldReturnViewResultWithFilledProductSelectList")]
        public void Create_ShouldReturnViewResultWithFilledProductSelectList()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, url = "lien vers produit1", isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, url = "lien vers produit2", isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, url = "lien vers produit3", isActive = true, isDelete = false };

            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };


            var package1 = new Package { Id = 1, Name = "Pack", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1, product2, product3

            }.AsQueryable());

            ViewResult result = packageController.Create().ShouldBeOfType<ViewResult>();
            var model = (ViewModelPackage)result.Model;
            model.SelectListProduct[1].Value.ShouldBe("2");
            model.SelectListProduct[0].Text.ShouldBe("Produit1 - " + product1.Price + "$");
            model.SelectListProduct[2].Text.ShouldBe("Produit3 - " + product3.Price + "$");
        }

        [Fact(DisplayName = "CreatePOST_ShouldReturnViewResult0")]
        public void CreatePOST_ShouldReturnViewResul0t()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, url = "lien vers produit1", isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, url = "lien vers produit2", isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, url = "lien vers produit3", isActive = true, isDelete = false };

            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lien11Short = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lien12Short = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };


            var package1 = new Package { Id = 1, Name = "Pack", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };
            //var package2 = new PackageShort { Id = 1, Name = "PackShort" };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1, product2, product3

            }.AsQueryable());

            mockProductRepository.Setup(q => q.Get(1)).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            mockProductRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                product2

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lien11Short);
            viewModel.NewLiensProduitPackage.Add(lien12Short);
            viewModel.Package = package1;

            var result = packageController.Create(viewModel, formCollection);

        }

        [Fact(DisplayName = "CreatePOST_ShouldReturnViewResult1")]
        public void CreatePOST_ShouldRedirectReturnViewResult1()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, url = "lien vers produit1", isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, url = "lien vers produit2", isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, url = "lien vers produit3", isActive = true, isDelete = false };

            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lien11Short = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lien12Short = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };


            var package1 = new Package { Id = 1, Name = "Pack", codeProduit = "1", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };
            //var package2Short = new PackageShort { Id = 1, Name = "PackShort" };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1, product2, product3

            }.AsQueryable());

            mockProductRepository.Setup(q => q.Get(1)).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lien11Short);
            viewModel.Package = package1;

            var result = packageController.Create(viewModel, formCollection).ShouldBeOfType<RedirectToActionResult>();

        }

        [Fact(DisplayName = "CreatePOST_ShouldReturnViewResult2")]
        public void CreatePOST_ShouldRedirectReturnViewResult2()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", codeProduit = "1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, url = "lien vers produit1", isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", codeProduit = "2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, url = "lien vers produit2", isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", codeProduit = "3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, url = "lien vers produit3", isActive = true, isDelete = false };

            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };


            var package1 = new Package { Id = 1, Name = "Pack", codeProduit = "1", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };
            // var package2Short = new PackageShort { Id = 1, Name = "PackShort" };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1, product2, product3

            }.AsQueryable());

            mockProductRepository.Setup(q => q.Get(1)).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            mockProductRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                product2

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort11);
            viewModel.Package = package1;

            var result = packageController.Create(viewModel, formCollection).ShouldBeOfType<RedirectToActionResult>();
            
        }


        [Fact(DisplayName = "CreatePOST_ShouldReturnViewResult3")]
        public void CreatePOST_ShouldRedirectReturnViewResult3()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, url = "lien vers produit1", isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, url = "lien vers produit2", isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, url = "lien vers produit3", isActive = true, isDelete = false };

            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };
            var lienShort10 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 0 };


            var package1 = new Package { Id = 1, Name = "Pack", codeProduit = "1", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };
            //var package2Short = new PackageShort { Id = 1, Name = "PackShort" };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1, product2, product3

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort10);
            viewModel.Package = package1;

            var result = packageController.Create(viewModel, formCollection).ShouldBeOfType<ViewResult>();

            var model = (ViewModelPackage)result.Model;
            model.errorMessage.ShouldBe("Une erreur s'est produite, veuillez vérifier les produits sélectionnés et que leur quantité. ");
            model.Package.Name.ShouldBe("Pack");
            model.NewLiensProduitPackage.Count.ShouldBe(2);
            model.NewLiensProduitPackage[0].Quantity.ShouldBe(13);
            model.NewLiensProduitPackage[0].ProductId.ShouldBe(1);
            model.NewLiensProduitPackage[1].Quantity.ShouldBe(0);
            model.NewLiensProduitPackage[1].ProductId.ShouldBe(1);
        }

        [Fact(DisplayName = "CreatePOST_ShouldReturnViewResult4")]
        public void CreatePOST_ShouldReturnViewResult4()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, isActive = true, isDelete = false };

            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };
            var lienShort10 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 0 };


            var package1 = new Package { Id = 1, Name = "    ", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };
            //var package2Short = new PackageShort { Id = 1, Name = "PackShort" };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1, product2, product3

            }.AsQueryable());

            mockProductRepository.Setup(q => q.Get(1)).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            mockProductRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                product2

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort2);
            viewModel.Package = package1;
        }

        [Fact(DisplayName = "CreatePOST_ShouldReturnViewResult5")]
        public void CreatePOST_ShouldReturnViewResult5()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, isActive = true, isDelete = false };

            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };
            var lienShort10 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 0 };


            var package1 = new Package { Id = 1, Name = "Package", codeProduit = "1", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };
            var package2 = new Package { Id = 2, Name = "Package", codeProduit = "2", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };

            var data = new List<Package>
            {
                package1,
                package2
            }.AsQueryable();

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1, product2, product3

            }.AsQueryable());

            mockPackageRepository.Setup(q => q.GetAll()).Returns(new List<Package>
            {
                package1

            }.AsQueryable());

            mockProductRepository.Setup(q => q.Get(1)).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            mockProductRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                product2

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort2);
            viewModel.Package = package2;

            var result = packageController.Create(viewModel, formCollection).ShouldBeOfType<ViewResult>();

            var model = (ViewModelPackage)result.Model;
            model.errorMessage.ShouldBe("Un package avec un nom identique existe déjà. ");
            var test = model.Package.Description;
            // package: product1 x1, product2 x2 + lienshort1: product1 x13, lienshort2: product2 x25
            model.Package.Description.ShouldBe("Produits inclus: " + product1.Name + " (x14), " + product2.Name + " (x27)");
            //model.Package.Description.ShouldContain(product1.Name);
            //model.Package.Description.ShouldContain(product2.Name);
            model.Package.Name.ShouldBe("Package");
            model.NewLiensProduitPackage.Count.ShouldBe(2);
            model.NewLiensProduitPackage[0].Quantity.ShouldBe(13);
            model.NewLiensProduitPackage[0].ProductId.ShouldBe(1);
            model.NewLiensProduitPackage[1].Quantity.ShouldBe(25);
            model.NewLiensProduitPackage[1].ProductId.ShouldBe(2);
        }

        [Fact(DisplayName = "CreatePOST_ShouldReturnRedirectToActionResult")]
        public void CreatePOST_ShouldReturnRedirectToActionResult()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, isActive = true, isDelete = false };

            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };
            var lienShort10 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 0 };


            var package1 = new Package { Id = 1, Name = "Package", codeProduit = "1", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };
            var package2 = new Package { Id = 2, Name = "Package", codeProduit = "2", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1, product2, product3

            }.AsQueryable());

            mockProductRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            mockProductRepository.Setup(q => q.Get(1)).Returns(new List<Product>
            {
                product2

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort2);
            viewModel.Package = package2;

            var result = packageController.Create(viewModel, formCollection);

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Edit_ShouldReturnViewResult()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, isActive = true, isDelete = false };

            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };
            var lienShort10 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 0 };


            var package1 = new Package { Id = 1, Name = "Package", DiscountPercentage = 1, lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };
            var package2 = new Package { Id = 2, Name = "Package", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockPackageRepository.Setup(q => q.Get(1)).Returns(new List<Package>
            {
                package1

            }.AsQueryable());

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1, product2, product3

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort2);
            viewModel.LiensInPackage = new List<LienProduitPackage>();
            viewModel.Package = package1;

            var result = packageController.Edit(1).ShouldBeOfType<ViewResult>();
            var model = (ViewModelPackage)result.Model;

            model.discountApplied.ShouldBe(true);
            model.SelectListProduct.Count.ShouldBe(3);
            model.HiddenSelectListProductName.Count.ShouldBe(3);
        }

        [Fact]
        public void Edit_ShouldReturnViewResult2()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, isActive = true, isDelete = false };

            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };
            var lienShort10 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 0 };


            var package1 = new Package { Id = 1, Name = "Package", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };
            var package2 = new Package { Id = 2, Name = "Package", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockPackageRepository.Setup(q => q.Get(2)).Returns(new List<Package>
            {
                package2

            }.AsQueryable());

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort2);
            viewModel.LiensInPackage = new List<LienProduitPackage>();
            viewModel.Package = package2;

            var result = packageController.Edit(2).ShouldBeOfType<ViewResult>();
            var model = (ViewModelPackage)result.Model;

            model.discountApplied.ShouldBe(false);
            model.SelectListProduct.Count.ShouldBe(1);
            model.HiddenSelectListProductId.Count.ShouldBe(1);
        }

        [Fact]
        public void Edit_ShouldReturnNotFound()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, isActive = true, isDelete = false };

            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };
            var lienShort10 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 0 };


            var package1 = new Package { Id = 1, Name = "Package",  DiscountPercentage = 1, lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };
            var package2 = new Package { Id = 2, Name = "Package", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockPackageRepository.Setup(q => q.Get(1)).Returns(new List<Package>
            {


            }.AsQueryable());

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {


            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort2);
            viewModel.Package = package2;

            var result = packageController.Edit(1).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void EditPOST_ShouldReturnNotFound()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, isActive = true, isDelete = false };

            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };
            var lienShort10 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 0 };


            var package1 = new Package { Id = 1, Name = "Package", DiscountPercentage = 1, lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };
            var package2 = new Package { Id = 2, Name = "Package", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12, lien13 } };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockPackageRepository.Setup(q => q.Get(1)).Returns(new List<Package>
            {
                package1

            }.AsQueryable());

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            mockProductRepository.Setup(q => q.Get(1)).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            mockProductRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                product2

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort2);
            viewModel.LiensInPackage = new List<LienProduitPackage>();
            viewModel.Package = package1;

            var result = packageController.Edit(100, viewModel, formCollection).ShouldBeOfType<NotFoundResult>();
            //var model = (ViewModelPackage)result.Model;

            //result.ViewName.ShouldContain("package/edit/1");
            //model.discountApplied.ShouldBe(false);
            //model.SelectListProduct.Count.ShouldBe(1);
            //model.HiddenSelectListProductId.Count.ShouldBe(1);
        }

        [Fact]
        public void EditPOST_ShouldReturnViewResult1()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, isActive = true, isDelete = false };

            var lien1 = new LienProduitPackage { Id = 4, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };
            var lienShort10 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 0 };


            var package1 = new Package { Id = 1, Name = "Package", codeProduit = "1", DiscountPercentage = 1, lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien1 } };
            var package2 = new Package { Id = 2, Name = "Package", codeProduit = "2", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien1 } };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockPackageRepository.Setup(q => q.Get(1)).Returns(new List<Package>
            {
                package1

            }.AsQueryable());

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.LiensInPackage = new List<LienProduitPackage>();
            viewModel.Package = package1;

            var result = packageController.Edit(1, viewModel, formCollection).ShouldBeOfType<RedirectToActionResult>();

        }

        [Fact]
        public void EditPOST_ShoulReturnViewResult2()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, isActive = true, isDelete = false };

            var lien1 = new LienProduitPackage { Id = 4, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };
            var lienShort10 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 0 };


            var package1 = new Package { Id = 1, Name = "Package", DiscountPercentage = 1, lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien1 } };
            var package2 = new Package { Id = 2, Name = "Package", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12 } };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockPackageRepository.Setup(q => q.Get(1)).Returns(new List<Package>
            {
                package1

            }.AsQueryable());

            mockPackageRepository.Setup(q => q.GetAll()).Returns(new List<Package>
            {
                package2

            }.AsQueryable());

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort2);
            viewModel.LiensInPackage = new List<LienProduitPackage>();
            viewModel.Package = package1;

            var result = packageController.Edit(1, viewModel, formCollection).ShouldBeOfType<ViewResult>();
            var model = result.Model as ViewModelPackage;
            model.errorMessage.ShouldBe("Un package avec un nom identique existe déjà. ");
        }

        [Fact]
        public void EditPOST_ShoulReturnViewResult3()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, isActive = true, isDelete = false };

            var lien1 = new LienProduitPackage { Id = 4, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };
            var lienShort10 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 0 };


            var package1 = new Package { Id = 1, Name = "", DiscountPercentage = 1, lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien1 } };
            var package2 = new Package { Id = 2, Name = "Package", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12 } };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockPackageRepository.Setup(q => q.Get(1)).Returns(new List<Package>
            {
                package1

            }.AsQueryable());

            mockPackageRepository.Setup(q => q.GetAll()).Returns(new List<Package>
            {
                package2

            }.AsQueryable());

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort2);
            viewModel.LiensInPackage = new List<LienProduitPackage>();
            viewModel.Package = package1;

            var result = packageController.Edit(1, viewModel, formCollection).ShouldBeOfType<ViewResult>();
        }

        [Fact]
        public void EditPOST_ShoulReturnRedirectToActionResult1()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, isActive = true, isDelete = false };

            var lien1 = new LienProduitPackage { Id = 4, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };
            var lienShort10 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 0 };


            var package1 = new Package { Id = 1, Name = "Package", codeProduit = "1", DiscountPercentage = 1, lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien1 } };
            var package2 = new Package { Id = 2, Name = "Package", codeProduit = "2", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12 } };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockPackageRepository.Setup(q => q.Get(1)).Returns(new List<Package>
            {
                package1

            }.AsQueryable());

            mockPackageRepository.Setup(q => q.GetAll()).Returns(new List<Package>
            {


            }.AsQueryable());

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort2);
            viewModel.LiensInPackage = new List<LienProduitPackage>();
            viewModel.discountApplied = false;
            viewModel.Package = package1;

            var result = packageController.Edit(1, viewModel, formCollection).ShouldBeOfType<RedirectToActionResult>();
            //var model = (ViewModelPackage)result.Model;
            //model.Package.DiscountPercentage.ShouldBe(0);
        }

        [Fact]
        public void EditPOST_ShoulReturnRedirectToActionResult2()
        {
            Mock<IRepository<LienProduitPackage>> mockLienRepository = new Mock<IRepository<LienProduitPackage>>();
            Mock<IRepository<Package>> mockPackageRepository = new Mock<IRepository<Package>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            PackageController packageController = new PackageController(mockPackageRepository.Object, mockProductRepository.Object, mockLienRepository.Object, mockBlobStorage.Object);

            var product1 = new Product { Id = 1, Name = "Produit1", Price = 10.50M, History = "Histoire1", Description = "description1", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product2 = new Product { Id = 2, Name = "Produit2", Price = 20.50M, History = "Histoire2", Description = "description2", ProductCategoryId = 1, ProductSizeId = 1, isActive = true, isDelete = false };
            var product3 = new Product { Id = 3, Name = "Produit3", Price = 30.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 2, ProductSizeId = 2, isActive = true, isDelete = false };

            var lien1 = new LienProduitPackage { Id = 4, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien11 = new LienProduitPackage { Id = 1, PackageId = 1, Product = product1, ProductId = 1, Quantity = 1 };
            var lien12 = new LienProduitPackage { Id = 2, PackageId = 1, Product = product2, ProductId = 2, Quantity = 2 };
            var lien13 = new LienProduitPackage { Id = 3, PackageId = 1, Product = product3, ProductId = 3, Quantity = 3 };

            var lienShort1 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 13 };
            var lienShort2 = new LienProduitPackageShort { Id = 2, ProductId = 2, Quantity = 25 };
            var lienShort11 = new LienProduitPackageShort { Id = 3, ProductId = 1, Quantity = 90 };
            var lienShort10 = new LienProduitPackageShort { Id = 1, ProductId = 1, Quantity = 0 };


            var package1 = new Package { Id = 1, Name = "Package", codeProduit = "1", DiscountPercentage = 1, lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien1 } };
            var package2 = new Package { Id = 2, Name = "Package", codeProduit = "2", lstLienProduitPackage = new List<LienProduitPackage> { lien11, lien12 } };

            Mock<IFormCollection> formMock = new Mock<IFormCollection>();
            var dictionnaire = new Dictionary<String, Microsoft.Extensions.Primitives.StringValues>();
            dictionnaire.Add("empty, but required", "dictionnary");

            var formCollection = new FormCollection(dictionnaire);

            mockPackageRepository.Setup(q => q.Get(1)).Returns(new List<Package>
            {
                package1

            }.AsQueryable());

            mockPackageRepository.Setup(q => q.GetAll()).Returns(new List<Package>
            {
                package1

            }.AsQueryable());

            mockProductRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                product1

            }.AsQueryable());

            ViewModelPackage viewModel = new ViewModelPackage();
            viewModel.NewLiensProduitPackage.Add(lienShort1);
            viewModel.NewLiensProduitPackage.Add(lienShort2);
            viewModel.LiensInPackage = new List<LienProduitPackage>();
            viewModel.discountApplied = false;
            viewModel.Package = package1;

            var result = packageController.Edit(1, viewModel, formCollection).ShouldBeOfType<RedirectToActionResult>();
        }


    }
}
