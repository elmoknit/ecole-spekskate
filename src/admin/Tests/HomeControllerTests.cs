﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using SpekSkates.Admin.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web;
using SpekSkates.Admin.Web.Modules.HomePage;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.HomePage.ViewModels;
using SpekSkates.Admin.Web.Modules.Product;
using SpekSkates.Admin.Web.Modules.Slider;

namespace SpekSkates.Tests
{
    public class HomeControllerTests
    {

        [Fact]
        public void Index_ShouldReturnViewResult() {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object,mockProductRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 1, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>()}

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerHomePage.Index();
            var model = (List<HomePage>)result.Model;

            model.Count.ShouldBe(1);
            model[0].Name.ShouldBe("Accueil");
            model[0].AboutImagePath.ShouldBe("AboutImagePath");
            model[0].AboutText.ShouldBe("aboutText");
            model[0].AboutTitle.ShouldBe("AboutTite");
            model[0].Id.ShouldBe(1);
            model[0].PersoContent.ShouldBe("persoContent");
            model[0].PersoImagePath.ShouldBe("PersoImagePath");
            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("views/homepage/index.cshtml");
        }



        [Fact]
        public void Edit_ShouldReturnViewModelDetails() {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>()}

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerHomePage.Edit(2);
            var model = (Admin.Web.Modules.HomePage.ViewModels.ViewModelDetail)result.Model;
            model.HomePage.Name.ShouldBe("Accueil");
            model.HomePage.AboutImagePath.ShouldBe("AboutImagePath");
            model.HomePage.AboutText.ShouldBe("aboutText");
            model.HomePage.AboutTitle.ShouldBe("AboutTite");
            model.HomePage.Id.ShouldBe(2);
            model.HomePage.PersoContent.ShouldBe("persoContent");
            model.HomePage.PersoImagePath.ShouldBe("PersoImagePath");
            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Edit_ShouldReturnNotFoundResult() {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


            mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>()}

            }.AsQueryable());

            controllerHomePage.Edit(800).ShouldBeOfType<NotFoundResult>();
            controllerHomePage.Edit(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Edit_ShouldChangeHomePage() {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


            mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>()}

            }.AsQueryable());

            var mv = new Admin.Web.Modules.HomePage.ViewModels.ViewModelDetail {
                HomePage = new HomePage { Name = "Accueil3", AboutImagePath = "AboutImagePath3", AboutText = "aboutText3", AboutTitle = "AboutTite3", Id = 2, PersoContent = "persoContent3", PersoImagePath = "PersoImagePath3", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>() }
            };

            IActionResult result = controllerHomePage.Edit(2, mv);
            mockRepository.Verify(d => d.Update(It.IsAny<HomePage>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }

        [Fact]
        public void Delete_ShouldReturnNotFoundResult() {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


            mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>()}

            }.AsQueryable());


            controllerHomePage.Delete("blob",101, "identifiant").ShouldBeOfType<NotFoundResult>();
            controllerHomePage.Delete("", 2, "identifiant").ShouldBeOfType<NotFoundResult>();
        }


        [Fact]
        public void Delete_ShouldDeleteABlob() {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


            mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>()}

            }.AsQueryable());

            IActionResult result = controllerHomePage.Delete("blob", 2, "identifiant");

            mockRepository.Verify(d => d.Save(It.IsAny<HomePage>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        /////////////////////////////
        [Fact]
        public void Default_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


            mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>(), isDefault = false}

            }.AsQueryable());

            controllerHomePage.Default(800).ShouldBeOfType<NotFoundResult>();
            controllerHomePage.Default(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Default_ShouldMakeDefaultHomePage()
        {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


            var mockobject = mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>(), isDefault = false }

            }.AsQueryable());

            IActionResult result = controllerHomePage.Default(2);
            mockRepository.Verify(d => d.Update(It.IsAny<HomePage>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }

        [Fact]
        public void Active_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


            mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>(), isActive = false}

            }.AsQueryable());

            controllerHomePage.Active(800).ShouldBeOfType<NotFoundResult>();
            controllerHomePage.Active(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Active_ShouldActiveHomePage()
        {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


            var mockobject = mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>(), isActive = false }

            }.AsQueryable());
            
            IActionResult result = controllerHomePage.Active(2);
            mockRepository.Verify(d => d.Update(It.IsAny<HomePage>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }


        [Fact]
        public void UnActive_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


            mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>(), isActive = true}

            }.AsQueryable());

            controllerHomePage.Active(800).ShouldBeOfType<NotFoundResult>();
            controllerHomePage.Active(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Unactive_ShouldUnactiveHomePage()
        {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


            var mockobject = mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>(), isActive = true }

            }.AsQueryable());

            IActionResult result = controllerHomePage.Active(2);
            mockRepository.Verify(d => d.Update(It.IsAny<HomePage>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }

        [Fact]
        public void DeleteItem_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


            mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>(), isDelete = false}

            }.AsQueryable());

            controllerHomePage.DeleteItem(800).ShouldBeOfType<NotFoundResult>();
            controllerHomePage.DeleteItem(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void DeleteItem_ShouldDeleteHomePage()
        {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();
            Mock<IRepository<Slider>> mockSliderRepository = new Mock<IRepository<Slider>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            HomePageController controllerHomePage = new HomePageController(mockRepository.Object, mockSliderRepository.Object, mockProductRepository.Object, mockBlobStorage.Object);


            var mockobject = mockRepository.Setup(q => q.Get(2)).Returns(new List<HomePage>
            {
                new HomePage { Name = "Accueil", AboutImagePath = "AboutImagePath", AboutText = "aboutText", AboutTitle = "AboutTite", Id = 2, PersoContent = "persoContent", PersoImagePath = "PersoImagePath", HomePageProducts = new List<HomePageProduct>(), HomePageSliders = new List<HomePageSlider>(), isDelete = false }

            }.AsQueryable());

            IActionResult result = controllerHomePage.DeleteItem(2);
            mockRepository.Verify(d => d.Update(It.IsAny<HomePage>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }


    }
}
