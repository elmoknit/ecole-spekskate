﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using SpekSkates.Admin.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web;
using SpekSkates.Admin.Web.Modules.History;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.History.ViewModels;
using SpekSkates.Admin.Web.Modules.Product;
using SpekSkates.Admin.Web.Modules.Slider;
using SpekSkates.Admin.Web.Modules.History;

namespace SpekSkates.Tests
{
    public class HistoryControllerTests
    {

        [Fact]
        public void Index_ShouldReturnViewResult() {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<History>
            {
                new History { Name = "History", Id = 1, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerHistory.Index();
            var model = (List<History>)result.Model;

            model.Count.ShouldBe(1);
            model[0].Name.ShouldBe("History");
            model[0].Picture1Path.ShouldBe("path1");
            model[0].Picture2Path.ShouldBe("path2");
            model[0].Picture3Path.ShouldBe("path3");
            model[0].Id.ShouldBe(1);
            model[0].Text.ShouldBe("Text");
            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("views/history/index.cshtml");
        }



        [Fact]
        public void Edit_ShouldReturnViewModelDetails() {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<History>
            {
                new History { Name = "History", Id = 2, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerHistory.Edit(2);
            var model = (Admin.Web.Modules.History.ViewModels.ViewModelDetail)result.Model;
            model.History.Name.ShouldBe("History");
            model.History.Picture1Path.ShouldBe("path1");

            model.History.Picture1Path.ShouldBe("path1");
            model.History.Picture2Path.ShouldBe("path2");
            model.History.Picture3Path.ShouldBe("path3");
            model.History.Text.ShouldBe("Text");
           
            model.History.Id.ShouldBe(2);
            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Edit_ShouldReturnNotFoundResult() {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<History>
            {
                new History { Name = "History", Id = 2, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());
            controllerHistory.Edit(800).ShouldBeOfType<NotFoundResult>();
            controllerHistory.Edit(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Edit_ShouldChangeHistory() {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<History>
            {
                new History { Name = "History", Id = 2, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());

            var mv = new Admin.Web.Modules.History.ViewModels.ViewModelDetail {
             History = new History { Name = "History3", Id = 2, Picture1Path = "path13", Picture2Path= "path23", Picture3Path = "path33", Text = "Text3", isActive = true, isDefault= false, isDelete = false }
            };

            IActionResult result = controllerHistory.Edit(2, mv);
            mockRepository.Verify(d => d.Update(It.IsAny<History>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }

        [Fact]
        public void Delete_ShouldReturnNotFoundResult() {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<History>
            {
                new History { Name = "History", Id = 2, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());


            controllerHistory.Delete("blob",101, "identifiant").ShouldBeOfType<NotFoundResult>();
            controllerHistory.Delete("", 2, "identifiant").ShouldBeOfType<NotFoundResult>();
        }


        [Fact]
        public void Delete_ShouldDeleteABlob() {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<History>
            {
                new History { Name = "History", Id = 2, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());


            IActionResult result = controllerHistory.Delete("path1", 2, "picture1");

            mockRepository.Verify(d => d.Save(It.IsAny<History>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        /////////////////////////////
        [Fact]
        public void Default_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<History>
            {
                new History { Name = "History", Id = 2, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());


            controllerHistory.Default(800).ShouldBeOfType<NotFoundResult>();
            controllerHistory.Default(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Default_ShouldMakeDefaultHistory()
        {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<History>
            {
                new History { Name = "History", Id = 2, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());

            IActionResult result = controllerHistory.Default(2);
            mockRepository.Verify(d => d.Update(It.IsAny<History>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }

        [Fact]
        public void Active_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<History>
            {
                new History { Name = "History", Id = 2, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());

            controllerHistory.Active(800).ShouldBeOfType<NotFoundResult>();
            controllerHistory.Active(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Active_ShouldActiveHistory()
        {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<History>
            {
                new History { Name = "History", Id = 2, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());

            IActionResult result = controllerHistory.Active(2);
            mockRepository.Verify(d => d.Update(It.IsAny<History>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }


        [Fact]
        public void UnActive_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<History>
            {
                new History { Name = "History", Id = 2, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());

            controllerHistory.Active(800).ShouldBeOfType<NotFoundResult>();
            controllerHistory.Active(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Unactive_ShouldUnactiveHistory()
        {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<History>
            {
                new History { Name = "History", Id = 2, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());


            IActionResult result = controllerHistory.Active(2);
            mockRepository.Verify(d => d.Update(It.IsAny<History>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }

        [Fact]
        public void DeleteItem_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<History>
            {
                new History { Name = "History", Id = 2, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());


            controllerHistory.DeleteItem(800).ShouldBeOfType<NotFoundResult>();
            controllerHistory.DeleteItem(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void DeleteItem_ShouldDeleteHistory()
        {
            Mock<IRepository<History>> mockRepository = new Mock<IRepository<History>>();
            Mock<IRepository<EventHistory>> mockEventHistoryRepository = new Mock<IRepository<EventHistory>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();

            HistoryController controllerHistory = new HistoryController(mockRepository.Object, mockEventHistoryRepository.Object, mockBlobStorage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<History>
            {
                new History { Name = "History", Id = 2, Picture1Path = "path1", Picture2Path= "path2", Picture3Path = "path3", Text = "Text", isActive = true, isDefault= false, isDelete = false }

            }.AsQueryable());


            IActionResult result = controllerHistory.DeleteItem(2);
            mockRepository.Verify(d => d.Update(It.IsAny<History>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }


    }
}
