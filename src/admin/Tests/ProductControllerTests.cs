﻿using System.Collections.Generic;
using Xunit;
using Shouldly;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.BoardStyle;
using SpekSkates.Admin.Web.Modules.Contact;
using SpekSkates.Admin.Web.Modules.Contact.Controllers;
using SpekSkates.Admin.Web.Modules.Product;
using SpekSkates.Admin.Web.Modules.ProductCategory;
using SpekSkates.Admin.Web.Modules.ProductShape;
using SpekSkates.Admin.Web.Modules.ProductSize;
using SpekSkates.Admin.Web.Modules.Package;

namespace SpekSkates.Tests
{
    public class ProductControllerTests
    {
        [Fact]
        public void Index_ShouldReturnViewResult()
        {
            Mock<IRepository<ProductCategory>> mockCategoryRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<BoardStyle>> mockBoardStyleRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Package>> mockPackage = new Mock<IRepository<Package>>();

            ProductController controllerProduct = new ProductController(mockRepository.Object, mockCategoryRepository.Object,
                mockSizeRepository.Object, mockShapeRepository.Object, mockBoardStyleRepository.Object, mockBlobStorage.Object, mockPackage.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<Product>
            {
                new Product {Id=1, Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerProduct.Index();
            var model = (List<Product>)result.Model;

            model.Count.ShouldBe(1);
            model[0].Name.ShouldBe("Produit1");
            model[0].Price.ShouldBe(12.50M);
            model[0].History.ShouldBe("Histoire");
            model[0].Description.ShouldBe("description");
            model[0].Id.ShouldBe(1);
            model[0].ProductCategoryId.ShouldBe(1);
            model[0].ProductSizeId.ShouldBe(1);
            model[0].url.ShouldBe("lien vers produit");
            model[0].isActive.ShouldBe(true);
            model[0].isDelete.ShouldBe(false);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("views/product/index.cshtml");
        }



        [Fact]
        public void Edit_ShouldReturnViewModelDetails()
        {
            Mock<IRepository<ProductCategory>> mockCategoryRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<BoardStyle>> mockBoardStyleRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Package>> mockPackage = new Mock<IRepository<Package>>();

            ProductController controllerProduct = new ProductController(mockRepository.Object, mockCategoryRepository.Object,
                mockSizeRepository.Object, mockShapeRepository.Object, mockBoardStyleRepository.Object, mockBlobStorage.Object, mockPackage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                  new Product { Id= 2,Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerProduct.Edit(2);
            var model = (Admin.Web.Modules.Product.ViewModelDetail)result.Model;
            model.Product.Name.ShouldBe("Produit1");
            model.Product.Price.ShouldBe(12.50M);
            model.Product.History.ShouldBe("Histoire");
            model.Product.Description.ShouldBe("description");
            model.Product.Id.ShouldBe(2);
            model.Product.ProductCategoryId.ShouldBe(1);
            model.Product.ProductSizeId.ShouldBe(1);
            model.Product.url.ShouldBe("lien vers produit");
            model.Product.isActive.ShouldBe(true);
            model.Product.isDelete.ShouldBe(false);
            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Edit_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<Package>> mockPackage = new Mock<IRepository<Package>>();

            Mock<IRepository<ProductCategory>> mockCategoryRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<BoardStyle>> mockBoardStyleRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            ProductController controllerProduct = new ProductController(mockRepository.Object, mockCategoryRepository.Object,
                mockSizeRepository.Object, mockShapeRepository.Object, mockBoardStyleRepository.Object, mockBlobStorage.Object, mockPackage.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                  new Product { Id= 2,Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }

            }.AsQueryable());

            controllerProduct.Edit(800).ShouldBeOfType<NotFoundResult>();
            controllerProduct.Edit(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Edit_ShouldChangeProduct()
        {
            Mock<IRepository<ProductCategory>> mockCategoryRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<BoardStyle>> mockBoardStyleRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Package>> mockPackage = new Mock<IRepository<Package>>();

            ProductController controllerProduct = new ProductController(mockRepository.Object, mockCategoryRepository.Object,
                mockSizeRepository.Object, mockShapeRepository.Object, mockBoardStyleRepository.Object, mockBlobStorage.Object, mockPackage.Object);

            mockCategoryRepository.Setup(q => q.Get(1)).Returns(new List<ProductCategory>
            {
                  new ProductCategory { Id= 2,Name = "Produit1", CategoryType= CategoryType.Accessory, isDelete = false }

            }.AsQueryable());


            mockRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                  new Product { Id= 2,Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }

            }.AsQueryable());

            var mv = new Admin.Web.Modules.Product.ViewModelDetail
            {
                Product = new Product { Id = 2, Name = "Produit3", Price = 13.50M, History = "Histoire3", Description = "description3", ProductCategoryId = 1, ProductSizeId = 1, url = "lien vers produit3", isActive = true, isDelete = false, }
            };
            mv.SizeSelectId = "2_1";
            mv.ShapeSelectId = "2_1";
            IActionResult result = controllerProduct.Edit(2, mv);
            mockRepository.Verify(d => d.Update(It.IsAny<Product>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Delete_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<ProductCategory>> mockCategoryRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<BoardStyle>> mockBoardStyleRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Package>> mockPackage = new Mock<IRepository<Package>>();

            ProductController controllerProduct = new ProductController(mockRepository.Object, mockCategoryRepository.Object,
                mockSizeRepository.Object, mockShapeRepository.Object, mockBoardStyleRepository.Object, mockBlobStorage.Object, mockPackage.Object);
            mockRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                  new Product { Id= 2,Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }

            }.AsQueryable());


            controllerProduct.Deleteblob("blob", 101).ShouldBeOfType<NotFoundResult>();
            controllerProduct.Deleteblob("", 2).ShouldBeOfType<NotFoundResult>();
        }


        [Fact]
        public void Delete_ShouldDeleteABlob()
        {
            Mock<IRepository<ProductCategory>> mockCategoryRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<BoardStyle>> mockBoardStyleRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IRepository<Package>> mockPackage = new Mock<IRepository<Package>>();

            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            ProductController controllerProduct = new ProductController(mockRepository.Object, mockCategoryRepository.Object,
                mockSizeRepository.Object, mockShapeRepository.Object, mockBoardStyleRepository.Object, mockBlobStorage.Object, mockPackage.Object);
            mockRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                  new Product { Id= 2,Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }

            }.AsQueryable());

            IActionResult result = controllerProduct.Deleteblob("blob", 2);

            mockRepository.Verify(d => d.Save(It.IsAny<Product>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();
        }



        [Fact]
        public void Active_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<ProductCategory>> mockCategoryRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<BoardStyle>> mockBoardStyleRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Package>> mockPackage = new Mock<IRepository<Package>>();

            ProductController controllerProduct = new ProductController(mockRepository.Object, mockCategoryRepository.Object,
                mockSizeRepository.Object, mockShapeRepository.Object, mockBoardStyleRepository.Object, mockBlobStorage.Object, mockPackage.Object);
            mockRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                  new Product { Id= 2,Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = false, isDelete = false }

            }.AsQueryable());

            controllerProduct.Active(800).ShouldBeOfType<NotFoundResult>();
            controllerProduct.Active(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Active_ShouldActiveProduct()
        {
            Mock<IRepository<ProductCategory>> mockCategoryRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<BoardStyle>> mockBoardStyleRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Package>> mockPackage = new Mock<IRepository<Package>>();

            ProductController controllerProduct = new ProductController(mockRepository.Object, mockCategoryRepository.Object,
                mockSizeRepository.Object, mockShapeRepository.Object, mockBoardStyleRepository.Object, mockBlobStorage.Object, mockPackage.Object);
            mockRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                  new Product { Id= 2,Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = false, isDelete = false }

            }.AsQueryable());


            IActionResult result = controllerProduct.Active(2);
            mockRepository.Verify(d => d.Update(It.IsAny<Product>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }


        [Fact]
        public void UnActive_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<ProductCategory>> mockCategoryRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<BoardStyle>> mockBoardStyleRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Package>> mockPackage = new Mock<IRepository<Package>>();

            ProductController controllerProduct = new ProductController(mockRepository.Object, mockCategoryRepository.Object,
                mockSizeRepository.Object, mockShapeRepository.Object, mockBoardStyleRepository.Object, mockBlobStorage.Object, mockPackage.Object);
            mockRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                  new Product { Id= 2,Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }

            }.AsQueryable());

            controllerProduct.Active(800).ShouldBeOfType<NotFoundResult>();
            controllerProduct.Active(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Unactive_ShouldUnactiveProduct()
        {
            Mock<IRepository<ProductCategory>> mockCategoryRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<BoardStyle>> mockBoardStyleRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            Mock<IRepository<Package>> mockPackage = new Mock<IRepository<Package>>();
            ProductController controllerProduct = new ProductController(mockRepository.Object, mockCategoryRepository.Object,
                mockSizeRepository.Object, mockShapeRepository.Object, mockBoardStyleRepository.Object, mockBlobStorage.Object, mockPackage.Object);
            mockRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                  new Product { Id= 2,Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }

            }.AsQueryable());

            IActionResult result = controllerProduct.Active(2);
            mockRepository.Verify(d => d.Update(It.IsAny<Product>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }

        [Fact]
        public void DeleteItem_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<ProductCategory>> mockCategoryRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<BoardStyle>> mockBoardStyleRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IRepository<Package>> mockPackage = new Mock<IRepository<Package>>();

            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            ProductController controllerProduct = new ProductController(mockRepository.Object, mockCategoryRepository.Object,
                mockSizeRepository.Object, mockShapeRepository.Object, mockBoardStyleRepository.Object, mockBlobStorage.Object, mockPackage.Object);
            mockRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                  new Product { Id= 2,Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }

            }.AsQueryable());


            controllerProduct.DeleteItem(800).ShouldBeOfType<NotFoundResult>();
            controllerProduct.DeleteItem(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void DeleteItem_ShouldDeleteProduct()
        {
            Mock<IRepository<ProductCategory>> mockCategoryRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<BoardStyle>> mockBoardStyleRepository = new Mock<IRepository<BoardStyle>>();
            Mock<IRepository<Product>> mockRepository = new Mock<IRepository<Product>>();
            Mock<IRepository<Package>> mockPackage = new Mock<IRepository<Package>>();

            Mock<IAzureBlobStorage> mockBlobStorage = new Mock<IAzureBlobStorage>();
            ProductController controllerProduct = new ProductController(mockRepository.Object, mockCategoryRepository.Object,
                mockSizeRepository.Object, mockShapeRepository.Object, mockBoardStyleRepository.Object, mockBlobStorage.Object, mockPackage.Object);
            mockRepository.Setup(q => q.Get(2)).Returns(new List<Product>
            {
                  new Product { Id= 2,Name = "Produit1", Price = 12.50M, History = "Histoire", Description = "description", ProductCategoryId = 1, ProductSizeId = 1, url="lien vers produit", isActive = true, isDelete = false }

            }.AsQueryable());

            IActionResult result = controllerProduct.DeleteItem(2);
            mockRepository.Verify(d => d.Update(It.IsAny<Product>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }
    }
}
