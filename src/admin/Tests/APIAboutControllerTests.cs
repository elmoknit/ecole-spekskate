﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using SpekSkates.Admin.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Newtonsoft.Json;
using SpekSkates.Admin.Web.Modules.Cms;
using SpekSkates.Admin.Web.Modules.About;
using temp.Controllers;

namespace SpekSkates.Tests
{
    public class APIAboutControllerTests
    {

        [Fact]
        public void AboutUs_GetDefaultShouldReturnOk()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();

            apiAboutController apiControllerAboutUs = new apiAboutController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<AboutUs>
            {

                new AboutUs {
                    Name = "Accueil",
                    IntroductionText = "Introd",
                    Content = "Contenu",
                    ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2",
                    ImagePath3 = "ImagePath3",
                    Id = 1,
                    isDefault = true
                }


            }.AsQueryable());

            IActionResult result = apiControllerAboutUs.Default();
            var okResult = result as OkObjectResult;

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;
            var aboutUs = data.Value as AboutUs;

            aboutUs.Name.ShouldBe("Accueil");
            aboutUs.IntroductionText.ShouldBe("Introd");
            aboutUs.Content.ShouldBe("Contenu");
            aboutUs.ImagePath1.ShouldBe("ImagePath1");
            aboutUs.ImagePath2.ShouldBe("ImagePath2");
            aboutUs.ImagePath3.ShouldBe("ImagePath3");
        }

        [Fact]
        public void AboutUs_GetDefaultShouldReturnNotFound()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();

            apiAboutController apiControllerAboutUs = new apiAboutController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<AboutUs>
            {
            }.AsQueryable());

            IActionResult result = apiControllerAboutUs.Default();

            var okResult = result as NotFoundObjectResult;

            okResult.ShouldNotBeNull();

            okResult.StatusCode.ShouldBe(404);

            var json = okResult.Value as JsonResult;
            json.Value.ShouldBe("Not found");
        }

        [Fact]
        public void AboutUs_GetCustomShouldReturnOk()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();

            apiAboutController apiControllerAboutUs = new apiAboutController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<AboutUs>
            {

                new AboutUs {
                    Name = "Custom",
                    IntroductionText = "Introd",
                    Content = "Contenu",
                    ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2",
                    ImagePath3 = "ImagePath3",
                    Id = 1,
                    isDefault = true
                }
            }.AsQueryable());

            IActionResult result = apiControllerAboutUs.Custom("Custom");

            var okResult = result as OkObjectResult;

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;
            var aboutUs = data.Value as AboutUs;

            aboutUs.Name.ShouldBe("Custom");
            aboutUs.IntroductionText.ShouldBe("Introd");
            aboutUs.Content.ShouldBe("Contenu");
            aboutUs.ImagePath1.ShouldBe("ImagePath1");
            aboutUs.ImagePath2.ShouldBe("ImagePath2");
            aboutUs.ImagePath3.ShouldBe("ImagePath3");

        }

        [Fact]
        public void AboutUs_GetCustomShouldReturnNotFound()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();

            apiAboutController apiControllerAboutUs = new apiAboutController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<AboutUs>
            {

                new AboutUs {
                    Name = "Accueil",
                    IntroductionText = "Introd",
                    Content = "Contenu",
                    ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2",
                    ImagePath3 = "ImagePath3",
                    Id = 1,
                    isDefault = true
                }
            }.AsQueryable());

            IActionResult result = apiControllerAboutUs.Custom("Custom123");
            var okResult = result as NotFoundObjectResult;

            okResult.ShouldNotBeNull();

            okResult.StatusCode.ShouldBe(404);

        }

        [Fact]
        public void AboutUs_GetAllPageCustomNameShouldReturnOk()
        {
            Mock<IRepository<AboutUs>> mockRepository = new Mock<IRepository<AboutUs>>();

            apiAboutController apiControllerAboutUs = new apiAboutController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<AboutUs>
            {

                new AboutUs {
                    Name = "Custom1",
                    IntroductionText = "Introd",
                    Content = "Contenu",
                    ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2",
                    ImagePath3 = "ImagePath3",
                    Id = 1,
                    isDefault = false,
                    isActive = true,
                    isDelete = false
                },
                new AboutUs {
                    Name = "Custom2",
                    IntroductionText = "Introd",
                    Content = "Contenu",
                    ImagePath1 = "ImagePath1",
                    ImagePath2 = "ImagePath2",
                    ImagePath3 = "ImagePath3",
                    Id = 1,
                    isDefault = false,
                    isActive = true,
                    isDelete = false
                }
            }.AsQueryable());

            IActionResult result = apiControllerAboutUs.GetAllPageCustomName();
            var okResult = result as OkObjectResult;

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;

            var pageName = (List<string>)data.Value;
            pageName[0].ShouldBe("Custom1");
            pageName[1].ShouldBe("Custom2");

        }
    }
}
