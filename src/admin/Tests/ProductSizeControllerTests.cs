﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using SpekSkates.Admin.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web;
using SpekSkates.Admin.Web.Modules.HomePage;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.Slider;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System.IO;
using Microsoft.AspNetCore.Mvc.Rendering;
using SpekSkates.Admin.Web.Modules.ProductCategory;
using SpekSkates.Admin.Web.Modules.ProductSize;
using SpekSkates.Admin.Web.Modules.ProductSize.Controllers;
using SpekSkates.Admin.Web.Modules.ProductSize.ViewModels;

namespace SpekSkates.Admin.Tests
{
    public class ProductSizeControllerTest
    {

        [Fact]
        public void Index_ShouldReturnViewResult()
        {
            Mock<IRepository<ProductSize>> mockRepository = new Mock<IRepository<ProductSize>>();
            Mock <IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductSizeController controllerProductSize = new ProductSizeController(mockRepository.Object, mockProductCategory.Object);
            mockRepository.Setup(q => q.GetAll()).Returns(new List<ProductSize>
            {
                new ProductSize {
                    Name = "Cat1",
                    Id= 1
                },
                new ProductSize {
                    Name = "Cat2",
                    Id= 2
                }

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerProductSize.Index();
            var model = (List<ProductSize>)result.Model;

            model.Count.ShouldBe(2);
            model[0].Name.ShouldBe("Cat1");
            model[0].Id.ShouldBe(1);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("views/ProductSize/index.cshtml");
        }

        [Fact]
        public void Create_ShouldReturnViewResult()
        {
            Mock<IRepository<ProductSize>> mockRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductSizeController controllerProductSize = new ProductSizeController(mockRepository.Object, mockProductCategory.Object);


            ViewResult result = (ViewResult)controllerProductSize.Create();


            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Create_ShouldCreateAProductSize()
        {
            Mock<IRepository<ProductSize>> mockRepository = new Mock<IRepository<ProductSize>>();

            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductSizeController controllerProductSize = new ProductSizeController(mockRepository.Object, mockProductCategory.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<ProductSize>
            {
                new ProductSize {
                    Name = "Cat1",
                    Id= 1
                },
                new ProductSize {
                    Name = "Cat2",
                    Id= 2
                }

            }.AsQueryable());

            ProductSize productSize = new ProductSize
            {
                Name = "Cat3",
                Id = 3
            };
            var mv = new ViewModel
            {
                ProductSize = productSize
            };
            IActionResult result = controllerProductSize.Create(mv);

            mockRepository.Verify(d => d.Save(It.IsAny<ProductSize>()), Times.Once());

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Edit_ShouldReturnViewResult()
        {
            Mock<IRepository<ProductSize>> mockRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductSizeController controllerProductSize = new ProductSizeController(mockRepository.Object, mockProductCategory.Object);

            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductSize>
            {
                new ProductSize {
                    Name = "Cat5",
                    Id= 5,
                    ProductCategoriesProductSizes = new List<ProductCategoryProductSize>()
                }

            }.AsQueryable());
          
         
            ViewResult result = (ViewResult)controllerProductSize.Edit(5);

            var model = (ViewModel)result.Model;

            model.ProductSize.Name.ShouldBe("Cat5");
            model.ProductSize.Id.ShouldBe(5);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Edit_ShouldEditAProductSize()
        {
            Mock<IRepository<ProductSize>> mockRepository = new Mock<IRepository<ProductSize>>();

            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductSizeController controllerProductSize = new ProductSizeController(mockRepository.Object, mockProductCategory.Object);


            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductSize>
            {
                new ProductSize {
                    Name = "Cat5",
                    Id= 5,
                    ProductCategoriesProductSizes = new List<ProductCategoryProductSize>()
                }

            }.AsQueryable());
            ViewModel mv = new ViewModel();
            ProductSize ProductSize = new ProductSize
            {
                Name = "Cat2",
                Id = 5
            };
            mv.ProductSize = ProductSize;
            IActionResult result = controllerProductSize.Edit(5, mv);

            mockRepository.Verify(d => d.Update(It.IsAny<ProductSize>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Edit_ShouldAddProductCategory()
        {
            Mock<IRepository<ProductSize>> mockRepository = new Mock<IRepository<ProductSize>>();

            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductSizeController controllerProductSize = new ProductSizeController(mockRepository.Object, mockProductCategory.Object);


            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductSize>
            {
                new ProductSize {
                    Name = "Cat5",
                    Id= 5,
                    ProductCategoriesProductSizes = new List<ProductCategoryProductSize>()
                }

            }.AsQueryable());
            ViewModel mv = new ViewModel();
            mv.ProductSize = new ProductSize
            {
                Name = "Cat5",
                Id = 5,
                ProductCategoriesProductSizes = new List<ProductCategoryProductSize> { new ProductCategoryProductSize()
                {
                    ProductCategory = new ProductCategory()
                    {
                        Id= 1,
                        Name = "Cat1",
                        CategoryType = CategoryType.Board,
                        ProductCategoriesProductSizes = new List<ProductCategoryProductSize>()
                    },
                    ProductCategoryId = 1,
                    ProductSize = new ProductSize {
                    Name = "Cat5",
                    Id= 5,
                    ProductCategoriesProductSizes = new List<ProductCategoryProductSize>()
                },
                    ProductSizeId = 5
                } }
            };

            
            IActionResult result = controllerProductSize.Edit(5, mv);

            mockRepository.Verify(d => d.Update(It.IsAny<ProductSize>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Edit_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<ProductSize>> mockRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductSizeController controllerProductSize = new ProductSizeController(mockRepository.Object, mockProductCategory.Object);


            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductSize>
            {
                new ProductSize {
                    Name = "Cat5",
                    Id= 5
                }

            }.AsQueryable());

            controllerProductSize.Edit(101).ShouldBeOfType<NotFoundResult>();
            controllerProductSize.Edit(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Delete_ShouldReturnViewResult()
        {
            Mock<IRepository<ProductSize>> mockRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductSizeController controllerProductSize = new ProductSizeController(mockRepository.Object, mockProductCategory.Object);


            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductSize>
            {
                new ProductSize {
                    Name = "Cat5",
                    Id= 5
                }

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerProductSize.Delete(5);

            var model = (ProductSize)result.Model;
            model.Name.ShouldBe("Cat5");
            model.Id.ShouldBe(5);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Delete_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<ProductSize>> mockRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductSizeController controllerProductSize = new ProductSizeController(mockRepository.Object, mockProductCategory.Object);

            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductSize>
            {
                new ProductSize {
                    Name = "Cat5",
                    Id= 5
                }

            }.AsQueryable());

            controllerProductSize.Delete(101).ShouldBeOfType<NotFoundResult>();
            controllerProductSize.Delete(null).ShouldBeOfType<NotFoundResult>();
        }


        [Fact]
        public void Delete_ShouldDeleteAProductSize()
        {
            Mock<IRepository<ProductSize>> mockRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductCategory>> mockProductCategory = new Mock<IRepository<ProductCategory>>();

            ProductSizeController controllerProductSize = new ProductSizeController(mockRepository.Object, mockProductCategory.Object);

            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductSize>
            {
                new ProductSize {
                    Name = "Cat5",
                    Id= 5
                }

            }.AsQueryable());


            IActionResult result = controllerProductSize.DeleteConfirmed(5);


            result.ShouldBeOfType<RedirectToActionResult>();
        }

    }
}
