﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using SpekSkates.Admin.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web;
using SpekSkates.Admin.Web.Modules.HomePage;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.Slider;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System.IO;
using SpekSkates.Admin.Web.Modules.ProductCategory;
using SpekSkates.Admin.Web.Modules.ProductCategory.Controllers;
using SpekSkates.Admin.Web.Modules.ProductCategory.ViewModels;
using SpekSkates.Admin.Web.Modules.ProductShape;
using SpekSkates.Admin.Web.Modules.ProductSize;
using SpekSkates.Admin.Web.Modules.Product;

namespace SpekSkates.Admin.Tests
{
    public class ProductCategoryControllerTest
    {

        [Fact]
        public void Index_ShouldReturnViewResult()
        {
            Mock<IRepository<ProductCategory>> mockRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockProductSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockProductShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            ProductCategoryController controllerProductCategory = new ProductCategoryController(mockRepository.Object, mockProductSizeRepository.Object, mockProductShapeRepository.Object, mockProductRepository.Object);
            mockRepository.Setup(q => q.GetAll()).Returns(new List<ProductCategory>
            {
                new ProductCategory {
                    Name = "Cat1",
                    Id= 1,
                    CategoryType = CategoryType.Accessory
                },
                new ProductCategory {
                    Name = "Cat2",
                    Id= 2,
                    CategoryType = CategoryType.Board
                }

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerProductCategory.Index();
            var model = (List<ProductCategory>)result.Model;

            model.Count.ShouldBe(2);
            model[0].Name.ShouldBe("Cat1");
            model[0].Id.ShouldBe(1);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("views/productCategory/index.cshtml");
        }

        [Fact]
        public void Create_ShouldReturnViewResult()
        {
            Mock<IRepository<ProductCategory>> mockRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockProductSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockProductShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            ProductCategoryController controllerProductCategory = new ProductCategoryController(mockRepository.Object, mockProductSizeRepository.Object, mockProductShapeRepository.Object, mockProductRepository.Object);

            ViewResult result = (ViewResult)controllerProductCategory.Create();


            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Create_ShouldCreateAProductCategory()
        {
            Mock<IRepository<ProductCategory>> mockRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockProductSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockProductShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            ProductCategoryController controllerProductCategory = new ProductCategoryController(mockRepository.Object, mockProductSizeRepository.Object, mockProductShapeRepository.Object, mockProductRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<ProductCategory>
            {
                new ProductCategory {
                    Name = "Cat1",
                    Id= 1,
                    CategoryType = CategoryType.Board
                },
                new ProductCategory {
                    Name = "Cat2",
                    Id= 2,
                    CategoryType = CategoryType.Accessory
                }

            }.AsQueryable());

            ProductCategory productCategory = new ProductCategory
            {
                Name = "Cat3",
                Id = 3,
                CategoryType = CategoryType.Board
            };
            var mv = new ViewModel
            {
                ProductCategory = productCategory
            };


            IActionResult result = controllerProductCategory.Create(mv);

            mockRepository.Verify(d => d.Save(It.IsAny<ProductCategory>()), Times.Once());

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Edit_ShouldReturnViewResult()
        {
            Mock<IRepository<ProductCategory>> mockRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockProductSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockProductShapeRepository = new Mock<IRepository<ProductShape>>();
            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            ProductCategoryController controllerProductCategory = new ProductCategoryController(mockRepository.Object, mockProductSizeRepository.Object, mockProductShapeRepository.Object, mockProductRepository.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductCategory>
            {
                new ProductCategory {
                    Name = "Cat5",
                    Id= 5,
                    CategoryType = CategoryType.Board,
                    ProductCategoriesProductSizes = new List<ProductCategoryProductSize>(),
                    ProductCategoriesProductShapes = new List<ProductCategoryProductShape>(),
                    RelatedCategories = new List<RelationCatCat>()
                }

            }.AsQueryable());
            ViewResult result = (ViewResult)controllerProductCategory.Edit(5);

            var model = (ViewModel)result.Model;
            model.ProductCategory.Name.ShouldBe("Cat5");
            model.ProductCategory.Id.ShouldBe(5);
            model.ProductCategory.CategoryType.ShouldBe(CategoryType.Board);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Edit_ShouldEditAProductCategory()
        {
            Mock<IRepository<ProductCategory>> mockRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockProductSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockProductShapeRepository = new Mock<IRepository<ProductShape>>();

            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            ProductCategoryController controllerProductCategory = new ProductCategoryController(mockRepository.Object, mockProductSizeRepository.Object, mockProductShapeRepository.Object, mockProductRepository.Object);
            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductCategory>
            {
                new ProductCategory {
                    Name = "Cat5",
                    Id= 5,
                    CategoryType = CategoryType.Board,
                    ProductCategoriesProductSizes = new List<ProductCategoryProductSize>(),
                    ProductCategoriesProductShapes = new List<ProductCategoryProductShape>(),
                    RelatedCategories = new List<RelationCatCat>()
                }

            }.AsQueryable());

            ViewModel mv = new ViewModel();
            mv.ProductCategory = new ProductCategory
            {
                Name = "Cat2",
                Id = 5,
                CategoryType = CategoryType.Board,
                ProductCategoriesProductSizes = new List<ProductCategoryProductSize>(),
                ProductCategoriesProductShapes = new List<ProductCategoryProductShape>(),
                RelatedCategories = new List<RelationCatCat>()
            };

            IActionResult result = controllerProductCategory.Edit(5, mv);

            mockRepository.Verify(d => d.Update(It.IsAny<ProductCategory>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Edit_ShouldAddProductSize()
        {
            Mock<IRepository<ProductCategory>> mockRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockProductSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockProductShapeRepository = new Mock<IRepository<ProductShape>>();

            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            ProductCategoryController controllerProductCategory = new ProductCategoryController(mockRepository.Object, mockProductSizeRepository.Object, mockProductShapeRepository.Object, mockProductRepository.Object);

    mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductCategory>
            {
                new ProductCategory {
                    Name = "Cat5",
                    Id= 5,
                    CategoryType = CategoryType.Board,
                    ProductCategoriesProductSizes = new List<ProductCategoryProductSize>(),
                    ProductCategoriesProductShapes = new List<ProductCategoryProductShape>(),
                    RelatedCategories = new List<RelationCatCat>()
                }

            }.AsQueryable());
            ViewModel mv = new ViewModel();
            mv.ProductCategory = new ProductCategory
            {
                Name = "Cat5",
                Id = 5,
                ProductCategoriesProductSizes = new List<ProductCategoryProductSize> { new ProductCategoryProductSize()
                {
                    ProductCategory = new ProductCategory()
                    {
                        Id= 1,
                        Name = "Cat1",
                        CategoryType = CategoryType.Board,
                        ProductCategoriesProductSizes = new List<ProductCategoryProductSize>(),
                        ProductCategoriesProductShapes = new List<ProductCategoryProductShape>()
                    },
                    ProductCategoryId = 1,
                    ProductSize = new ProductSize {
                    Name = "Cat5",
                    Id= 5,
                    ProductCategoriesProductSizes = new List<ProductCategoryProductSize>()
                },
                    ProductSizeId = 5
                } }
            };


            IActionResult result = controllerProductCategory.Edit(5, mv);

            mockRepository.Verify(d => d.Update(It.IsAny<ProductCategory>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Edit_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<ProductCategory>> mockRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockProductSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockProductShapeRepository = new Mock<IRepository<ProductShape>>();

            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            ProductCategoryController controllerProductCategory = new ProductCategoryController(mockRepository.Object, mockProductSizeRepository.Object, mockProductShapeRepository.Object, mockProductRepository.Object);

            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductCategory>
            {
                new ProductCategory {
                    Name = "Cat5",
                    Id= 5,
                    CategoryType = CategoryType.Board
                }

            }.AsQueryable());

            controllerProductCategory.Edit(101).ShouldBeOfType<NotFoundResult>();
            controllerProductCategory.Edit(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Delete_ShouldReturnViewResult()
        {
            Mock<IRepository<ProductCategory>> mockRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockProductSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockProductShapeRepository = new Mock<IRepository<ProductShape>>();

            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            ProductCategoryController controllerProductCategory = new ProductCategoryController(mockRepository.Object, mockProductSizeRepository.Object, mockProductShapeRepository.Object, mockProductRepository.Object);

            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductCategory>
            {
                new ProductCategory {
                    Name = "Cat5",
                    Id= 5,
                    CategoryType = CategoryType.Accessory
                }

            }.AsQueryable());

            ViewResult result = (ViewResult)controllerProductCategory.Delete(5);

            var model = (ViewModelSupression)result.Model;
            model.ProductCategory.Name.ShouldBe("Cat5");
            model.ProductCategory.Id.ShouldBe(5);

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Delete_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<ProductCategory>> mockRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockProductSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockProductShapeRepository = new Mock<IRepository<ProductShape>>();

            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            ProductCategoryController controllerProductCategory = new ProductCategoryController(mockRepository.Object, mockProductSizeRepository.Object, mockProductShapeRepository.Object, mockProductRepository.Object);

            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductCategory>
            {
                new ProductCategory {
                    Name = "Cat5",
                    Id= 5,
                    CategoryType = CategoryType.Accessory
                }

            }.AsQueryable());

            controllerProductCategory.Delete(101).ShouldBeOfType<NotFoundResult>();
            controllerProductCategory.Delete(null).ShouldBeOfType<NotFoundResult>();
        }


        [Fact]
        public void Delete_ShouldDeleteAProductCategory()
        {
            Mock<IRepository<ProductCategory>> mockRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockProductSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockProductShapeRepository = new Mock<IRepository<ProductShape>>();

            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            ProductCategoryController controllerProductCategory = new ProductCategoryController(mockRepository.Object, mockProductSizeRepository.Object, mockProductShapeRepository.Object, mockProductRepository.Object);

            var productCategory = new ProductCategory
            {
                Name = "Cat5",
                Id = 5,
                CategoryType = CategoryType.Accessory
            };

            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductCategory>
            {
                productCategory
            }.AsQueryable());
            var mv = new ViewModelSupression
            {
                ProductCategory = productCategory,
                NameConfirm = "Cat5",
            };

            IActionResult result = controllerProductCategory.DeleteConfirmed(mv);


            result.ShouldBeOfType<RedirectToActionResult>();
        }

        [Fact]
        public void Edit_ShouldAddARelatedCategories()
        {
            Mock<IRepository<ProductCategory>> mockRepository = new Mock<IRepository<ProductCategory>>();
            Mock<IRepository<ProductSize>> mockProductSizeRepository = new Mock<IRepository<ProductSize>>();
            Mock<IRepository<ProductShape>> mockProductShapeRepository = new Mock<IRepository<ProductShape>>();

            Mock<IRepository<Product>> mockProductRepository = new Mock<IRepository<Product>>();

            ProductCategoryController controllerProductCategory = new ProductCategoryController(mockRepository.Object, mockProductSizeRepository.Object, mockProductShapeRepository.Object, mockProductRepository.Object);

            mockRepository.Setup(q => q.Get(5)).Returns(new List<ProductCategory>
            {
                new ProductCategory {
                    Name = "Cat5",
                    Id= 5,
                    CategoryType = CategoryType.Board,
                    ProductCategoriesProductSizes = new List<ProductCategoryProductSize>(),
                    ProductCategoriesProductShapes = new List<ProductCategoryProductShape>(),
                    RelatedCategories = new List<RelationCatCat>()
                }

            }.AsQueryable());
            ViewModel mv = new ViewModel();

            mv.ProductCategory = new ProductCategory
            {
                Name = "Cat5",
                Id = 5,
                RelatedCategories = new List<RelationCatCat> { new RelationCatCat()
                {
                    ProductCategory1 = new ProductCategory()
                    {
                        Id= 1,
                        Name = "Cat1",
                        CategoryType = CategoryType.Board,
                        ProductCategoriesProductSizes = new List<ProductCategoryProductSize>(),
                        ProductCategoriesProductShapes = new List<ProductCategoryProductShape>()
                    },
                    ProductCategory1Id = 1,
                    ProductCategory2 = new ProductCategory() {
                    Name = "Cat5",
                    Id= 5,
                    CategoryType = CategoryType.Board,
                    ProductCategoriesProductSizes = new List<ProductCategoryProductSize>(),
                    ProductCategoriesProductShapes = new List<ProductCategoryProductShape>()
                },
                    ProductCategory2Id = 6
                } }
            };


            IActionResult result = controllerProductCategory.Edit(5, mv);

            mockRepository.Verify(d => d.Update(It.IsAny<ProductCategory>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();
        }

    }
}