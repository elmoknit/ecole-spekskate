﻿using System.Collections.Generic;
using Xunit;
using Shouldly;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.Contact;
using SpekSkates.Admin.Web.Modules.Contact.Controllers;

namespace SpekSkates.Tests
{
    public class ContactControllerTests
    {


        [Fact]
        public void Default_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<Contact>> mockRepository = new Mock<IRepository<Contact>>();
           
            ContactController controllerContact = new ContactController(mockRepository.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<Contact>
            {
                new Contact { Name = "Accueil", Content = "Contenu", Id = 2, isDefault = false }

            }.AsQueryable());

            controllerContact.Default(800).ShouldBeOfType<NotFoundResult>();
            controllerContact.Default(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Default_ShouldMakeDefaultContactPage()
        {
            Mock<IRepository<Contact>> mockRepository = new Mock<IRepository<Contact>>();

            ContactController controllerContact = new ContactController(mockRepository.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<Contact>
            {
                new Contact { Name = "Accueil", Content = "Contenu", Id = 2, isDefault = false }

            }.AsQueryable());

            IActionResult result = controllerContact.Default(2);
            mockRepository.Verify(d => d.Update(It.IsAny<Contact>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }

        [Fact]
        public void Active_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<Contact>> mockRepository = new Mock<IRepository<Contact>>();

            ContactController controllerContact = new ContactController(mockRepository.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<Contact>
            {
                new Contact { Name = "Accueil", Content = "Contenu", Id = 2, isActive = false }

            }.AsQueryable());


            controllerContact.Active(800).ShouldBeOfType<NotFoundResult>();
            controllerContact.Active(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Active_ShouldActiveContactPage()
        {
            Mock<IRepository<Contact>> mockRepository = new Mock<IRepository<Contact>>();

            ContactController controllerContact = new ContactController(mockRepository.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<Contact>
            {
                new Contact { Name = "Accueil", Content = "Contenu", Id = 2, isActive = false }

            }.AsQueryable());

            IActionResult result = controllerContact.Active(2);
            mockRepository.Verify(d => d.Update(It.IsAny<Contact>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }


        [Fact]
        public void UnActive_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<Contact>> mockRepository = new Mock<IRepository<Contact>>();

            ContactController controllerContact = new ContactController(mockRepository.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<Contact>
            {
                new Contact { Name = "Accueil", Content = "Contenu", Id = 2, isActive = true }

            }.AsQueryable());

            controllerContact.Active(800).ShouldBeOfType<NotFoundResult>();
            controllerContact.Active(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void Unactive_ShouldUnactiveContactPage()
        {
            Mock<IRepository<Contact>> mockRepository = new Mock<IRepository<Contact>>();

            ContactController controllerContact = new ContactController(mockRepository.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<Contact>
            {
                new Contact { Name = "Accueil", Content = "Contenu", Id = 2, isActive = true }

            }.AsQueryable());
            IActionResult result = controllerContact.Active(2);
            mockRepository.Verify(d => d.Update(It.IsAny<Contact>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }

        [Fact]
        public void DeleteItem_ShouldReturnNotFoundResult()
        {
            Mock<IRepository<Contact>> mockRepository = new Mock<IRepository<Contact>>();

            ContactController controllerContact = new ContactController(mockRepository.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<Contact>
            {
                new Contact { Name = "Accueil", Content = "Contenu", Id = 2, isActive = true }

            }.AsQueryable());

            controllerContact.DeleteItem(800).ShouldBeOfType<NotFoundResult>();
            controllerContact.DeleteItem(null).ShouldBeOfType<NotFoundResult>();
        }

        [Fact]
        public void DeleteItem_ShouldDeleteContactPage()
        {
            Mock<IRepository<Contact>> mockRepository = new Mock<IRepository<Contact>>();

            ContactController controllerContact = new ContactController(mockRepository.Object);

            mockRepository.Setup(q => q.Get(2)).Returns(new List<Contact>
            {
                new Contact { Name = "Accueil", Content = "Contenu",  Id = 2, isActive = true }

            }.AsQueryable());

            IActionResult result = controllerContact.DeleteItem(2);
            mockRepository.Verify(d => d.Update(It.IsAny<Contact>()), Times.AtLeast(1));

            result.ShouldBeOfType<RedirectToActionResult>();

        }

    }
}
