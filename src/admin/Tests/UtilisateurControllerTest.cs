﻿using Microsoft.AspNetCore.Identity;
using Moq;
using SpekSkates.Admin.Web.Modules.Page;
using SpekSkates.Admin.Web.Modules.Utilisateur.Email;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using SpekSkates.Admin.Tests.Manager;
using Microsoft.AspNetCore.Mvc;
using Shouldly;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Linq;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Models;

namespace SpekSkates.Admin.Tests
{
    public class UtilisateurControllerTest
    {

        [Fact]
        public void Index_ShouldReturnViewResult()
        {
            var objUserManager = new Mock<FakeUserManager>().Object;
            var objSignInManager = new Mock<FakeSignInManager>().Object;
            var messageService = new Mock<IMessageService>().Object;
            var UtilisateurController = new UtilisateurController(objUserManager, objSignInManager, messageService);


            ViewResult result = (ViewResult)UtilisateurController.Index();

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("Index");

        }

        [Fact]
        public void Login_ShouldReturnViewResult()
        {
            var objUserManager = new Mock<FakeUserManager>().Object;
            var objSignInManager = new Mock<FakeSignInManager>().Object;
            var messageService = new Mock<IMessageService>().Object;
            var UtilisateurController = new UtilisateurController(objUserManager, objSignInManager, messageService);


            ViewResult result = (ViewResult)UtilisateurController.Login();

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("Login");
        }

        [Fact]
        public  void Login_ShouldNotLoginSomeoneSuccesfully()
        {
            var messageService = new Mock<IMessageService>().Object;

            var users = new List<IdentityUser>
            {
                new IdentityUser
                {
                    UserName = "Test",
                    Id = Guid.NewGuid().ToString(),
                    Email = "test@test.it"
                }

            }.AsQueryable();

            var fakeUserManager = new Mock<FakeUserManager>();

            fakeUserManager.Setup(x => x.Users)
                .Returns(users);

           
            var signInManager = new Mock<FakeSignInManager>();

            signInManager.Setup(x => x.IsSignedIn(It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var UtilisateurController = new UtilisateurController(fakeUserManager.Object, signInManager.Object, messageService);

            UtilisateurController.ControllerContext = new ControllerContext();
            UtilisateurController.ControllerContext.HttpContext = new DefaultHttpContext();

            ViewResult result = (ViewResult)UtilisateurController.Login("test@test.it", "Qwer1@", true);

            // L'utilisateur dans le contexte ne doit pas etre nulle
            UtilisateurController.HttpContext.User.Identity.IsAuthenticated.ShouldBeFalse();

            result.ShouldBeOfType<ViewResult>()
               .ViewData.ShouldNotBeNull();
        }

        [Fact]
        public void Login_ShouldLoginSomeoneSuccesfully()
        {
            var messageService = new Mock<IMessageService>().Object;
            
            var users = new List<IdentityUser>
            {
                new IdentityUser
                {
                    UserName = "Test",
                    Id = Guid.NewGuid().ToString(),
                    Email = "test@test.it"
                }

            }.AsQueryable();

            var fakeUserManager = new Mock<FakeUserManager>();

            fakeUserManager.Setup(x => x.Users)
                .Returns(users);


          //  fakeUserManager.Setup(x => x.DeleteAsync(It.IsAny<IdentityUser>()))
          //   .ReturnsAsync(IdentityResult.Success);
          //  fakeUserManager.Setup(x => x.CreateAsync(It.IsAny<IdentityUser>(), It.IsAny<string>()))
          //  .ReturnsAsync(IdentityResult.Success);
          //  fakeUserManager.Setup(x => x.UpdateAsync(It.IsAny<IdentityUser>()))
          //.ReturnsAsync(IdentityResult.Success);
            fakeUserManager.Setup(x => x.FindByEmailAsync(It.IsAny<String>()))
             .ReturnsAsync(users.First());

            var signInManager = new Mock<FakeSignInManager>();

            signInManager.Setup(
             x => x.PasswordSignInAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .ReturnsAsync(Microsoft.AspNetCore.Identity.SignInResult.Success);
            signInManager.Setup(x => x.IsSignedIn(It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var UtilisateurController = new UtilisateurController(fakeUserManager.Object, signInManager.Object, messageService);

            UtilisateurController.ControllerContext = new ControllerContext();
            UtilisateurController.ControllerContext.HttpContext = new DefaultHttpContext();

            UtilisateurController.Login("test@test.it", "Qwer1@", true);

            // L'utilisateur dans le contexte ne doit pas etre nulle
            UtilisateurController.HttpContext.User.ShouldNotBeNull();

        }

        [Fact]
        public void ForgotPassword_ShouldReturnViewResult()
        {
            var objUserManager = new Mock<FakeUserManager>().Object;
            var objSignInManager = new Mock<FakeSignInManager>().Object;
            var messageService = new Mock<IMessageService>().Object;
            var UtilisateurController = new UtilisateurController(objUserManager, objSignInManager, messageService);


            ViewResult result = (ViewResult)UtilisateurController.ForgotPassword();

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("ForgotPassword");

        }

        [Fact]
        public void AddAdmin_ShouldReturnViewResult()
        {
            var objUserManager = new Mock<FakeUserManager>().Object;
            var objSignInManager = new Mock<FakeSignInManager>().Object;
            var messageService = new Mock<IMessageService>().Object;
            var UtilisateurController = new UtilisateurController(objUserManager, objSignInManager, messageService);


            ViewResult result = (ViewResult)UtilisateurController.AddAdmin();

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("AddAdmin");

        }

        [Fact]
        public void ResetPassword_ShouldReturnViewResult()
        {
            var objUserManager = new Mock<FakeUserManager>().Object;
            var objSignInManager = new Mock<FakeSignInManager>().Object;
            var messageService = new Mock<IMessageService>().Object;
            var UtilisateurController = new UtilisateurController(objUserManager, objSignInManager, messageService);


            ViewResult result = (ViewResult)UtilisateurController.ResetPassword("String reprsentant un token, la valeur est pas importante dans ce test");

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("ResetPassword");

        }

        [Fact]
        public void ResetPassword_ShouldReturnView()
        {
            var messageService = new Mock<IMessageService>().Object;

            var users = new List<IdentityUser>
            {
                new IdentityUser
                {
                    UserName = "Test",
                    Id = Guid.NewGuid().ToString(),
                    Email = "test@test.it"
                }

            }.AsQueryable();

            ResetPasswordViewModel model = null;
            

            var fakeUserManager = new Mock<FakeUserManager>();

            fakeUserManager.Setup(x => x.Users)
                .Returns(users);

            var signInManager = new Mock<FakeSignInManager>();

          
            var UtilisateurController = new UtilisateurController(fakeUserManager.Object, signInManager.Object, messageService);

            UtilisateurController.ControllerContext = new ControllerContext();
            UtilisateurController.ControllerContext.HttpContext = new DefaultHttpContext();

            ViewResult result = (ViewResult)UtilisateurController.ResetPassword(model);


            result.ShouldBeOfType<ViewResult>()
    .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("ResetPassword");
        }

        [Fact]
        public void ResetPassword_ShouldReturnContent()
        {
            var messageService = new Mock<IMessageService>().Object;

            var users = new List<IdentityUser>
            {
                new IdentityUser
                {
                    UserName = "Test",
                    Id = Guid.NewGuid().ToString(),
                    Email = "test@test.it"
                }

            }.AsQueryable();

            var model = new ResetPasswordViewModel
            {
                Token = "a",
                Email = "aaaa@aaa.com",
                Password = "Qwer1@",
                ConfirmPassword = "Qwer1@"
            };
                
            

            var fakeUserManager = new Mock<FakeUserManager>();

            fakeUserManager.Setup(x => x.Users)
                .Returns(users);


            fakeUserManager.Setup(x => x.FindByNameAsync(It.IsAny<String>()))
             .ReturnsAsync(users.First());

            fakeUserManager.Setup(x => x.ResetPasswordAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(Microsoft.AspNetCore.Identity.IdentityResult.Success);


            var signInManager = new Mock<FakeSignInManager>();

            var UtilisateurController = new UtilisateurController(fakeUserManager.Object, signInManager.Object, messageService);

            UtilisateurController.ControllerContext = new ControllerContext();
            UtilisateurController.ControllerContext.HttpContext = new DefaultHttpContext();

            ViewResult result = (ViewResult)UtilisateurController.ResetPassword(model);

            result.ShouldBeOfType<ViewResult>()
    .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("Index");
        }

        [Fact]
        public void Logout_ShouldReturnViewResult()
        {
            var objUserManager = new Mock<FakeUserManager>().Object;
            var objSignInManager = new Mock<FakeSignInManager>().Object;
            var messageService = new Mock<IMessageService>().Object;
            var UtilisateurController = new UtilisateurController(objUserManager, objSignInManager, messageService);


            ViewResult result = (ViewResult)UtilisateurController.ResetPassword("String reprsentant un token, la valeur est pas importante dans ce test");

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("ResetPassword");

        }

        [Fact]
        public void Index_ShouldReturnViewResultWithAdmin()
        {
            var objUserManager = new Mock<FakeUserManager>().Object;
            var objSignInManager = new Mock<FakeSignInManager>().Object;
            var messageService = new Mock<IMessageService>().Object;
            var UtilisateurController = new UtilisateurController(objUserManager, objSignInManager, messageService);


            ViewResult result = (ViewResult)UtilisateurController.Index();

            result.ShouldBeOfType<ViewResult>()
                .ViewData.ShouldNotBeNull();
            result.ViewName.ShouldContain("Index");

        }

        [Fact]
        public void DeleteAsync_SouldReturnNotFound()
        {
            var objUserManager = new Mock<FakeUserManager>().Object;
            var objSignInManager = new Mock<FakeSignInManager>().Object;
            var messageService = new Mock<IMessageService>().Object;
            var UtilisateurController = new UtilisateurController(objUserManager, objSignInManager, messageService);

            NotFoundResult result = (NotFoundResult)UtilisateurController.DeleteAsync(String.Empty);

            result.ShouldBeOfType<NotFoundResult>()
                .ShouldNotBeNull();
            result.StatusCode.ShouldBe(404);

        }

        [Fact]
        public void DeleteAsync_SouldReturnUserNotFound()
        {
            var objUserManager = new Mock<FakeUserManager>();
            var objSignInManager = new Mock<FakeSignInManager>().Object;
            var messageService = new Mock<IMessageService>().Object;

            IdentityUser user = null;

            objUserManager.Setup(x => x.FindByIdAsync(It.IsAny<String>()))
            .ReturnsAsync(user);

            var UtilisateurController = new UtilisateurController(objUserManager.Object, objSignInManager, messageService);

            NotFoundResult result = (NotFoundResult)UtilisateurController.DeleteAsync("2");

            result.ShouldBeOfType<NotFoundResult>()
                .ShouldNotBeNull();
            result.StatusCode.ShouldBe(404);

        }

    }
}
