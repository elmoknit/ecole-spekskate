﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using SpekSkates.Admin.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Newtonsoft.Json;
using SpekSkates.Admin.Web.Modules.Cms;
using SpekSkates.Admin.Web.Modules.HomePage;


namespace SpekSkates.Tests
{
    public class APIHomeControllerTests
    {

        [Fact]
        public void HomePage_GetDefaultShouldReturnOk()
        {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();

            ApiHomePageController apiControllerHomePage = new ApiHomePageController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<HomePage>
            {
                new HomePage
                {
                    Name = "Accueil",
                    AboutImagePath = "AboutImagePath",
                    AboutText = "aboutText",
                    AboutTitle = "AboutTite",
                    Id = 1,
                    PersoContent = "persoContent",
                    PersoImagePath = "PersoImagePath",
                    HomePageProducts = new List<HomePageProduct>(),
                    HomePageSliders = new List<HomePageSlider>(),
                    isDefault = true,
                    isActive = true,
                    isDelete = false
                }

            }.AsQueryable());

            IActionResult result = apiControllerHomePage.Default();
            var okResult = result as OkObjectResult;

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;
            var homePage = data.Value as HomePageSmall;
            
            homePage.Name.ShouldBe("Accueil");
            homePage.AboutImagePath.ShouldBe("AboutImagePath");
            homePage.AboutText.ShouldBe("aboutText");
            homePage.AboutTitle.ShouldBe("AboutTite");
            homePage.PersoContent.ShouldBe("persoContent");
            homePage.PersoImagePath.ShouldBe("PersoImagePath");
        }

        [Fact]
        public void HomePage_GetDefaultShouldReturnNotFound()
        {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();

            ApiHomePageController apiControllerHomePage = new ApiHomePageController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<HomePage>
            {
                new HomePage
                {
                    Name = "Accueil",
                    AboutImagePath = "AboutImagePath",
                    AboutText = "aboutText",
                    AboutTitle = "AboutTite",
                    Id = 1,
                    PersoContent = "persoContent",
                    PersoImagePath = "PersoImagePath",
                    HomePageProducts = new List<HomePageProduct>(),
                    HomePageSliders = new List<HomePageSlider>(),
                    isDefault = false,
                    isActive = true,
                    isDelete = false
                }

            }.AsQueryable());

            IActionResult result = apiControllerHomePage.Default();

            var okResult = result as NotFoundObjectResult;

            okResult.ShouldNotBeNull();

            okResult.StatusCode.ShouldBe(404);

            var json = okResult.Value as JsonResult;
            json.Value.ShouldBe("Not found");
        }

        [Fact]
        public void HomePage_GetCustomShouldReturnOk()
        {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();

            ApiHomePageController apiControllerHomePage = new ApiHomePageController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<HomePage>
            {
                new HomePage
                {
                    Name = "Custom",
                    AboutImagePath = "AboutImagePath",
                    AboutText = "aboutText",
                    AboutTitle = "AboutTite",
                    Id = 1,
                    PersoContent = "persoContent",
                    PersoImagePath = "PersoImagePath",
                    HomePageProducts = new List<HomePageProduct>(),
                    HomePageSliders = new List<HomePageSlider>(),
                    isDefault = true,
                    isActive = true,
                    isDelete = false
                }

            }.AsQueryable());

            IActionResult result = apiControllerHomePage.Custom("Custom");

            var okResult = result as OkObjectResult;

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;
            var homePage = data.Value as HomePageSmall;

            homePage.Name.ShouldBe("Custom");
            homePage.AboutImagePath.ShouldBe("AboutImagePath");
            homePage.AboutText.ShouldBe("aboutText");
            homePage.AboutTitle.ShouldBe("AboutTite");
            homePage.PersoContent.ShouldBe("persoContent");
            homePage.PersoImagePath.ShouldBe("PersoImagePath");

        }

        [Fact]
        public void HomePage_GetCustomShouldReturnNotFound()
        {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();

            ApiHomePageController apiControllerHomePage = new ApiHomePageController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<HomePage>
            {
                new HomePage
                {
                    Name = "Accueil",
                    AboutImagePath = "AboutImagePath",
                    AboutText = "aboutText",
                    AboutTitle = "AboutTite",
                    Id = 1,
                    PersoContent = "persoContent",
                    PersoImagePath = "PersoImagePath",
                    HomePageProducts = new List<HomePageProduct>(),
                    HomePageSliders = new List<HomePageSlider>(),
                    isDefault = false,
                    isActive = true,
                    isDelete = false
                }

            }.AsQueryable());

            IActionResult result = apiControllerHomePage.Custom("Custom123");
            var okResult = result as NotFoundObjectResult;

            okResult.ShouldNotBeNull();

            okResult.StatusCode.ShouldBe(404);

        }

        [Fact]
        public void HomePage_GetAllPageCustomNameShouldReturnOk()
        {
            Mock<IRepository<HomePage>> mockRepository = new Mock<IRepository<HomePage>>();

            ApiHomePageController apiControllerHomePage = new ApiHomePageController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<HomePage>
            {
                new HomePage
                {
                    Name = "Custom1",
                    AboutImagePath = "AboutImagePath",
                    AboutText = "aboutText",
                    AboutTitle = "AboutTite",
                    Id = 1,
                    PersoContent = "persoContent",
                    PersoImagePath = "PersoImagePath",
                    HomePageProducts = new List<HomePageProduct>(),
                    HomePageSliders = new List<HomePageSlider>(),
                    isDefault = false,
                    isActive = true,
                    isDelete = false
                },
                                new HomePage
                {
                    Name = "Custom2",
                    AboutImagePath = "AboutImagePath",
                    AboutText = "aboutText",
                    AboutTitle = "AboutTite",
                    Id = 1,
                    PersoContent = "persoContent",
                    PersoImagePath = "PersoImagePath",
                    HomePageProducts = new List<HomePageProduct>(),
                    HomePageSliders = new List<HomePageSlider>(),
                    isDefault = false,
                    isActive = true,
                    isDelete = false
                }

            }.AsQueryable());

            IActionResult result = apiControllerHomePage.GetAllPageCustomName();
            var okResult = result as OkObjectResult;

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;

            var pageName = (List<string>)data.Value;
            pageName[0].ShouldBe("Custom1");
            pageName[1].ShouldBe("Custom2");

        }
    }
}
