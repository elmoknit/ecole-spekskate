﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using SpekSkates.Admin.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web;
using Moq;
using SpekSkates.Admin.Web.Base;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Newtonsoft.Json;
using SpekSkates.Admin.Web.Modules.Cms;
using SpekSkates.Admin.Web.Modules.Contact;
using temp.Controllers;

namespace SpekSkates.Tests
{
    public class APIContactControllerTests
    {

        [Fact]
        public void Contact_GetDefaultShouldReturnOk()
        {
            Mock<IRepository<Contact>> mockRepository = new Mock<IRepository<Contact>>();

            apiContactController apiControllerContact = new apiContactController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<Contact>
            {
                new Contact { Name = "Accueil", Content = "Content", Id = 2, isDefault = true, isActive = false, isDelete = false }

            }.AsQueryable());

            IActionResult result = apiControllerContact.Default();
            var okResult = result as OkObjectResult;

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;
            var contact = data.Value as Contact;

            contact.Name.ShouldBe("Accueil");
            contact.Content.ShouldBe("Content");

        }

        [Fact]
        public void Contact_GetDefaultShouldReturnNotFound()
        {
            Mock<IRepository<Contact>> mockRepository = new Mock<IRepository<Contact>>();

            apiContactController apiControllerContact = new apiContactController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<Contact>
            {

            }.AsQueryable());

            IActionResult result = apiControllerContact.Default();

            var okResult = result as NotFoundObjectResult;

            okResult.ShouldNotBeNull();

            okResult.StatusCode.ShouldBe(404);

            var json = okResult.Value as JsonResult;
            json.Value.ShouldBe("Not found");
        }

        [Fact]
        public void Contact_GetCustomShouldReturnOk()
        {
            Mock<IRepository<Contact>> mockRepository = new Mock<IRepository<Contact>>();

            apiContactController apiControllerContact = new apiContactController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<Contact>
            {
                new Contact { Name = "Custom", Content = "Content", Id = 2, isDefault = true, isActive = false, isDelete = false }

            }.AsQueryable());

            IActionResult result = apiControllerContact.Custom("Custom");

            var okResult = result as OkObjectResult;

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;
            var contact = data.Value as Contact;

            contact.Name.ShouldBe("Custom");
            contact.Content.ShouldBe("Content");


        }

        [Fact]
        public void Contact_GetCustomShouldReturnNotFound()
        {
            Mock<IRepository<Contact>> mockRepository = new Mock<IRepository<Contact>>();

            apiContactController apiControllerContact = new apiContactController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<Contact>
            {

            }.AsQueryable());

            IActionResult result = apiControllerContact.Custom("Custom123");
            var okResult = result as NotFoundObjectResult;

            okResult.ShouldNotBeNull();

            okResult.StatusCode.ShouldBe(404);

        }

        [Fact]
        public void Contact_GetAllPageCustomNameShouldReturnOk()
        {
            Mock<IRepository<Contact>> mockRepository = new Mock<IRepository<Contact>>();

            apiContactController apiControllerContact = new apiContactController(mockRepository.Object);

            mockRepository.Setup(q => q.GetAll()).Returns(new List<Contact>
            {
                new Contact { Name = "Custom1", Content = "Content", Id = 2, isDefault = false, isActive = true, isDelete = false },
                new Contact { Name = "Custom2", Content = "Content", Id = 2, isDefault = false, isActive = true, isDelete = false }

            }.AsQueryable());

            IActionResult result = apiControllerContact.GetAllPageCustomName();
            var okResult = result as OkObjectResult;

            okResult.ShouldNotBeNull();
            okResult.StatusCode.ShouldBe(200);
            var data = okResult.Value as JsonResult;

            var pageName = (List<string>)data.Value;
            pageName[0].ShouldBe("Custom1");
            pageName[1].ShouldBe("Custom2");

        }
    }
}
