﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Team.Models;

namespace SpekSkates.Admin.Web.Models
{
    public class imageTemporaire : BaseEntity
    {
        public string imageId { get; set; }

        public string imagePath { get; set; }
    }
}
