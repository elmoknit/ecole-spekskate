﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Models
{
    public class changePassword
    {
        [DataType(DataType.Password)]
        [Required(ErrorMessage ="Veuillez entrez votre ancien mot de passe")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Veuillez entrez votre nouveau mot de passe")]
        [StringLength(100, ErrorMessage = "Le {0} doit etre au moins {2} charactere long.", MinimumLength = 6)]
        [RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$")]
        [DataType(DataType.Password)]
        [Display(Name = "Nouveau mot de passe")]
        public string Password { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "Confirmer mot de passe")]
        [Compare("Password", ErrorMessage = "Les mot de passes ne sont pas identique")]
        public string ConfirmPassword { get; set; }

        public string ErrorMessage { get; set; }

        public string ConfirmationMessage { get; set; }

    }
}
