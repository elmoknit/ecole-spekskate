﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace SpekSkates.Admin.Web.Seed
{
    public class DbInitializer : IDbInitializer
    {
        private readonly SpekSkatesDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public DbInitializer(
            SpekSkatesDbContext context,
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        //This example just creates an Administrator role and one Admin users
        public async void Initialize(IApplicationBuilder app)
        {
            //create database schema if none exists
            _context.Database.EnsureCreated();

            //If there is already an Administrator role, abort
            if (_context.Roles.Any(r => r.Name == "Administrator")) return;

            //Create the Administartor Role
             using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var signInManager = serviceScope.ServiceProvider.GetService<SignInManager<IdentityUser>>();
                var userManager = serviceScope.ServiceProvider.GetService<UserManager<IdentityUser>>();

                //Create the default Admin account and apply the Administrator role
                string user = "mvezina@cegepgarneau.ca";
                string password = "Garneau123!";
                await userManager.CreateAsync(new IdentityUser { UserName = user, Email = user, EmailConfirmed = true }, password);

                user = "fcote@spektrummedia.com";
                password = "Password123!";
                await userManager.CreateAsync(new IdentityUser { UserName = user, Email = user, EmailConfirmed = true }, password);

                user = "gsaad@spektrummedia.com";
                password = "Password123!";
                await userManager.CreateAsync(new IdentityUser { UserName = user, Email = user, EmailConfirmed = true }, password);
            }


            //await _userManager.AddToRoleAsync(await _userManager.FindByNameAsync(user), "Administrator");
        }
    }
}
