﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.DependencyInjection;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.About;
using SpekSkates.Admin.Web.Modules.BoardStyle;
using SpekSkates.Admin.Web.Modules.Contact;
using SpekSkates.Admin.Web.Modules.History;
using SpekSkates.Admin.Web.Modules.HomePage;
using SpekSkates.Admin.Web.Modules.Package;
using SpekSkates.Admin.Web.Modules.Product;
using SpekSkates.Admin.Web.Modules.ProductCategory;
using SpekSkates.Admin.Web.Modules.ProductShape;
using SpekSkates.Admin.Web.Modules.ProductSize;
using SpekSkates.Admin.Web.Modules.Slider;
using SpekSkates.Admin.Web.Modules.Team.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web
{
    public static class SeedBD
    {

        public static bool AllMigrationsApplied(this DbContext context)
        {
            var applied = context.GetService<IHistoryRepository>()
                .GetAppliedMigrations()
                .Select(m => m.MigrationId);

            var total = context.GetService<IMigrationsAssembly>()
                .Migrations
                .Select(m => m.Key);

            return !total.Except(applied).Any();
        }


        public static void EnsureSeeded(this SpekSkatesDbContext context)
        {
            if (!context.Users.Any())
            {

            }
            if (!context.AboutUs.Any())
            {
                AboutUs(context);
                context.SaveChanges();
            }
            if (!context.Contact.Any())
            {
                Contact(context);
                context.SaveChanges();
            }
            if (!context.Employee.Any())
            {
                Employee(context);
                context.SaveChanges();
            }
            if (!context.HomePage.Any())
            {
                HomePage(context);
                context.SaveChanges();
            }
            if (!context.History.Any())
            {
                History(context);
                context.SaveChanges();
            }
            if (!context.HomePageProduct.Any())
            {
                HomePageProduct(context);
                context.SaveChanges();
            }
            if (!context.HomePageSlider.Any())
            {
                HomePageSlider(context);
                context.SaveChanges();

            }
            if (!context.Package.Any())
            {
                Package(context);
                context.SaveChanges();
            }
            if (!context.ProductCategory.Any())
            {
                ProductCategory(context);
                context.SaveChanges();
            }
            if (!context.ProductSize.Any())
            {
                ProductSize(context);
                context.SaveChanges();
            }
            if (!context.Slider.Any())
            {
                Slider(context);
                context.SaveChanges();
            }
            if (!context.Product.Any())
            {
                Product(context);
                context.SaveChanges();
            }
        }

        private static void Users(SpekSkatesDbContext context)
        {
            //Le seed des users existent ailleurs

        }

        private static void AboutUs(SpekSkatesDbContext context)
        {
            context.AddRange(new AboutUs
            {
                Name = "AboutUs",
                Content = "Contenu de la page About",
                IntroductionText = "Texte d'introduction de la page about",
                isActive = true,
                isDefault = true,
                isDelete = false

            });
        }


        private static void Contact(SpekSkatesDbContext context)
        {
            context.Contact.Add(new Contact
            {
                Name = "Contact",
                Content = "Contenu de la page About",
                isActive = true,
                isDefault = true,
                isDelete = false
            });
        }

        private static void Employee(SpekSkatesDbContext context)
        {
            context.Employee.AddRange(new Employee
            {
                Description = "Travaillant",
                Name = "Max Powerhouse"
            },
            new Employee
            {
                Description = "Riche",
                Name = "montgomery burns"
            }
            );
        }

        private static void HomePage(SpekSkatesDbContext context)
        {
            context.HomePage.Add(new HomePage
            {
                AboutText = "Texte informatif page d'accueil",
                AboutTitle = "Titre page d'accueil",
                Name = "Accueil",
                PersoContent = "Personnalisation",
                isActive = true,
                isDefault = true,
                isDelete = false
            });
        }

        private static void History(SpekSkatesDbContext context)
        {
            context.History.Add(new History
            {
                Name = "History",
                Text = "Text",
                isActive = true,
                isDefault = true,
                isDelete = false
            });
        }

        private static void HomePageProduct(SpekSkatesDbContext context)
        {
            //context.HomePageProduct.Add(
            //    new HomePageProduct
            //    {

            //    });
        }

        private static void HomePageSlider(SpekSkatesDbContext context)
        {
            //context.HomePageSlider.Add(
            //    new HomePageSlider
            //    {

            //    });
        }

        private static void Package(SpekSkatesDbContext context)
        {



            context.Package.AddRange(
                new Package
                {
                    Name = "Package 1",
                    Description = "desc",
                    lstLienProduitPackage = new List<Modules.Package.Models.LienProduitPackage>() {
                   new Modules.Package.Models.LienProduitPackage {
                       Product = new Product {
                           Description = "Description du produit",
                Name = "Produit 12",
                Price = 9.99M,
                ProductCategoryId = 7,
               
                BoardStyleId = 7,
                BoardStyle = new BoardStyle
                {
                    Name = "Punk nouveau genre"
                },
                ProductCategory = new ProductCategory
                {
                    Name = "Cat"

                },
                ProductSizeId = 7,
                ProductSize = new ProductSize
                {
                    Name = "Mproduct-size 1"
                },
                ProductShapeId = 7,
                isEnPromotion = true,
                History = "Histoire très intéressante",
                ProductShape = new ProductShape
                {
                    Name = "Shape 7"
                },
                isActive = true,
                isDelete = false,
                codeProduit = "code 7"
                    }, Quantity =3,
                       ProductId=1
                    
                    },
                   new Modules.Package.Models.LienProduitPackage
                   {

                       Product = new Product {
                           Description = "Description du produit",
                Name = "Produit 17",
                Price = 9.99M,
                ProductCategoryId = 66,
                BoardStyleId = 66,
                BoardStyle = new BoardStyle
                {
                    Name = "Punk 666"
                },
                ProductCategory = new ProductCategory
                {
                    Name = "Cat 666"

                },
                ProductSizeId = 66,
                ProductSize = new ProductSize
                {
                    Name = "Grandeur moyenne"
                },
                ProductShapeId = 66,
                isEnPromotion = true,
                History = "Histoire très intéressante",
                ProductShape = new ProductShape
                {
                    Name = "Shape 666"
                },
                isActive = true,
                isDelete = false,
                codeProduit = "code 66"
                    }, Quantity =3,
                       ProductId=2
                   }
                    }
                });
        }

        private static void Product(SpekSkatesDbContext context)
        {
            context.Product.AddRange(new Product
            {
                Description = "Description du produit",
                Name = "Produit 1",
                Price = 9.99M,
                ProductCategoryId = 1,
                BoardStyleId = 1,
                BoardStyle = new BoardStyle
                {
                    Name = "Punk"
                },
                ProductCategory = new ProductCategory
                {
                    Name = "Categorie 2"

                },
                ProductSizeId = 1,
                ProductSize = new ProductSize
                {
                    Name = "Mid-Size 2"
                },
                ProductShapeId = 1,
                isEnPromotion = true,
                History = "Histoire très intéressante",
                ProductShape = new ProductShape
                {
                    Name = "Shape 2"
                },
                isActive = true,
                isDelete = false,
                codeProduit = "code 1"
            },
            new Product
            {
                Description = "Description d'un autre produit",
                Name = "Produit 2",
                Price = 10.99M,
                BoardStyleId = 2,
                BoardStyle = new BoardStyle
                {
                    Name = "Funky 3"
                },
                ProductCategoryId = 2,
                ProductShapeId = 2,
                ProductSize = new ProductSize
                {
                    Name = "Mini 3"
                },
                ProductSizeId = 2,
                ProductCategory = new ProductCategory
                {
                    Name = "Categorie 233"
                },
                ProductShape = new ProductShape
                {
                    Name = "Shape 23"
                },
                isActive = true,
                isDelete = false,
                codeProduit = "code 2",
                History = "Histoire moins intéressante, mais obligatoire pour un produit"
            },
            new Product
            {
                Description = "Description d'un autre produit",
                Name = "Produit 34",
                Price = 11.99M,
                ProductCategoryId = 3,
                BoardStyleId = 3,
                BoardStyle = new BoardStyle
                {
                    Name = "Sports 4"
                },
                ProductShapeId = 3,
                ProductSizeId = 3,
                ProductSize =
                new ProductSize
                {
                    Name = "Mid-Size 6"
                },
                ProductShape = new ProductShape
                {
                    Name = "Shape 7"
                },
                ProductCategory = new ProductCategory
                {
                    Name = "Categorie 7"
                },
                isActive = true,
                isDelete = false,
                History = "Troisième histoire pour le produit",
                codeProduit ="code 3"
            }
            );
        }

        private static void ProductCategory(SpekSkatesDbContext context)
        {
            //context.ProductCategory.AddRange(new ProductCategory
            //{
            //    Name = "Categorie 4"
            //},
            //new ProductCategory
            //{
            //    Name = "Categorie 5"
            //}
            //);
        }

        private static void ProductSize(SpekSkatesDbContext context)
        {
            context.ProductSize.AddRange(new ProductSize
            {
                Name = "Micro"

            },
            new ProductSize
            {
                Name = "Mini"
            },
            new ProductSize
            {
                Name = "Mid-Size"
            }
            );
        }

        private static void Slider(SpekSkatesDbContext context)
        {
            context.Slider.Add(
                new Slider
                {
                    Name = "Slider1",
                    Text = "Texte du premier Slider"
                });
        }

    }
}
