﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Seed
{
    public interface IDbInitializer
    {
        void Initialize(IApplicationBuilder app);
    }
}
