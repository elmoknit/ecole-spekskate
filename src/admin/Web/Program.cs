﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob; // Namespace for Blob storage types
using Microsoft.Azure; //Namespace for CloudConfigurationManager

namespace SpekSkates.Admin.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IWebHost webHost = BuildWebHost(args);
            MigrateDatabase(webHost);
            webHost.Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {

            return WebHost.CreateDefaultBuilder(args)
               .UseStartup<Startup>()
               .UseDefaultServiceProvider(options =>
                options.ValidateScopes = false)
               .UseUrls("http://localhost:9000")
               .Build();

        }
        private static void MigrateDatabase(IWebHost webHost)
        {
            using (var scope = webHost.Services.CreateScope())
            {
                scope.ServiceProvider.GetService<SpekSkatesDbContext>().Database.Migrate();
            }
        }
    }
}
