﻿$(document).ready(function () {
    $('.select2').select2();
    $(".select2Limit5").select2({
        maximumSelectionLength: 5
    });



    $("#SelectSizeId").children('option').hide();
    $("#SelectSizeId").children("option[data-category^=" + $("#SelectCategoryId").val() + "]").show()
    $("#SelectSizeId").children("option[data-default^=novalue]").show()

    $("#SelectShapeId").children('option').hide();
    $("#SelectShapeId").children("option[data-category^=" + $("#SelectCategoryId").val() + "]").show()
    $("#SelectShapeId").children("option[data-default^=novalue]").show()

    if ($("#SelectCategoryId").find(':selected').attr('data-board') === "Board") {
        $('#IsPlanche').removeClass('hide-board-option');
    } else {
        $('#IsPlanche').addClass('hide-board-option');
    }


    //$('#SelectSizeId').find('option:selected').removeAttr('selected');

    //$('#SelectSizeId').find('option').attr('value');

    //$('#SelectSizeId').find("option").each(function () {
    //    if ($(this).hasClass('hidden')) {
    //        $(this).removeAttr('selected');
    //    }
    //});

});


$(function () {



    $("#SelectCategoryId").on("change", function () {
        $("#SelectSizeId").children('option').hide();
        $("#SelectSizeId").children("option[data-category^=" + $("#SelectCategoryId").val() + "]").show()
        $("#SelectSizeId").children("option[data-default^=novalue]").show()
        $("#SelectSizeId").children("option[data-default^=novalue]").prop("selected", "selected")


        $("#SelectShapeId").children('option').hide();
        $("#SelectShapeId").children("option[data-category^=" + $("#SelectCategoryId").val() + "]").show()
        $("#SelectShapeId").children("option[data-default^=novalue]").show()
        $("#SelectShapeId").children("option[data-default^=novalue]").prop("selected", "selected")

        if ($("#SelectCategoryId").find(':selected').attr('data-board') === "Board") {
            $('#IsPlanche').removeClass('hide-board-option');
        } else {
            $('#IsPlanche').addClass('hide-board-option');
        }

    });





});


$(document).ready(function () {
    $('input[type=file]').change(function () {
        var val = $(this).val().toLowerCase();
        var regex = new RegExp("(.*?)\.(jpg|png)$");
        if ($(this).parent()[0].getElementsByClassName("errorimgformat").length > 0) {
            $(this).parent()[0].getElementsByClassName("errorimgformat")[0].remove();
        }

       
        if (!(regex.test(val))) {
            $(this).val('');

            var para = document.createElement("p");
            var node = document.createTextNode("Vous devez selectionner un fichier de type .jpg ou .png");
            para.className = "errorimgformat alert alert-danger";
            para.appendChild(node);

            $(this).parent()[0].appendChild(para);

        }
    });
});
document.addEventListener('DOMContentLoaded', function () {

}, false);