﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.History;


namespace SpekSkates.Admin.Web.Modules.Cms
{
    [Route("api/[controller]")]
    public class ApiHistoryController : Controller
    {
        IRepository<History.History> _historyRepository;
        public ApiHistoryController(IRepository<History.History> historyRepository)
        {
            _historyRepository = historyRepository;
        }
        // GET api/Home
        [HttpGet]
        public void Get()
        {

        }

        // GET api/values/5
        [HttpGet("{id}")]
        public void Get(int id)
        {

        }

        [Route("[action]")]
        public IActionResult Default()
        {
            var result = HistoryHelper.GetHistoryWithInclude(_historyRepository.GetAll().Where(s => s.isDefault && !s.isDelete));
            var returnResult = Json("");
            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }


        [Route("[action]/{pageName}")]
        public IActionResult Custom(string pageName)
        {
            var returnResult = Json("");
            if (pageName == null || pageName == string.Empty)
            {
                returnResult = Json("Bad Request");
                returnResult.StatusCode = 400;
                return BadRequest(returnResult);
            }

            var result = HistoryHelper.GetHistoryWithInclude(_historyRepository.GetAll().Where(s => s.Name == pageName && !s.isDelete));
            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            //result.EventHistories.ToList().Sort((x, y) => x.dateTime.CompareTo(y.dateTime)); ;
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }


        [Route("[action]")]
        public IActionResult GetAllPageCustomName()
        {
            var result = _historyRepository.GetAll().Where(s => !s.isDefault && s.isActive).Select(s => s.Name).ToList();
            var returnResult = Json("");
            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }


        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
