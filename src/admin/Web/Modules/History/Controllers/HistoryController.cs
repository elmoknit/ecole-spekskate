﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SpekSkates.Admin.Web;
using SpekSkates.Admin.Web.Models;
using SpekSkates.Admin.Web.Base;
using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob; // Namespace for Blob storage types
using Microsoft.Azure;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.History.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace SpekSkates.Admin.Web.Modules.History
{
    [Authorize]
    public class HistoryController : Controller
    {
        private IRepository<History> _itemRepository;
        private IRepository<EventHistory> _eventHistoryRepository;
        private readonly IAzureBlobStorage _blobStorage;



        public HistoryController(IRepository<History> historyRepository, IRepository<EventHistory> eventHistoryRepository, IAzureBlobStorage blobStorage)
        {
            _itemRepository = historyRepository;
            _eventHistoryRepository = eventHistoryRepository;
            _blobStorage = blobStorage;
        }


        // GET: Historys
        public IActionResult Index()
        {
            return View("~/views/history/index.cshtml", _itemRepository.GetAll().Where(s => !s.isDelete).ToList());
        }



        // GET: Historys/Edit/5
        public IActionResult Edit(int? id)
        {
            var mv = new ViewModelDetail();

            if (id == null)
            {
                return NotFound();
            }

            mv.History = HistoryHelper.GetHistoryWithInclude(_itemRepository.Get(id));

            if (mv.History == null)
            {
                return NotFound();
            }




            return View(mv);
        }


        // POST: Historys/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, ViewModelDetail mv)
        {

            if (id != mv.History.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                save(mv);

                try
                {
                    _itemRepository.Update(mv.History);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HistoryExists(mv.History.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            return View(mv);
        }


        public IActionResult Delete(string blobName, int id, string identifiant)
        {

            if (!string.IsNullOrEmpty(blobName))
            {
                var imageName = blobName.Split('/');
                _blobStorage.DeleteAsync(imageName[imageName.Count() - 1]);
                var history = _itemRepository.Get(id).FirstOrDefault();

                if (history == null)
                {
                    return NotFound();
                }
                if (identifiant == "picture1")
                {
                    history.Picture1Path = "";
                }
                else if (identifiant == "picture2")
                {
                    history.Picture2Path = "";
                }
                else if (identifiant == "picture3")
                {
                    history.Picture3Path = "";
                }
                _itemRepository.Save(history);
                return RedirectToAction("Edit", new { id = history.Id });
            }
            else
            {
                return NotFound();
            }



        }


        private bool HistoryExists(int id)
        {
            return _itemRepository.Get(id) != null;
        }


        private bool UploadFile(IFormFile file, string blobName)
        {
            if (file == null || file.Length == 0)
            { return false; }

            //source : https://github.com/TahirNaushad/Fiver.Azure.Blob/tree/master/Fiver.Azure.Blob.Client
            var fileStream = file.GetFileStream();

            _blobStorage.UploadAsync(blobName, fileStream);
            return true;
        }





        public IActionResult Create()
        {
            var mv = new ViewModelDetail();

            mv.History = new History
            {
                Name = "",
                Text = "",
                isDefault = false,
                isActive = true,
                isDelete = false
            };




            return View(mv);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ViewModelDetail mv)
        {
            if (ModelState.IsValid)
            {
                save(mv);
                try
                {
                    _itemRepository.Update(mv.History);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HistoryExists(mv.History.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            return View(mv);
        }


        public IActionResult Default(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var history = _itemRepository.Get(id).FirstOrDefault();
            var homepageRemoveDefault = _itemRepository.GetAll().Where(s => s.isDefault).FirstOrDefault();
            if (history == null)
            {
                return NotFound();
            }

            if (homepageRemoveDefault != null)
            {
                homepageRemoveDefault.isDefault = false;
                _itemRepository.Update(homepageRemoveDefault);
            }

            history.isDefault = true;
            history.isActive = false;
            _itemRepository.Update(history);


            return RedirectToAction(nameof(Index));
        }

        public IActionResult Active(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var history = _itemRepository.Get(id).FirstOrDefault();
            if (history == null)
            {
                return NotFound();
            }

            history.isActive = true;

            _itemRepository.Update(history);


            return RedirectToAction(nameof(Index));
        }


        public IActionResult UnActive(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var history = _itemRepository.Get(id).FirstOrDefault();
            if (history == null)
            {
                return NotFound();
            }

            history.isActive = false;

            _itemRepository.Update(history);


            return RedirectToAction(nameof(Index));
        }


        public IActionResult DeletePage(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var page = _itemRepository.Get(id).FirstOrDefault();

            if (page == null)
            {
                return NotFound();
            }

            return View(page);
        }

        public IActionResult DeleteItem(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var history = _itemRepository.Get(id).FirstOrDefault();

            if (history == null)
            {
                return NotFound();
            }


            history.isDelete = true;
            history.isActive = false;
            _itemRepository.Update(history);


            return RedirectToAction(nameof(Index));
        }


        public IActionResult CreateEvent(int historyId)
        {
            var mv = new ViewModelEvent();


            mv.EventHistory = new EventHistory
            {
                Date = "",
                Text = "",
                HistoryId = historyId,
                isDelete = false
            };




            return View(mv);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateEvent(ViewModelEvent mv)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _eventHistoryRepository.Update(mv.EventHistory);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HistoryExists(mv.EventHistory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction("Edit", new { id = mv.EventHistory.HistoryId });
            }
            return View(mv);
        }

        private void save(ViewModelDetail mv)
        {
            var fileNamePicture1 = "";
            if (mv.Picture1 != null && mv.Picture1.Length != 0)
            { fileNamePicture1 = mv.Picture1.GetFilename(); }

            if (UploadFile(mv.Picture1, fileNamePicture1))
            {
                mv.History.Picture1Path = IFromFileExtensions.ImagePath(fileNamePicture1);
            }


            var fileNamePicture2 = "";
            if (mv.Picture2 != null && mv.Picture2.Length != 0)
            { fileNamePicture2 = mv.Picture2.GetFilename(); }

            if (UploadFile(mv.Picture2, fileNamePicture2))
            {
                mv.History.Picture2Path = IFromFileExtensions.ImagePath(fileNamePicture2);
            }


            var fileNamePicture3 = "";
            if (mv.Picture3 != null && mv.Picture3.Length != 0)
            { fileNamePicture3 = mv.Picture3.GetFilename(); }

            if (UploadFile(mv.Picture3, fileNamePicture3))
            {
                mv.History.Picture3Path = IFromFileExtensions.ImagePath(fileNamePicture3);
            }
        }

        // GET: Historys/Edit/5
        public IActionResult EditEvent(int? id)
        {
            var mv = new ViewModelEvent();

            if (id == null)
            {
                return NotFound();
            }

            mv.EventHistory = _eventHistoryRepository.Get(id).FirstOrDefault();

            if (mv.EventHistory == null)
            {
                return NotFound();
            }




            return View(mv);
        }


        // POST: Historys/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditEvent(int id, ViewModelEvent mv)
        {

            if (id != mv.EventHistory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {

                try
                {
                    _eventHistoryRepository.Update(mv.EventHistory);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HistoryExists(mv.EventHistory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Edit", new { id = mv.EventHistory.HistoryId });
            }
            return View(mv);

        }

        public IActionResult DeleteEvent(int? id, int historyId)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eventhistory = _eventHistoryRepository.Get(id).FirstOrDefault();

            if (eventhistory == null)
            {
                return NotFound();
            }


            eventhistory.isDelete = true;

            _eventHistoryRepository.Update(eventhistory);


            return RedirectToAction("Edit", new { id = historyId });
        }
    }
}
