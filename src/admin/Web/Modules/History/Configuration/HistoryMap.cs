﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SpekSkates.Admin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.History
{
    public class HistoryMap
    {

        public HistoryMap(EntityTypeBuilder<History> entityBuilder)
        {
            entityBuilder.HasKey(t => t.Id);
        }
    }
}
