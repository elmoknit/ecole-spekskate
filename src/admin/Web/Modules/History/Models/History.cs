﻿using SpekSkates.Admin.Web.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage.Blob;
using SpekSkates.Admin.Web.Modules.Slider;

namespace SpekSkates.Admin.Web.Modules.History
{

    public class History : BaseEntity
    {

        [Required(ErrorMessage = "Le nom de la page est obligatoire")]
        [DisplayName("Nom de la page :")]
        public string Name { get; set; }


        [Required(ErrorMessage = "Le texte d'introduction est obligatoire")]
        [DisplayName("Texte d'introduction:")]
        public string Text { get; set; }


        [DisplayName("Photo 1:")]
        public string Picture1Path { get; set; }

        [DisplayName("Photo 2:")]
        public string Picture2Path { get; set; }

        [DisplayName("Photo 3:")]
        public string Picture3Path { get; set; }

        [DisplayName("Images de la bannière :")]
        public virtual ICollection<EventHistory> EventHistories { get; set; }


        public bool isDefault { get; set; }

        public bool isActive { get; set; }

        public bool isDelete { get; set; }

    }


    public class EventHistory : BaseEntity
    {
        public int HistoryId { get; set; }

        [Required(ErrorMessage = "Le titre est obligatoire")]
        [DisplayName("Titre:")]
        public string Date { get; set; }

        [Required(ErrorMessage = "Le texte est obligatoire")]
        [DisplayName("Texte d'introduction:")]
        public string Text { get; set; }

        public bool isDelete { get; set; }

        [Required(ErrorMessage ="La date est obligatoire")]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [DisplayName("Date:")]
        public DateTime dateTime { get; set; }

    }
}
