﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using SpekSkates.Admin.Web.AzureBlob;

namespace SpekSkates.Admin.Web.Modules.History.ViewModels
{
    public class ViewModel
    {
        public List<History> History {get; set;}
    }
}
