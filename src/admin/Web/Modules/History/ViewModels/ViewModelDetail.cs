﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using SpekSkates.Admin.Web.AzureBlob;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SpekSkates.Admin.Web.Modules.History.ViewModels
{
    public class ViewModelDetail
    {


        public History History { get; set; }
        [DisplayName("Téléverser une première image (Taille suggérée : 1080px x 320px)")]
        public IFormFile Picture1 { get; set; }
        [DisplayName("Téléverser une deuxième image (Taille suggérée : 1080px x 320px)")]
        public IFormFile Picture2 { get; set; }
        [DisplayName("Téléverser une troisième image (Taille suggérée : 1080px x 320px)")]
        public IFormFile Picture3 { get; set; }


    }
}
