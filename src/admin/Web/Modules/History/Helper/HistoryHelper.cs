﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.History
{
    public static class HistoryHelper
    {


        public static History GetHistoryWithInclude(IQueryable<History> history)
        {
            return history
                .Include(i => i.EventHistories)
                .FirstOrDefault();
        }
    }
}
