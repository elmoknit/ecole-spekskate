﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SpekSkates.Admin.Web.Modules.Board.Configuration
{
    public class BoardMap
    {
        public BoardMap(EntityTypeBuilder<Models.Board> entityBuilder)
        {
            entityBuilder.HasKey(t => t.Id);
            entityBuilder.Property(e => e.Name).IsRequired().HasMaxLength(100);
            //entityBuilder.Property(e => e.Style).IsRequired();
        }
    }
}
