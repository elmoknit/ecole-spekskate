﻿using SpekSkates.Admin.Web.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.Board.Models
{
    public class Board: BaseEntity
    {
        [Required(ErrorMessage = "Le nom do produit est obligatoire")]
        [DisplayName("Nom du produit : ")]
        public string Name { get; set; }


        [Required(ErrorMessage = "Le prix est obligatoire")]
        [DisplayName("Prix : ")]
        public decimal Price { get; set; }

        [DisplayName("Image du produit : ")]
        public string ImagePath { get; set; }

        [Required(ErrorMessage = "L'histoire du produit est obligatoire")]
        [DisplayName("Histoire du produit : ")]
        public string History { get; set; }

        [Required(ErrorMessage = "La description du produit est obligatoire")]
        [DisplayName("Description du produit : ")]
        public string Description { get; set; }



        [Required(ErrorMessage = "La taille est obligatoire")]
        [DisplayName("Taille du produit : ")]
        public int ProductSizeId { get; set; }

        public ProductSize.ProductSize ProductSize { get; set; }


        [Required(ErrorMessage = "La catégorie de produit est obligatoire")]
        [DisplayName("Catégorie du produit : ")]
        public int ProductCategoryId { get; set; }

        public ProductCategory.ProductCategory ProductCategory { get; set; }


        [DisplayName("Lien vers le produit : ")]
        public string Link { get; set; }

        public virtual ICollection<HomePage.HomePageProduct> HomePageProducts { get; set; }

        public bool isActive { get; set; }

        public bool isDelete { get; set; }


        [Required(ErrorMessage = "Le type de planche est obligatoire")]
        [DisplayName("Type de planche : ")]
        int BoardTypeId { get; set; }


        [DisplayName("Kicktail du planche : ")]
        bool IsKickTail { get; set; }
    }
}
