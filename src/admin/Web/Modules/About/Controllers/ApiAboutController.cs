﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.About;
using Newtonsoft.Json;
using SpekSkates.Admin.Web.Modules.About.Helper;

namespace temp.Controllers
{
    [Route("api/[controller]")]
    public class apiAboutController : Controller
    {
        IRepository<AboutUs> _aboutRepository;
        public apiAboutController(IRepository<AboutUs> aboutRepository)
        {
            _aboutRepository = aboutRepository;
            //Employee employ = new Employee() {name="bob", description="wqinf", image=true };
            //About modelTest = new About() { Name = "Modele 1..??", team = new List<Employee> { employ }, Content="contenu", introduction="introdu", Language=1};
           // modelTest.Content = "SKAFNSDDBODGBDS";
            //_aboutRepository.Update(modelTest);
           // _aboutRepository.Save(modelTest);
        }

        // GET api/About
        //[HttpGet]
        //public IActionResult Get()
        //{
        //    //faire des requetes en linq pour aller chercher les infos 
        //    // a parler avec l'equipe pour savoir comment gerer la bd
        //    // mon clavier a pas d'accent alors desole pour les fautes

        //    var result = AboutHelper.GetAboutUs(_aboutRepository.Get(1));
        //    var returnResult = Json("");
        //    if (result == null)
        //    {
        //        returnResult = Json("Not found");
        //        returnResult.StatusCode = 404;
        //        return NotFound(returnResult);
        //    }
        //    returnResult = Json(result);
        //    returnResult.StatusCode = 200;
        //    return Ok(returnResult);
        //}

        [Route("[action]")]
        public IActionResult Default()
        {
            var result = _aboutRepository.GetAll().Where(s => s.isDefault && !s.isDelete).FirstOrDefault();
            var returnResult = Json("");
            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }


        [Route("[action]/{pageName}")]
        public IActionResult Custom(string pageName)
        {
            var returnResult = Json("");
            if (pageName==null || pageName == string.Empty)
            {
                returnResult = Json("Bad Request");
                returnResult.StatusCode = 400;
                return BadRequest(returnResult);
            }
            var result = _aboutRepository.GetAll().Where(s => s.Name == pageName && !s.isDelete).FirstOrDefault();

            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }


        [Route("[action]")]
        public IActionResult GetAllPageCustomName()
        {
            var result = _aboutRepository.GetAll().Where(s => !s.isDefault && s.isActive).Select(s => s.Name).ToList();
            var returnResult = Json("");
            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }

        //// GET api/values/5
        //[HttpGet("{id}")]
        //public IActionResult Get(int id)
        //{
        //    var result = AboutHelper.GetAboutUs(_aboutRepository.Get(id));
        //    var returnResult = Json("");
        //    if (result == null)
        //    {
        //        returnResult = Json("Not found");
        //        returnResult.StatusCode = 404;
        //        return NotFound(returnResult);
        //    }
        //    returnResult = Json(result);
        //    returnResult.StatusCode = 200;
        //    return Ok(returnResult);
        //}

        [HttpPost("{id}")]
        public void Edit(int id)
        {
            //Un id ne peut pas etre nulle donc pas besoin de valider
            _aboutRepository.Save(_aboutRepository.Get(id).FirstOrDefault());
        }
    }
}
