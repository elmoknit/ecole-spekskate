﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Base;
using Microsoft.EntityFrameworkCore;
using SpekSkates.Admin.Web.Modules.Team.Models;
using System.Collections.Generic;
using SpekSkates.Admin.Web.Modules.Team.Controllers;
using SpekSkates.Admin.Web.Modules.About.ViewModels;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.Team.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace SpekSkates.Admin.Web.Modules.About.Controllers
{
    [Authorize]
    public class AboutController : Controller
    {

        private IRepository<AboutUs> _aboutRepository;
        private IRepository<Employee> _employeeRepository;
        private IAzureBlobStorage _blobStorage;

        public AboutController(IRepository<AboutUs> aboutRepository, IRepository<Employee> employeeRepository, IAzureBlobStorage blobStorage)
        {
            _aboutRepository = aboutRepository;
            _employeeRepository = employeeRepository;
            _blobStorage = blobStorage;

        }

        // GET: About
        public ActionResult Index()
        {
            return View("~/views/about/index.cshtml", _aboutRepository.GetAll().Where(s => !s.isDelete).ToList());
        }

        // GET: About/Create
        public ActionResult Create()
        {
            var mv = new ViewModelAboutUs();

            mv.AboutUs = new AboutUs
            {
                Name = "",
                isDefault = false,
                isActive = true,
                isDelete = false
            };

            return View(mv);
        }

        // POST: About/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ViewModelAboutUs mv)
        {
            if (ModelState.IsValid)
            {
                var fileNameAbout = "";

                if (mv.ImagePath1 != null && mv.ImagePath1.Length != 0)
                { fileNameAbout = mv.ImagePath1.GetFilename(); }

                if (UploadFile(mv.ImagePath1, fileNameAbout))
                {
                    mv.AboutUs.ImagePath1 = IFromFileExtensions.ImagePath(fileNameAbout);
                }

                fileNameAbout = "";
                if (mv.ImagePath2 != null && mv.ImagePath2.Length != 0)
                { fileNameAbout = mv.ImagePath2.GetFilename(); }

                if (UploadFile(mv.ImagePath2, fileNameAbout))
                {
                    mv.AboutUs.ImagePath2 = IFromFileExtensions.ImagePath(fileNameAbout);
                }

                fileNameAbout = "";
                if (mv.ImagePath3 != null && mv.ImagePath3.Length != 0)
                { fileNameAbout = mv.ImagePath3.GetFilename(); }

                if (UploadFile(mv.ImagePath3, fileNameAbout))
                {
                    mv.AboutUs.ImagePath3 = IFromFileExtensions.ImagePath(fileNameAbout);
                }

                try
                {
                    mv.AboutUs.isActive = true;
                    mv.AboutUs.isDefault = false;
                    mv.AboutUs.isDelete = false;
                    _aboutRepository.Update(mv.AboutUs);
                    _aboutRepository.Save(mv.AboutUs);

                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    ViewModelAboutUs mv2 = new ViewModelAboutUs() { MessageUtilisateur = "Une erreur s'est produite, la page n'a pas pu être créée."};
                    return View(mv2);
                }
            }
            return View(mv);
        }

        // GET: HomePages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mv = new ViewModelAboutUs();

            AboutUs aboutUs = _aboutRepository.Get(id).FirstOrDefault();
            mv.AboutUs = aboutUs;

            if (mv.AboutUs == null)
            {
                return NotFound();
            }

            mv.AboutUs.Team = _employeeRepository.GetAll().ToList();
            return View(mv);
        }

        // POST: About/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ViewModelAboutUs mv)
        {
            if (id != mv.AboutUs.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var fileNameAbout = "";

                if (mv.ImagePath1 != null && mv.ImagePath1.Length != 0)
                { fileNameAbout = mv.ImagePath1.GetFilename(); }

                if (UploadFile(mv.ImagePath1, fileNameAbout))
                {
                    mv.AboutUs.ImagePath1 = IFromFileExtensions.ImagePath(fileNameAbout);
                }

                fileNameAbout = "";
                if (mv.ImagePath2 != null && mv.ImagePath2.Length != 0)
                { fileNameAbout = mv.ImagePath2.GetFilename(); }

                if (UploadFile(mv.ImagePath2, fileNameAbout))
                {
                    mv.AboutUs.ImagePath2 = IFromFileExtensions.ImagePath(fileNameAbout);
                }

                fileNameAbout = "";
                if (mv.ImagePath3 != null && mv.ImagePath3.Length != 0)
                { fileNameAbout = mv.ImagePath3.GetFilename(); }

                if (UploadFile(mv.ImagePath3, fileNameAbout))
                {
                    mv.AboutUs.ImagePath3 = IFromFileExtensions.ImagePath(fileNameAbout);
                }

                try
                {
                    _aboutRepository.Update(mv.AboutUs);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AboutExists(mv.AboutUs.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return RedirectToAction(nameof(Index));
        }

        public ActionResult EditEmployee(int id, int pageId)
        {
            Employee employee = (Employee)_employeeRepository.Get(id).FirstOrDefault();
            if (employee == null)
            {
                return NotFound();
            }

            ViewModelEmployee mve = new ViewModelEmployee();
            mve.aboutUsId = pageId;
            mve.Employee = employee;
            var test = View(mve);
            return test;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployee(Employee employee, ViewModelEmployee mv)
        {
            if (ModelState.IsValid)
            {
                var fileNameAbout = "";
                if (mv.EmployeePicture != null && mv.EmployeePicture.Length != 0)
                { fileNameAbout = mv.EmployeePicture.GetFilename(); }

                if (UploadFile(mv.EmployeePicture, fileNameAbout))
                {
                    mv.Employee.ImagePath = IFromFileExtensions.ImagePath(fileNameAbout);
                }
            }
            try
            {
                _employeeRepository.Update(mv.Employee);
                _employeeRepository.Save(mv.Employee);
            }
            catch
            {
                throw;
            }

            if (_aboutRepository.Get(mv.aboutUsId).FirstOrDefault() != null)
            {

                return RedirectToAction(nameof(Edit), new { id = mv.aboutUsId });
            }

            //TODO message de redirection à l'utilisateur?...
            return RedirectToAction(nameof(Index));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmployee(int id, Employee employee, ViewModelAboutUs mv)
        {

            if (ModelState.IsValid)
            {
                var imageFileName = mv.EmployeePicture.GetFilename();
                var uploadResultBool = UploadFile(mv.EmployeePicture, imageFileName);
                if (mv.EmployeePicture != null && uploadResultBool)
                {
                    employee.ImagePath = IFromFileExtensions.ImagePath(imageFileName);
                }
                _employeeRepository.Update(employee);
                _employeeRepository.Save(employee);
            }
            else
            {
                // for debugging reasons
                var errors = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);
            }

                return RedirectToAction(nameof(Edit), new { id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddEmployeePicture(string blobname, int id, ViewModelAboutUs mv)
        {
            Employee employee = _employeeRepository.Get(id).FirstOrDefault();
            if (employee == null)
            {
                return NotFound();
            }

            var fileNameAbout = "";
            if (mv.EmployeePicture != null && mv.EmployeePicture.Length != 0)
            { fileNameAbout = mv.EmployeePicture.GetFilename(); }

            if (UploadFile(mv.EmployeePicture, fileNameAbout))
            {
                mv.Employee.ImagePath = IFromFileExtensions.ImagePath(fileNameAbout);
            }

            if (UploadFile(mv.EmployeePicture, mv.EmployeePicture.GetFilename()))
            {
                mv.Employee.ImagePath = IFromFileExtensions.ImagePath(mv.EmployeePicture.GetFilename());
            }

            try
            {
                _employeeRepository.Update(mv.Employee);
            }
            catch (DbUpdateConcurrencyException)
            {
                    throw;
            }

            return RedirectToAction(nameof(Edit), new { id });
        }

        private bool UploadFile(IFormFile file, string blobName)
        {
            if (file == null || file.Length == 0)
            { return false; }

            //source : https://github.com/TahirNaushad/Fiver.Azure.Blob/tree/master/Fiver.Azure.Blob.Client
            var fileStream = file.GetFileStream();

            _blobStorage.UploadAsync(blobName, fileStream);
            return true;
        }

        public IActionResult DeleteEmployeePicture(string blobName, int id, int pageId)
        {
            if (!string.IsNullOrEmpty(blobName))
            {
                var imageName = blobName.Split('/');
                _blobStorage.DeleteAsync(imageName[imageName.Count() - 1]);
                var employee = _employeeRepository.Get(id).FirstOrDefault();
                employee.ImagePath = "";
                _employeeRepository.Save(employee);
            }

            return RedirectToAction(nameof(EditEmployee), new { id });
        }

        public IActionResult Deleteblob(string blobName, int id, int pageId)
        {
            if (!string.IsNullOrEmpty(blobName))    
            {
                var imageName = blobName.Split('/');
                _blobStorage.DeleteAsync(imageName[imageName.Count() - 1]);
                var employee = _employeeRepository.Get(id).FirstOrDefault();
                employee.ImagePath = "";
                _employeeRepository.Save(employee);
            }

            return RedirectToAction(nameof(Edit), new { id = pageId});
        }

        public IActionResult DeleteblobAbout1(string blobName, int id)
        {
            if (!string.IsNullOrEmpty(blobName))
            {
                var imageName = blobName.Split('/');
                _blobStorage.DeleteAsync(imageName[imageName.Count() - 1]);
                var aboutUs = _aboutRepository.Get(id).FirstOrDefault();
                aboutUs.ImagePath1= "";
                _aboutRepository.Save(aboutUs);
            }

            return RedirectToAction(nameof(Edit), new { id });
        }

        public IActionResult DeleteblobAbout2(string blobName, int id)
        {
            if (!string.IsNullOrEmpty(blobName))
            {
                var imageName = blobName.Split('/');
                _blobStorage.DeleteAsync(imageName[imageName.Count() - 1]);
                var aboutUs = _aboutRepository.Get(id).FirstOrDefault();
                aboutUs.ImagePath2 = "";
                _aboutRepository.Save(aboutUs);
            }

            return RedirectToAction(nameof(Edit), new { id});
        }

        public IActionResult DeleteblobAbout3(string blobName, int id)
        {
            if (!string.IsNullOrEmpty(blobName))
            {
                var imageName = blobName.Split('/');
                _blobStorage.DeleteAsync(imageName[imageName.Count() - 1]);
                var aboutUs = _aboutRepository.Get(id).FirstOrDefault();
                aboutUs.ImagePath3 = "";
                _aboutRepository.Save(aboutUs);
            }

            return RedirectToAction(nameof(Edit), new { id });
        }

        
        public ActionResult DeleteEmployee(int id, int pageId)
        {
            Employee employee = (Employee)_employeeRepository.Get(id).FirstOrDefault();
            if (employee == null)
            {
                return NotFound();
            }
            ViewModelEmployee mv = new ViewModelEmployee() { Employee = employee };
            mv.aboutUsId = pageId;
            return View(mv);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteEmployee(int id, ViewModelEmployee mv)
        {
            Employee employee = _employeeRepository.Get(id).FirstOrDefault();
            if (employee != null)
            {
                try
                {
                    _employeeRepository.Delete(id);
                }
                catch
                {
                    throw;
                }
            }
            else
            {
                return NotFound();
            }
            return RedirectToAction(nameof(Edit), new { id = mv.aboutUsId });
        }


        // GET: About/Delete/5
        public ActionResult DeletePage(int id)
        {
            AboutUs aboutUs = _aboutRepository.Get(id).FirstOrDefault();
            return View(aboutUs);
        }

        // POST: About/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePage(int id, IFormCollection collection)
        {
            try
            {
                _aboutRepository.Delete(id);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View("Index");
            }
        }

        public ActionResult RedirectToEdit(int aboutUsId)
        {
            var page = _aboutRepository.Get(aboutUsId).FirstOrDefault();
            if (page != null)
            {
                return RedirectToAction(nameof(Edit), new { id = aboutUsId });
            }
            return RedirectToAction(nameof(Index));
        }

        private bool AboutExists(int id)
        {
            return _aboutRepository.Get(id) != null;
        }

        // GET: HomePages/Edit/5
        public IActionResult Default(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aboutus = _aboutRepository.Get(id).FirstOrDefault();
            var aboutusRemoveDefault = _aboutRepository.GetAll().Where(s => s.isDefault).FirstOrDefault();
            if (aboutus == null)
            {
                return NotFound();
            }

            if (aboutusRemoveDefault != null)
            {
                aboutusRemoveDefault.isDefault = false;
                _aboutRepository.Update(aboutusRemoveDefault);
            }

            aboutus.isDefault = true;
            aboutus.isActive = false;
            _aboutRepository.Update(aboutus);


            return RedirectToAction(nameof(Index));
        }

        public IActionResult Active(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aboutus = _aboutRepository.Get(id).FirstOrDefault();
            if (aboutus == null)
            {
                return NotFound();
            }

            aboutus.isActive = true;

            _aboutRepository.Update(aboutus);


            return RedirectToAction(nameof(Index));
        }


        public IActionResult UnActive(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aboutus = _aboutRepository.Get(id).FirstOrDefault();
            if (aboutus == null)
            {
                return NotFound();
            }

            aboutus.isActive = false;

            _aboutRepository.Update(aboutus);


            return RedirectToAction(nameof(Index));
        }


        // GET: HomePages/Edit/5
        public IActionResult DeleteItem(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aboutus = _aboutRepository.Get(id).FirstOrDefault();

            if (aboutus == null)
            {
                return NotFound();
            }


            aboutus.isDelete = true;
            aboutus.isActive = false;
            _aboutRepository.Update(aboutus);


            return RedirectToAction(nameof(Index));
        }
    }
}