﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.About.Helper
{
    public class AboutHelper
    {

        public static AboutUs GetAboutUs(IQueryable<AboutUs> aboutUs)
        {
            return aboutUs
                .FirstOrDefault();
        }
    }
}
