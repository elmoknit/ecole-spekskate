﻿using Microsoft.AspNetCore.Http;
using SpekSkates.Admin.Web.Modules.Team.Models;
using System.Collections.Generic;
using System.ComponentModel;

namespace SpekSkates.Admin.Web.Modules.About.ViewModels
{
    public class ViewModelAboutUs
    {
        public AboutUs AboutUs { get; set; }

        public Employee Employee { get; set; }

        public List<IFormFile> EmployeePictures { get; set; }

        [DisplayName("Téléverser la photo de l'employé (Taille suggérée : 350px x 350px)")]
        public IFormFile EmployeePicture { get; set; }

        [DisplayName("Téléverser une première image (Taille suggérée : 1080px x 320px)")]
        public IFormFile ImagePath1 { get; set; }
        [DisplayName("Téléverser une deuxième image (Taille suggérée : 1080px x 320px)")]
        public IFormFile ImagePath2 { get; set; }
        [DisplayName("Téléverser une troisième image (Taille suggérée : 1080px x 320px)")]
        public IFormFile ImagePath3 { get; set; }

        public string MessageUtilisateur { get; set; }

    }
}
    