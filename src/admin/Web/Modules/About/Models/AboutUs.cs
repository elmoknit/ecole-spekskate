﻿   using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Team.Models;
using SpekSkates.Admin.Web.Models;
using System.ComponentModel;
   using System.ComponentModel.DataAnnotations;

namespace SpekSkates.Admin.Web.Modules.About
{
    public class AboutUs : BaseEntity
    {
        [Required(ErrorMessage = "Le nom de la page est obligatore")]
        [DisplayName("Nom de la page: ")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Le texte d'introduction est obligatoire")]
        [DisplayName("Texte d'introduction: ")]
        public string IntroductionText{ get; set; }

        [Required(ErrorMessage = "Le contenu de la page est obligatoire")]
        [DisplayName("Contenu: ")]
        public string Content { get; set; }

        [DisplayName("Équipe: ")]
        public IList<Employee> Team { get; set; }

        [DisplayName("Image #1: ")]
        public string ImagePath1 { get; set; }

        [DisplayName("Image #2: ")]
        public string ImagePath2 { get; set; }

        [DisplayName("Image #3: ")]
        public string ImagePath3 { get; set; }

        //[Required(ErrorMessage = "La langue est obligatoire")]
        //[DisplayName("Langue: ")]
        //public int Language { get; set; }

        public bool isDefault { get; set; }

        public bool isActive { get; set; }
        public bool isDelete { get; set; }
    }
}
