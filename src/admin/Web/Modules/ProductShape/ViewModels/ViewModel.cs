﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SpekSkates.Admin.Web.Modules.ProductShape.ViewModels
{
    public class ViewModel
    {
        public ProductShape ProductShape { get; set; }

        public int[] ProductCategoryIds { get; set; }
        public List<SelectListItem> ProductCategorySelectList { set; get; }

        public string ErrorMessage { get; set; }
    }
}
