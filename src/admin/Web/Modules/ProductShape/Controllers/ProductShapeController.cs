﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using SpekSkates.Admin.Web.Modules.ProductCategory;
using SpekSkates.Admin.Web.Modules.ProductShape.ViewModels;

namespace SpekSkates.Admin.Web.Modules.ProductShape.Controllers
{
    [Authorize]
    public class ProductShapeController : Controller
    {

        private IRepository<ProductShape> _itemRepository;
        private IRepository<ProductCategory.ProductCategory> _productCategoryRepository;

        public ProductShapeController(IRepository<ProductShape> productShapeRepository,
            IRepository<ProductCategory.ProductCategory> productCategoryRepository)
        {
            _itemRepository = productShapeRepository;
            _productCategoryRepository = productCategoryRepository;
        }

        // GET: ProductShape
        public ActionResult Index()
        {
            return View("~/views/ProductShape/index.cshtml", _itemRepository.GetAll().Where(s => !s.isDelete).ToList());
        }

        // GET: ProductShape/Create
        public ActionResult Create()
        {
            var mv = new ViewModel();

            mv.ProductShape = new ProductShape
            {
                Name = "",
            };
            return View(mv);
        }

        // POST: ProductShape/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ViewModel mv)
        {

            if (ModelState.IsValid)
            {
                if (_itemRepository.GetAll().Where(s => s.Name == mv.ProductShape.Name && !s.isDelete).Any())
                {
                    mv.ErrorMessage = "Ce nom de catégorie existe déjà";
                    return View(mv);
                }
                _itemRepository.Update(mv.ProductShape);
                _itemRepository.Save(mv.ProductShape);

                return RedirectToAction("Edit", new { id = mv.ProductShape.Id });
            }
            return View(mv);

        }

        // GET: ProductShape/Edit/5
        public IActionResult Edit(int? id)
        {
            var mv = new ViewModel();

            if (id == null)
            {
                return NotFound();
            }

            mv.ProductShape = Helper.GetHomePageWithInclude(_itemRepository.Get(id));

            if (mv.ProductShape == null)
            {
                return NotFound();
            }
            mv.ProductCategorySelectList = new List<SelectListItem>();
            PrepMultiSelect(mv);

            return View(mv);
        }

        // POST: ProductShape/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ViewModel mv)
        {
            if (id != mv.ProductShape.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (_itemRepository.GetAll().Where(s => s.Name == mv.ProductShape.Name && s.Id != mv.ProductShape.Id && !s.isDelete).Any())
                {
                    mv.ErrorMessage = "Ce nom de catégorie existe déjà";
                    return View(mv);
                }
    
                try
                {
                    _itemRepository.Update(mv.ProductShape);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductShapeExists(mv.ProductShape.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                MultiSelectProcess(mv);
                return RedirectToAction(nameof(Index));
            }
            return View(mv);
        }


        // GET: ProductShape/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productShape = _itemRepository.Get(id).FirstOrDefault();

            if (productShape == null)
            {
                return NotFound();
            }

            return View(productShape);
        }

        // POST: ProductShape/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var item = _itemRepository.Get(id).FirstOrDefault();
            item.isDelete = true;
            _itemRepository.Update(item);
            return RedirectToAction(nameof(Index));
        }



        private bool ProductShapeExists(int id)
        {
            return _itemRepository.Get(id) != null;
        }

        private void PrepMultiSelect(ViewModel mv)
        {
            var productCategoryList = _productCategoryRepository.GetAll().Where(x=>!x.isDelete).ToList();
            mv.ProductCategorySelectList = new List<SelectListItem>();
            foreach (var productCategory in productCategoryList)
            {
                mv.ProductCategorySelectList.Add(new SelectListItem
                {
                    Text = productCategory.Name,
                    Value = productCategory.Id.ToString()
                });
            }

            mv.ProductCategoryIds =
                mv.ProductShape.ProductCategoriesProductShapes.Select(x => x.ProductCategoryId).ToArray();
        }

        private void MultiSelectProcess(ViewModel mv)
        {

            mv.ProductShape = Helper.GetHomePageWithInclude(_itemRepository.Get(mv.ProductShape.Id));

            var productCategories =
                _productCategoryRepository.GetAll().Where(s => mv.ProductCategoryIds.Contains(s.Id)).ToList();
            var productCategoryproductShapeSupress =
                mv.ProductShape.ProductCategoriesProductShapes.Where(
                    s =>
                        !productCategories.Select(c => c.Id).Contains(s.ProductCategoryId) &&
                        s.ProductShapeId == mv.ProductShape.Id).ToList();

            foreach (var productCategory in productCategories)
            {
                if (
                    !mv.ProductShape.ProductCategoriesProductShapes.Select(s => s.ProductCategoryId)
                        .Contains(productCategory.Id))
                {
                    mv.ProductShape.ProductCategoriesProductShapes.Add(new ProductCategoryProductShape
                    {
                        ProductCategory = productCategory,
                        ProductCategoryId = productCategory.Id,
                        ProductShapeId = mv.ProductShape.Id,
                        ProductShape = mv.ProductShape
                    });
                }
            }

            foreach (var toSupress in productCategoryproductShapeSupress)
            {
                mv.ProductShape.ProductCategoriesProductShapes.Remove(toSupress);
            }

            _itemRepository.Update(mv.ProductShape);
        }
    }
}