﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.ProductShape
{ 
    public static class Helper
    {


        public static ProductShape GetHomePageWithInclude(IQueryable<ProductShape> productShapes)
        {
            return productShapes
                .Include(i => i.ProductCategoriesProductShapes)
                .ThenInclude(y => y.ProductCategory)
                .FirstOrDefault();
        }
    }
}
