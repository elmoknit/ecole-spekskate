﻿using SpekSkates.Admin.Web.Base;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SpekSkates.Admin.Web.Modules.ProductShape
{
    public class ProductShape : BaseEntity
    {
        [Required(ErrorMessage = "Le nom de la forme est obligatoire")]
        [DisplayName("Nom de la forme :")]
        public string Name { get; set; }
        public bool isDelete { get; set; }
        public virtual ICollection<ProductCategory.ProductCategoryProductShape> ProductCategoriesProductShapes { get; set; }
    }

    
}
