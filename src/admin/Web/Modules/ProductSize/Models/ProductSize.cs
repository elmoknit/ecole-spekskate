﻿using SpekSkates.Admin.Web.Base;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SpekSkates.Admin.Web.Modules.ProductSize
{
    public class ProductSize : BaseEntity
    {
        [Required(ErrorMessage = "Le nom de la grandeur est obligatoire")]
        [DisplayName("Nom de la grandeur :")]
        public string Name { get; set; }
        public bool isDelete { get; set; }
        public virtual ICollection<ProductCategory.ProductCategoryProductSize> ProductCategoriesProductSizes { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    
}
