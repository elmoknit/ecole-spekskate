﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.ProductSize
{ 
    public static class Helper
    {


        public static ProductSize GetHomePageWithInclude(IQueryable<ProductSize> productSizes)
        {
            return productSizes
                .Include(i => i.ProductCategoriesProductSizes)
                .ThenInclude(y => y.ProductCategory)
                .FirstOrDefault();
        }
    }
}
