﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SpekSkates.Admin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.ProductSize
{
    public class ProductSizeMap
    {

        public ProductSizeMap(EntityTypeBuilder<ProductSize> entityBuilder)
        {
            entityBuilder.HasKey(t => t.Id);
            entityBuilder.Property(e => e.Name).IsRequired().HasMaxLength(100);

        }
    }
}
