﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using SpekSkates.Admin.Web.Modules.ProductCategory;
using SpekSkates.Admin.Web.Modules.ProductSize.ViewModels;

namespace SpekSkates.Admin.Web.Modules.ProductSize.Controllers
{
    [Authorize]
    public class ProductSizeController : Controller
    {

        private IRepository<ProductSize> _itemRepository;
        private IRepository<ProductCategory.ProductCategory> _productCategoryRepository;

        public ProductSizeController(IRepository<ProductSize> productSizeRepository,
            IRepository<ProductCategory.ProductCategory> productCategoryRepository)
        {
            _itemRepository = productSizeRepository;
            _productCategoryRepository = productCategoryRepository;
        }

        // GET: ProductSize
        public ActionResult Index()
        {
            return View("~/views/ProductSize/index.cshtml", _itemRepository.GetAll().Where(s => !s.isDelete).ToList());
        }

        // GET: ProductSize/Create
        public ActionResult Create()
        {
            var mv = new ViewModel();

            mv.ProductSize = new ProductSize
            {
                Name = "",
            };
            return View(mv);
        }

        // POST: ProductSize/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ViewModel mv)
        {

            if (ModelState.IsValid)
            {
                if (_itemRepository.GetAll().Where(s => s.Name == mv.ProductSize.Name && !s.isDelete).Any())
                {
                    mv.ErrorMessage = "Ce nom de catégorie existe déjà";
                    return View(mv);
                }
                _itemRepository.Update(mv.ProductSize);
                _itemRepository.Save(mv.ProductSize);

                return RedirectToAction("Edit", new {id = mv.ProductSize.Id});
            }
            return View(mv);

        }

        // GET: ProductSize/Edit/5
        public IActionResult Edit(int? id)
        {
            var mv = new ViewModel();

            if (id == null)
            {
                return NotFound();
            }

            mv.ProductSize = Helper.GetHomePageWithInclude(_itemRepository.Get(id));

            if (mv.ProductSize == null)
            {
                return NotFound();
            }

            mv.ProductCategorySelectList = new List<SelectListItem>();
            PrepMultiSelect(mv);

            return View(mv);
        }

        // POST: ProductSize/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ViewModel mv)
        {
            if (id != mv.ProductSize.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (_itemRepository.GetAll().Where(s => s.Name == mv.ProductSize.Name && s.Id != mv.ProductSize.Id && !s.isDelete).Any())
                {
                    mv.ErrorMessage = "Ce nom de forme existe déjà";
                    return View(mv);
                }
                try
                {
                    _itemRepository.Update(mv.ProductSize);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductSizeExists(mv.ProductSize.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                MultiSelectProcess(mv);
                return RedirectToAction(nameof(Index));
            }
            return View(mv);
        }


        // GET: ProductSize/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productSize = _itemRepository.Get(id).FirstOrDefault();

            if (productSize == null)
            {
                return NotFound();
            }

            return View(productSize);
        }

        // POST: ProductSize/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var item = _itemRepository.Get(id).FirstOrDefault();
            item.isDelete = true;
            _itemRepository.Update(item);
            return RedirectToAction(nameof(Index));
        }



        private bool ProductSizeExists(int id)
        {
            return _itemRepository.Get(id) != null;
        }

        private void PrepMultiSelect(ViewModel mv)
        {
            var productCategoryList = _productCategoryRepository.GetAll().Where(x=>!x.isDelete).ToList();
            mv.ProductCategorySelectList = new List<SelectListItem>();
            foreach (var productCategory in productCategoryList)
            {
                mv.ProductCategorySelectList.Add(new SelectListItem
                {
                    Text = productCategory.Name,
                    Value = productCategory.Id.ToString()
                });
            }

            mv.ProductCategoryIds =
                mv.ProductSize.ProductCategoriesProductSizes.Select(x => x.ProductCategoryId).ToArray();
        }

        private void MultiSelectProcess(ViewModel mv)
        {

            mv.ProductSize = Helper.GetHomePageWithInclude(_itemRepository.Get(mv.ProductSize.Id));

            var productCategories =
                _productCategoryRepository.GetAll().Where(s => mv.ProductCategoryIds.Contains(s.Id)).ToList();
            var productCategortproductSizeSupress =
                mv.ProductSize.ProductCategoriesProductSizes.Where(
                    s =>
                        !productCategories.Select(c => c.Id).Contains(s.ProductCategoryId) &&
                        s.ProductSizeId == mv.ProductSize.Id).ToList();

            foreach (var productCategory in productCategories)
            {
                if (
                    !mv.ProductSize.ProductCategoriesProductSizes.Select(s => s.ProductCategoryId)
                        .Contains(productCategory.Id))
                {
                    mv.ProductSize.ProductCategoriesProductSizes.Add(new ProductCategoryProductSize
                    {
                        ProductCategory = productCategory,
                        ProductCategoryId = productCategory.Id,
                        ProductSizeId = mv.ProductSize.Id,
                        ProductSize = mv.ProductSize
                    });
                }
            }

            foreach (var toSupress in productCategortproductSizeSupress)
            {
                mv.ProductSize.ProductCategoriesProductSizes.Remove(toSupress);
            }

            _itemRepository.Update(mv.ProductSize);
        }

    }
}