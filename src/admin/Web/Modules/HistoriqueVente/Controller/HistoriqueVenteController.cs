﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.IO;

namespace SpekSkates.Admin.Web.Modules.HistoriqueVente.Controllers
{
    public class HistoriqueVenteController : Controller
    {
        public IActionResult Index()
        {
            var client = new HttpClient();

            // Create the HttpContent for the form to be posted.
            var requestContent = new FormUrlEncodedContent(new[] {
    new KeyValuePair<string, string>("text", "This is a block of text"),
                });

            // Get the response.
            HttpResponseMessage response =  client.PostAsync(
                "http://api.repustate.com/v2/demokey/score.json",
                requestContent).Result;

            // Get the response content.
            HttpContent responseContent = response.Content;

            // Get the stream of the content.
            using (var reader = new StreamReader(responseContent.ReadAsStreamAsync().Result))
            {
                // Write the output.
                Console.WriteLine(reader.ReadToEndAsync().Result);
            }
            return View();
        }
    }
}