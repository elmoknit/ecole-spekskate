﻿using SpekSkates.Admin.Web.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage.Blob;
using SpekSkates.Admin.Web.Modules.Slider;

namespace SpekSkates.Admin.Web.Modules.HomePage
{

    public class HomePage : BaseEntity
    {

        [Required(ErrorMessage = "Le nom de la page est obligatoire")]
        [DisplayName("Nom de la page :")]
        public string Name { get; set; }

        [DisplayName("Image de la section «Personnalisation» : ")]
        public string PersoImagePath { get; set; }
        [Required(ErrorMessage = "Le résumé de la «Personnalisation» est obligatoire")]
        [DisplayName("Résumé de la section «Personnalisation» : ")]
        public string PersoContent { get; set; }

        [Required(ErrorMessage = "Titre de la section «À propos» est obligatoire")]
        [DisplayName("Titre de la section «À propos» : ")]
        public string AboutTitle { get; set; }
        [Required(ErrorMessage = "Résumé de la section «À propos» est obligatoire")]
        [DisplayName("Résumé de la section «À propos» :")]
        public string AboutText { get; set; }
        [DisplayName("Image de la section «À propos» :")]
        public string AboutImagePath { get; set; }

        public bool isDefault { get; set; }

        public bool isActive { get; set; }

        public bool isDelete { get; set; }

        [DisplayName("Images de la bannière :")]
        public virtual ICollection<HomePageSlider> HomePageSliders { get; set; }
        [DisplayName("Produits vedettes :")]
        public virtual ICollection<HomePageProduct> HomePageProducts { get; set; }

        public string testMigration1 { get; set; }
    }


    public class HomePageSlider
    {
        public int HomePageId { get; set; }
        public HomePage HomePage { get; set; }
        public int SliderId { get; set; }
        public Slider.Slider Slider { get; set; }
    }

    public class HomePageProduct
    {
        public int HomePageId { get; set; }
        public HomePage HomePage { get; set; }
        public int ProductId { get; set; }
        public Product.Product Product { get; set; }
    }
    
    public class HomePageSmall
    {

        public string Name { get; set; }

        public string PersoImagePath { get; set; }

        public string PersoContent { get; set; }

        public string AboutTitle { get; set; }

        public string AboutText { get; set; }

        public string AboutImagePath { get; set; }

        public List<Slider.Slider> Sliders { get; set; }

        public List<Product.Product> Products { get; set; }


    }
}
