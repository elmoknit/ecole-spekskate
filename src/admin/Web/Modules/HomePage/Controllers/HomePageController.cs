﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SpekSkates.Admin.Web;
using SpekSkates.Admin.Web.Models;
using SpekSkates.Admin.Web.Base;
using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob; // Namespace for Blob storage types
using Microsoft.Azure;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.HomePage.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace SpekSkates.Admin.Web.Modules.HomePage
{
    [Authorize]
    public class HomePageController : Controller
    {
        private IRepository<HomePage> _itemRepository;
        private IRepository<Slider.Slider> _sliderRepository;
        private IRepository<Product.Product> _produitRepository;
        private readonly IAzureBlobStorage _blobStorage;



        public HomePageController(IRepository<HomePage> homePageRepository, IRepository<Slider.Slider> sliderRepository, IRepository<Product.Product> produitRepository, IAzureBlobStorage blobStorage)
        {
            _itemRepository = homePageRepository;
            _sliderRepository = sliderRepository;
            _produitRepository = produitRepository;
            _blobStorage = blobStorage;
        }


        // GET: HomePages
        public IActionResult Index()
        {
            return View("~/views/homePage/index.cshtml", _itemRepository.GetAll().Where(s => !s.isDelete).ToList());
        }



        // GET: HomePages/Edit/5
        public IActionResult Edit(int? id)
        {
            var mv = new ViewModelDetail();

            if (id == null)
            {
                return NotFound();
            }

            mv.HomePage = HomePageHelper.GetHomePageWithInclude(_itemRepository.Get(id));

            if (mv.HomePage == null)
            {
                return NotFound();
            }

            prepMultiSelect(mv);




            return View(mv);
        }


        // POST: HomePages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, ViewModelDetail mv)
        {

            if (id != mv.HomePage.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var fileNameAbout = "";
                if (mv.FileAbout != null && mv.FileAbout.Length != 0)
                { fileNameAbout = mv.FileAbout.GetFilename(); }

                if (UploadFile(mv.FileAbout, fileNameAbout))
                {
                    mv.HomePage.AboutImagePath = IFromFileExtensions.ImagePath(fileNameAbout);
                }

                var fileNamePerso = "";
                if (mv.FilePerso != null && mv.FilePerso.Length != 0)
                { fileNamePerso = mv.FilePerso.GetFilename(); }

                if (UploadFile(mv.FilePerso, fileNamePerso))
                {
                    mv.HomePage.PersoImagePath = IFromFileExtensions.ImagePath(fileNamePerso);
                }

                try
                {
                    _itemRepository.Update(mv.HomePage);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HomePageExists(mv.HomePage.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                multiSelectProcess(mv);
                return RedirectToAction(nameof(Index));
            }
            return View(mv);
        }


        public IActionResult Delete(string blobName, int id, string identifiant)
        {

            if (!string.IsNullOrEmpty(blobName))
            {
                var imageName = blobName.Split('/');
                _blobStorage.DeleteAsync(imageName[imageName.Count() - 1]);
                var homePage = _itemRepository.Get(id).FirstOrDefault();

                if (homePage == null)
                {
                    return NotFound();
                }
                if (identifiant == "about")
                {
                    homePage.AboutImagePath = "";
                }
                else if (identifiant == "perso")
                {
                    homePage.PersoImagePath = "";
                }
                _itemRepository.Save(homePage);
                return RedirectToAction("Edit", new { id = homePage.Id });
            }
            else
            {
                return NotFound();
            }



        }


        private bool HomePageExists(int id)
        {
            return _itemRepository.Get(id) != null;
        }


        private bool UploadFile(IFormFile file, string blobName)
        {
            if (file == null || file.Length == 0)
            { return false; }

            //source : https://github.com/TahirNaushad/Fiver.Azure.Blob/tree/master/Fiver.Azure.Blob.Client
            var fileStream = file.GetFileStream();

            _blobStorage.UploadAsync(blobName, fileStream);
            return true;
        }

        private void prepMultiSelect(ViewModelDetail mv)
        {
            var sliderList = _sliderRepository.GetAll().ToList();
            mv.SliderSelectList = new List<SelectListItem>();
            foreach (var slider in sliderList)
            {
                mv.SliderSelectList.Add(new SelectListItem { Text = slider.Name, Value = slider.Id.ToString() });
            }

            if(mv.HomePage.HomePageSliders != null)
            {
                mv.SliderIds = mv.HomePage.HomePageSliders.Select(x => x.SliderId).ToArray();
            }



            var produitList = _produitRepository.GetAll().Where(z => z.isActive && !z.isDelete).ToList();
            mv.ProductSelectList = new List<SelectListItem>();
            foreach (var produit in produitList)
            {
                mv.ProductSelectList.Add(new SelectListItem { Text = produit.Name, Value = produit.Id.ToString() });
            }

            if (mv.HomePage.HomePageProducts != null)
            {
                mv.ProductIds = mv.HomePage.HomePageProducts.Select(x => x.ProductId).ToArray();
            }
        }

        private void multiSelectProcess(ViewModelDetail mv)
        {
            //Slider
            mv.HomePage = HomePageHelper.GetHomePageWithInclude(_itemRepository.Get(mv.HomePage.Id));

            var sliders = _sliderRepository.GetAll().Where(s => mv.SliderIds.Contains(s.Id)).ToList();
            var homePagesliderSupress = mv.HomePage.HomePageSliders.Where(s => !sliders.Select(c => c.Id).Contains(s.SliderId) && s.HomePageId == mv.HomePage.Id).ToList();

            foreach (var slider in sliders)
            {
                if (!mv.HomePage.HomePageSliders.Select(s => s.SliderId).Contains(slider.Id))
                {
                    mv.HomePage.HomePageSliders.Add(new HomePageSlider { HomePageId = mv.HomePage.Id, HomePage = mv.HomePage, SliderId = slider.Id, Slider = slider });
                }
            }

            foreach (var homePageSliderToSupress in homePagesliderSupress)
            {
                mv.HomePage.HomePageSliders.Remove(homePageSliderToSupress);
            }


            //Product
            var products = _produitRepository.GetAll().Where(s => mv.ProductIds.Contains(s.Id)).ToList();
            var homePageProductSupress = mv.HomePage.HomePageProducts.Where(s => !products.Select(c => c.Id).Contains(s.ProductId) && s.HomePageId == mv.HomePage.Id).ToList();

            foreach (var product in products)
            {
                if (!mv.HomePage.HomePageProducts.Select(s => s.ProductId).Contains(product.Id))
                {
                    mv.HomePage.HomePageProducts.Add(new HomePageProduct { HomePageId = mv.HomePage.Id, HomePage = mv.HomePage, ProductId = product.Id, Product = product });
                }
            }

            foreach (var homePageProductToSupress in homePageProductSupress)
            {
                mv.HomePage.HomePageProducts.Remove(homePageProductToSupress);
            }


            _itemRepository.Update(mv.HomePage);
        }


        public IActionResult Create()
        {
            var mv = new ViewModelDetail();

            mv.HomePage = new HomePage
            {
                Name = "",
                PersoContent = "",
                AboutTitle = "",
                AboutText = "",
                isDefault = false,
                isActive = true,
                isDelete = false
            };

            prepMultiSelect(mv);


            return View(mv);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ViewModelDetail mv)
        {
            if (ModelState.IsValid)
            {
                var fileNameAbout = "";
                if (mv.FileAbout != null && mv.FileAbout.Length != 0)
                { fileNameAbout = mv.FileAbout.GetFilename(); }

                if (UploadFile(mv.FileAbout, fileNameAbout))
                {
                    mv.HomePage.AboutImagePath = IFromFileExtensions.ImagePath(fileNameAbout);
                }

                var fileNamePerso = "";
                if (mv.FilePerso != null && mv.FilePerso.Length != 0)
                { fileNamePerso = mv.FilePerso.GetFilename(); }

                if (UploadFile(mv.FilePerso, fileNamePerso))
                {
                    mv.HomePage.PersoImagePath = IFromFileExtensions.ImagePath(fileNamePerso);
                }

                try
                {
                    _itemRepository.Update(mv.HomePage);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HomePageExists(mv.HomePage.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                multiSelectProcess(mv);
                return RedirectToAction(nameof(Index));
            }
            return View(mv);
        }


        public IActionResult Default(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var homePage = _itemRepository.Get(id).FirstOrDefault();
            var homepageRemoveDefault = _itemRepository.GetAll().Where(s => s.isDefault).FirstOrDefault();
            if (homePage == null)
            {
                return NotFound();
            }

            if (homepageRemoveDefault != null)
            {
                homepageRemoveDefault.isDefault = false;
                _itemRepository.Update(homepageRemoveDefault);
            }

            homePage.isDefault = true;
            homePage.isActive = false;
            _itemRepository.Update(homePage);


            return RedirectToAction(nameof(Index));
        }

        public IActionResult Active(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var homePage = _itemRepository.Get(id).FirstOrDefault();
            if (homePage == null)
            {
                return NotFound();
            }

            homePage.isActive = true;

            _itemRepository.Update(homePage);


            return RedirectToAction(nameof(Index));
        }


        public IActionResult UnActive(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var homePage = _itemRepository.Get(id).FirstOrDefault();
            if (homePage == null)
            {
                return NotFound();
            }

            homePage.isActive = false;

            _itemRepository.Update(homePage);


            return RedirectToAction(nameof(Index));
        }


        public IActionResult DeletePage(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var page = _itemRepository.Get(id).FirstOrDefault();

            if (page == null)
            {
                return NotFound();
            }

            return View(page);
        }

        public IActionResult DeleteItem(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var homePage = _itemRepository.Get(id).FirstOrDefault();

            if (homePage == null)
            {
                return NotFound();
            }


            homePage.isDelete = true;
            homePage.isActive = false;
            _itemRepository.Update(homePage);


            return RedirectToAction(nameof(Index));
        }
    }
}
