﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.HomePage;


namespace SpekSkates.Admin.Web.Modules.Cms
{
    [Route("api/[controller]")]
    public class ApiHomePageController : Controller
    {
        IRepository<HomePage.HomePage> _homePageRepository;
        public ApiHomePageController(IRepository<HomePage.HomePage> homePageRepository)
        {
            _homePageRepository = homePageRepository;
        }
       

        [Route("[action]")]
        public IActionResult Default()
        {
            var result = HomePageHelper.GetHomePageSmall(HomePageHelper.GetHomePageWithInclude(_homePageRepository.GetAll().Where(s => s.isDefault && !s.isDelete)));
            var returnResult = Json("");
            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }


        [Route("[action]/{pageName}")]
        public IActionResult Custom(string pageName)
        {
            var returnResult = Json("");
            if (pageName == null || pageName == string.Empty)
            {
                returnResult = Json("Bad Request");
                returnResult.StatusCode = 400;
                return BadRequest(returnResult);
            }

            var result = HomePageHelper.GetHomePageSmall(HomePageHelper.GetHomePageWithInclude(_homePageRepository.GetAll().Where(s => s.Name == pageName && !s.isDelete)));
            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }


        [Route("[action]")]
        public IActionResult GetAllPageCustomName()
        {
            var result = _homePageRepository.GetAll().Where(s => !s.isDefault && s.isActive).Select(s => s.Name).ToList();
            var returnResult = Json("");
            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }

    }
}
