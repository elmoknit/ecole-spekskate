﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.HomePage
{
    public static class HomePageHelper
    {


        public static HomePage GetHomePageWithInclude(IQueryable<HomePage> homePage)
        {
            return homePage
                .Include(i => i.HomePageSliders)
                .ThenInclude(y => y.Slider)
                .Include(i => i.HomePageProducts)
                .ThenInclude(y => y.Product)
                .FirstOrDefault();
        }


        public static HomePageSmall GetHomePageSmall(HomePage homePage)
        {
            if (homePage == null)
            { return null; }
            var homePageSmall = new HomePageSmall { Name = homePage.Name, PersoImagePath = homePage.PersoImagePath, PersoContent = homePage.PersoContent, AboutTitle = homePage.AboutTitle, AboutText = homePage.AboutText, AboutImagePath = homePage.AboutImagePath, Sliders = homePage.HomePageSliders.Select(s => s.Slider).ToList(), Products = homePage.HomePageProducts.Select(s => s.Product).ToList() };

            foreach (var slider in homePageSmall.Sliders)
            {
                slider.HomePageSliders = null;
            }

            foreach (var product in homePageSmall.Products)
            {
                product.HomePageProducts = null;
            }

            return homePageSmall;
        }
    }
}
