﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using SpekSkates.Admin.Web.AzureBlob;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SpekSkates.Admin.Web.Modules.HomePage.ViewModels
{
    public class ViewModelDetail
    {


        public HomePage HomePage { get; set; }

        [DisplayName("Téléverser une image pour la section «À propos» (Taille suggérée : 1080px x 320px)")]
        public IFormFile FileAbout { get; set; }
        [DisplayName("Téléverser une image pour la section «Personnalisation» (Taille suggérée : 1080px x 320px)")]
        public IFormFile FilePerso { get; set; }

        public int[] SliderIds { get; set; }
        public List<SelectListItem> SliderSelectList { set; get; }


        public int[] ProductIds { get; set; }
        public List<SelectListItem> ProductSelectList { set; get; }


    }
}
