﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using SpekSkates.Admin.Web.AzureBlob;

namespace SpekSkates.Admin.Web.Modules.HomePage.ViewModels
{
    public class ViewModel
    {
        public List<HomePage> HomePage {get; set;}

        public IFormFile File { get; set; }
    }
}
