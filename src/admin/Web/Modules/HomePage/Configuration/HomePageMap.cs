﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SpekSkates.Admin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.HomePage
{
    public class HomePageMap
    {

        public HomePageMap(EntityTypeBuilder<HomePage> entityBuilder)
        {
            entityBuilder.HasKey(t => t.Id);
        }
    }
}
