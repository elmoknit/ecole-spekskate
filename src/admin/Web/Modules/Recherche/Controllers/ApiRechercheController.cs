﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Modules;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Product;
using Microsoft.EntityFrameworkCore;

namespace temp.Controllers
{
    [Route("api/[controller]")]
    public class ApiRechercheController : Controller
    {
            IRepository<Product> _productRepository;
            public ApiRechercheController(IRepository<Product> productRepository)
            {
                _productRepository = productRepository;
            }

            // GET api/values/5
            [HttpGet("{recherche}")]
            public IActionResult recherche([FromBody]string recherche)
            {
            var returnResult = Json("");
            if (recherche == null || recherche == string.Empty)
            {
                returnResult = Json("Bad Request");
                returnResult.StatusCode = 400;
                return BadRequest(returnResult);
            }

            var result = GetProductWithInclude(_productRepository.GetAll()).Where(x => x.Name.Contains(recherche) || x.Description.Contains(recherche) || x.ProductCategory.Name.Contains(recherche) || x.ProductShape.Name.Contains(recherche) ||x.ProductSize.Name.Contains(recherche));

            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }
        public IActionResult a()
        {
            return Ok("");
        }

        private static IQueryable<Product> GetProductWithInclude(IQueryable<Product> product)
        {
            return product
                .Include(i => i.BoardStyle)
                .Include(j => j.ProductCategory)
                .Include(k => k.ProductShape)
                .Include(l => l.ProductSize);

        }
    }
}