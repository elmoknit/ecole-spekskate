﻿using SpekSkates.Admin.Web.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.Slider
{

    public class Slider : BaseEntity
    {
        [Required(ErrorMessage = "Le nom de l'image est obligatoire")]
        [DisplayName("Nom de l'image :")]
        public string Name { get; set; }
        
        [DisplayName("Image :")]
        public string ImagePath { get; set; }
        [DisplayName("Text associé à l'image :")]
        public string Text { get; set; }
        [DisplayName("Lien associé à l'image :")]
        public string Link { get; set; }


        public virtual ICollection<HomePage.HomePageSlider> HomePageSliders { get; set; }


    }
}
