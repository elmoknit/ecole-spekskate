﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using SpekSkates.Admin.Web.AzureBlob;

namespace SpekSkates.Admin.Web.Modules.Slider
{
    public class ViewModelDetail
    {
        public Slider Slider { get; set; }

        [DisplayName("Téléverser une image (Taille suggérée : 1080px x 320px)")]
        public IFormFile File { get; set; }
    }
}
