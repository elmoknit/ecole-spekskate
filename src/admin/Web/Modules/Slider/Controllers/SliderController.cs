﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SpekSkates.Admin.Web;
using SpekSkates.Admin.Web.Models;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.AzureBlob;
using Microsoft.AspNetCore.Http;              
using Microsoft.AspNetCore.Authorization;
using SpekSkates.Admin.Web.Modules.HomePage;


namespace SpekSkates.Admin.Web.Modules.Slider
{
    [Authorize]
    public class SliderController : Controller
    {
        private IRepository<Slider> _itemRepository;
        private readonly IAzureBlobStorage _blobStorage;

        public SliderController(IRepository<Slider> sliderRepository, IAzureBlobStorage blobStorage)
        {
            _itemRepository = sliderRepository;
            _blobStorage = blobStorage;
        }


        // GET: Slider
        public IActionResult Index()
        {
            return View("~/views/slider/index.cshtml", _itemRepository.GetAll().ToList());
        }


        // GET: Slider/Create
        public IActionResult Create()
        {
            var mv = new ViewModelDetail { Slider = new Slider() };
            return View(mv);
        }

        // POST: Slider/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ViewModelDetail mv)
        {
            if (ModelState.IsValid)
            {
                var fileName = "";
                if (mv.File != null && mv.File.Length != 0)
                { fileName = mv.File.GetFilename(); }
                if (UploadFile(mv.File, fileName))
                {
                    mv.Slider.ImagePath = IFromFileExtensions.ImagePath(fileName);
                }
                _itemRepository.Update(mv.Slider);
                _itemRepository.Save(mv.Slider);
                return RedirectToAction(nameof(Index));
            }
            return View(mv.Slider);
        }

        // GET: Slider/Edit/5
        public IActionResult Edit(int? id)
        {
            var mv = new ViewModelDetail();
            if (id == null)
            {
                return NotFound();
            }
            mv.Slider = _itemRepository.Get(id).FirstOrDefault();

            if (mv.Slider == null)
            {
                return NotFound();
            }
            return View(mv);
        }

        // POST: Slider/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, ViewModelDetail mv)
        {
            if (id != mv.Slider.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var fileName = "";
                if (mv.File != null && mv.File.Length != 0)
                { fileName = mv.File.GetFilename(); }
                if (UploadFile(mv.File, fileName))
                {
                    mv.Slider.ImagePath = IFromFileExtensions.ImagePath(fileName);
                }
                try
                {
                    _itemRepository.Update(mv.Slider);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SliderExists(mv.Slider.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(mv);
        }

        // GET: Slider/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var slider = _itemRepository.Get(id).FirstOrDefault();

            if (slider == null)
            {
                return NotFound();
            }

            return View(slider);
        }

        // POST: Slider/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            _itemRepository.Delete(id);
            return RedirectToAction(nameof(Index));
        }

        private bool SliderExists(int id)
        {
            return _itemRepository.Get(id) != null;
        }

        public IActionResult Deleteblob(string blobName, int id)
        {
            if (!string.IsNullOrEmpty(blobName))
            {
                var imageName = blobName.Split('/');
                _blobStorage.DeleteAsync(imageName[imageName.Count() - 1]);
                var slider = _itemRepository.Get(id).FirstOrDefault();
                slider.ImagePath = "";
                _itemRepository.Save(slider);

                return RedirectToAction(nameof(Index));
            }

            return NotFound();


        }

        private bool UploadFile(IFormFile file, string blobName)
        {
            if (file == null || file.Length == 0)
            { return false; }

            //source : https://github.com/TahirNaushad/Fiver.Azure.Blob/tree/master/Fiver.Azure.Blob.Client
            var fileStream = file.GetFileStream();

            _blobStorage.UploadAsync(blobName, fileStream);
            return true;
        }
    }
}
