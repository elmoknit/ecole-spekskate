﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SpekSkates.Admin.Web;
using SpekSkates.Admin.Web.Models;
using SpekSkates.Admin.Web.Base;
using Microsoft.AspNetCore.Identity;
using SpekSkates.Admin.Web.Modules.Utilisateur.Email;
using SpekSkates.Admin.Web.Modules.Utilisateur.Module_commun;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;

namespace SpekSkates.Admin.Web.Modules.Page
{
    public class UtilisateurController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly IMessageService _messageService;
        private string absUrl;

        public UtilisateurController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, IMessageService messageService)
        {
            this._userManager = userManager;
            this._signInManager = signInManager;

            this._messageService = messageService;
        }

        //public IActionResult Register()
        //{
        //    return View();
        //}


        public IActionResult Login()
        {
            return View("Login");
        }

        [HttpPost]
        public IActionResult Login(string email, string password, bool rememberMe)
        {
            if(email == string.Empty || password == string.Empty || email == null || password == null)
            {
                if(email == string.Empty || email == null)
                    ModelState.AddModelError(string.Empty, "Le email est obligatoire");

                if (password == string.Empty || password == null)
                    ModelState.AddModelError(string.Empty, "Le mot de passe est obligatoire");

                return View();
            }
            var user = _userManager.FindByEmailAsync(email).Result;
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, "Le nom d'utilisateur ou le mot de passe est incorrect");
                return View();
            }

            var passwordSignInResult =  _signInManager.PasswordSignInAsync(user, password, isPersistent: rememberMe, lockoutOnFailure: false).Result;
            if (!passwordSignInResult.Succeeded)
            {
                ModelState.AddModelError(string.Empty, "Le nom d'utilisateur ou le mot de passe est incorrect");
                return View();
            }

            return Redirect("~/");
        }

        public IActionResult ForgotPassword()
        {
            return View("ForgotPassword");
        }

        public IActionResult AddAdmin()
        {
            return View("AddAdmin");
        }

   

        [HttpPost]
        public IActionResult ForgotPassword(string email)
        {
            if(email == null || email == string.Empty)
            {
                ModelState.AddModelError(string.Empty, "Le courriel ne peut être vide");
                return View();
            }

            var user = _userManager.FindByEmailAsync(email).Result;
            if (user == null)
                return Content("Email introuvable");

            var passwordResetToken = _userManager.GeneratePasswordResetTokenAsync(user).Result;
            var passwordResetUrl = Url.Action("ResetPassword", "Utilisateur", new { id = user.Id, token = passwordResetToken }, Request.Scheme);

            _messageService.Send(email, "Mot de passe réinitialisé", $"Cliquez <a href=\"" + passwordResetUrl + "\">ici</a> pour réinitialiser votre mot de passe","");

            return View("Index", _userManager.Users.ToList());
        }



        [AllowAnonymous]
        public ActionResult ResetPassword(string token)
        {
            return View("ResetPassword");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid || model == null)
            {
                return View("ResetPassword",model);
            }
            var user = _userManager.FindByNameAsync(model.Email).Result;
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return View();
            }
            var result = _userManager.ResetPasswordAsync(user, model.Token, model.Password).Result;
            if (result.Succeeded)
            {

                if (_userManager.IsEmailConfirmedAsync(user).Result == false){
                    var emailConfirmationToken =  _userManager.GenerateEmailConfirmationTokenAsync(user).Result;
                    var emailConfirmationResult =  _userManager.ConfirmEmailAsync(user, emailConfirmationToken).Result;
                }
                return View("Index", _userManager.Users.ToList());
            }
            return View();
        }



        public IActionResult Logout()
        {
            _signInManager.SignOutAsync();

            return Redirect("~/");
        }
        //[HttpPost]
        //public IActionResult Register(string email, string password, string repassword)
        //{
        //    if (password != repassword)
        //    {
        //        ModelState.AddModelError(string.Empty, "Le mot de passe n'est pas identique");
        //        return View("Register");
        //    }

        //    var newUser = new IdentityUser
        //    {
        //        UserName = email,
        //        Email = email
        //    };

        //    var userCreationResult =  _userManager.CreateAsync(newUser, password).Result;
        //    if (!userCreationResult.Succeeded)
        //    {
        //        foreach (var error in userCreationResult.Errors)
        //            ModelState.AddModelError(string.Empty, error.Description);
        //        return View();
        //    }
            
        //    return Content("Vous aurez une confirmation par courriel");
        //}

        public  IActionResult VerifyEmail(string id, string token)
        {
            var user =  _userManager.FindByIdAsync(id).Result;
            if (user == null)
                throw new InvalidOperationException();

            var emailConfirmationResult =  _userManager.ConfirmEmailAsync(user, token).Result;
            if (!emailConfirmationResult.Succeeded)
                return Content(emailConfirmationResult.Errors.Select(error => error.Description).Aggregate((allErrors, error) => allErrors += ", " + error));

            return Content("Courriel validé. Vous pouvez maintenant vous connecter.");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public IActionResult AddAdmin(string email)
        {
            var newUser = new IdentityUser
            {
                UserName = email,
                Email = email
            };

            if (_userManager.Users.Where(x=>x.Email == email).Count() != 0)
            {
                ModelState.AddModelError(string.Empty, "Cet courriel est déjà utilisé.");
                return View();
            }

            var userCreationResult =  _userManager.CreateAsync(newUser, ModuleCommun.RandomString(48)).Result;
            if (!userCreationResult.Succeeded)
            {
                foreach (var error in userCreationResult.Errors)
                    ModelState.AddModelError(string.Empty, error.Description);
                return View();
            }
            var passwordResetToken =  _userManager.GeneratePasswordResetTokenAsync(newUser).Result;
            var passwordResetUrl = Url.Action("ResetPassword", "Utilisateur", new { id = newUser.Id, token = passwordResetToken }, Request.Scheme);

            _messageService.Send(email, "Mot de passe réinitiallisé", $"Cliquez <a href=\"" + passwordResetUrl + "\">ici</a> pour réinitialliser votre mot de passe","");

            return View("Index", _userManager.Users.ToList());
        }

        // GET: Skates
        public IActionResult Index()
        {
                return View("Index", _userManager.Users.ToList());
        }

        // GET: Skates/Delete/5
        [ActionName("Delete")]
        [Authorize]
        public IActionResult DeleteAsync(string id)
        {
            if (id == string.Empty)
            {
                return NotFound();
            }

            IdentityUser user =  _userManager.FindByIdAsync(id).Result;

            if (user == null)
            {
                return NotFound();
            }

            if(_userManager.GetUserAsync(HttpContext.User).Result.Id == _userManager.FindByIdAsync(id).Result.Id)
            {
                return Content("Vous ne pouvez pas vous supprimer vous-même");
            }

            return View("Delete",user);
        }

        // POST: Utilisateur/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public  IActionResult DeleteConfirmedAsync(string id)
        {
            IdentityUser user =  _userManager.FindByIdAsync(id).Result;
            var a = _userManager.DeleteAsync(user).Result;
            return RedirectToAction(nameof(Index));
        }

        public IActionResult changePassword()
        {
            //changePassword model = new changePassword();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public IActionResult changePassword(changePassword model)
        {
            
            if (!ModelState.IsValid || model == null)
            {
                return View("changePassword", model);
            }
            var user = _userManager.FindByNameAsync(HttpContext.User.Identity.Name).Result;
            if (user == null)
            {
                // Don't reveal that the user does not exist
                model.ErrorMessage = "Une erreur s'est produite. ";
            }

            bool isOk = _userManager.CheckPasswordAsync(user, model.OldPassword).Result;
            if (!isOk)
            {
                model.ErrorMessage = "Mauvais mot de passe. ";
            }

            var passwordResetToken = _userManager.GeneratePasswordResetTokenAsync(user).Result;

            var result = _userManager.ResetPasswordAsync(user, passwordResetToken, model.Password).Result;
            if (result.Succeeded)
            {
                model.ConfirmationMessage = "Mot de passe changé avec succès! ";
            }
            return View("changePassword", model);
        }

        public ActionResult Download()
        {
            var fileName = $"guide_utilisateur.pdf";
            var filepath = $"{fileName}";
            try
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(filepath);
                return File(fileBytes, "application/x-msdownload", fileName);
            }
            catch (Exception e)
            {
                return NotFound();
            }
        }


    }



}