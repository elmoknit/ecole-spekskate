﻿using System.Net.Mail;
using System.Net;

namespace SpekSkates.Admin.Web.Modules.Utilisateur.Email
{
    public class FileMessageService : IMessageService
    {
        public bool Send(string To, string subject, string message, string From = "")
        {
            try
            {
                SmtpClient client = new SmtpClient("smtp.mailgun.org", 587);
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("postmaster@sandboxdb3ee00d3d6c4d9aa01ad59ccbffff78.mailgun.org", "6df303b2a5d7e8e554cfd95a888b8ba8");

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress("postmaster@sandboxdb3ee00d3d6c4d9aa01ad59ccbffff78.mailgun.org");

                mailMessage.To.Add(To);
                mailMessage.Body = message;
                mailMessage.Subject = subject;
                if(From != string.Empty)
                {
                  mailMessage.From = new MailAddress(From);
                }

                client.Send(mailMessage);
                return true;
            }
            catch
            {
                return false;
            }
        }
        
    }
}
