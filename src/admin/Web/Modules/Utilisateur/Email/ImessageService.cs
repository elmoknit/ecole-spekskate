﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.Utilisateur.Email
{
    public interface IMessageService
    {
       bool Send(string email, string subject, string message, string From);
    }
}
