﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Base;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using SpekSkates.Admin.Web.Modules.Team.Models;
using SpekSkates.Admin.Web.Modules.Contact;
using SpekSkates.Admin.Web.Modules.Utilisateur.Email;

namespace temp.Controllers
{
    [Route("api/[controller]")]
    public class apiContactController : Controller
    {
        IRepository<Contact> _contactRepository;
        public apiContactController(IRepository<Contact> contactRepository)
        {
            _contactRepository = contactRepository;
        }

        // GET api/contact
        //[HttpGet]
        //public string Get()
        //{
        //    //faire des requetes en linq pour aller chercher les infos 
        //    // a parler avec l'equipe pour savoir comment gerer la bd
        //    // mon clavier a pas d'accent alors desole pour les fautes

        //    var contactModelFound = _contactRepository.Get(1).FirstOrDefault();
        //    return JsonConvert.SerializeObject(contactModelFound);

        //    //return "Quia consequuntur enim labore quos ipsum explicabo autem eveniet. Minus et molestiae provident ullam iusto dolor fugit. Delectus dolores laborum iure et quae quia";
        //}

        [Route("[action]")]
        public IActionResult Default()
        {


            var result = _contactRepository.GetAll().Where(s => s.isDefault && !s.isDelete).FirstOrDefault();
            var returnResult = Json("");
            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }


        [Route("[action]/{pageName}")]
        public IActionResult Custom(string pageName)
        {
            var returnResult = Json("");
            if (pageName == null || pageName == string.Empty)
            {
                returnResult = Json("Bad Request");
                returnResult.StatusCode = 400;
                return BadRequest(returnResult);
            }

            var result = _contactRepository.GetAll().Where(s => s.Name == pageName && !s.isDelete).FirstOrDefault();

            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }


        [Route("[action]")]
        public IActionResult GetAllPageCustomName()
        {
            var result = _contactRepository.GetAll().Where(s => !s.isDefault && s.isActive).Select(s => s.Name).ToList();
            var returnResult = Json("");
            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            //Un id ne peut pas etre nulle donc aucune validation
            return "value";
        }

        //A modifier
        [HttpPost]
        public string Post([FromForm]string title, [FromForm] string email, [FromForm]string message)
        {
            if (title == string.Empty || email == string.Empty || message == string.Empty || title == null || email == null || message == null)
            {
                return "Echec";
            }
            FileMessageService FMS = new FileMessageService();
            if (FMS.Send("spekskates.noreply@gmail.com", title, message, email)){
                return "Message envoyee avec success ";
            }
            return "Echec";
        }
    }
}
