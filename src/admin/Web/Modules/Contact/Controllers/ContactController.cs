﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace SpekSkates.Admin.Web.Modules.Contact.Controllers
{
    [Authorize]
    public class ContactController : Controller
    {

        private IRepository<Contact> _itemRepository;

        public ContactController(IRepository<Contact> homePageRepository)
        {
            _itemRepository = homePageRepository;
        }

        // GET: Contact
        public ActionResult Index()
        {
            return View("~/views/contact/index.cshtml", _itemRepository.GetAll().Where(s => !s.isDelete).ToList());
        }

        // GET: Contact/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Contact/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Contact/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Contact contact)
        {
            if (ModelState.IsValid)
            {
                contact.isActive = true;
                contact.isDefault = false;
                contact.isDelete = false;
                _itemRepository.Update(contact);
                _itemRepository.Save(contact);
                return RedirectToAction(nameof(Index));
            }
                return View(contact);
        }

        // GET: HomePages/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contact = _itemRepository.Get(id).FirstOrDefault();

            if (contact == null)
            {
                return NotFound();
            }
            return View(contact);
        }

        // POST: Contact/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Contact contact)
        {
            if (id != contact.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _itemRepository.Update(contact);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContactExists(contact.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(contact);
        }

        public IActionResult DeletePage(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var page = _itemRepository.Get(id).FirstOrDefault();

            if (page == null)
            {
                return NotFound();
            }

            return View(page);
        }

        // GET: Contact/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Contact/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        private bool ContactExists(int id)
        {
            return _itemRepository.Get(id) != null;
        }


        public IActionResult Default(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contact = _itemRepository.Get(id).FirstOrDefault();
            var contactRemoveDefault = _itemRepository.GetAll().Where(s => s.isDefault).FirstOrDefault();
            if (contact == null)
            {
                return NotFound();
            }

            if (contactRemoveDefault != null)
            {
                contactRemoveDefault.isDefault = false;
                _itemRepository.Update(contactRemoveDefault);
            }

            contact.isDefault = true;
            contact.isActive = false;
            _itemRepository.Update(contact);


            return RedirectToAction(nameof(Index));
        }

        public IActionResult Active(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contact = _itemRepository.Get(id).FirstOrDefault();
            if (contact == null)
            {
                return NotFound();
            }

            contact.isActive = true;

            _itemRepository.Update(contact);


            return RedirectToAction(nameof(Index));
        }


        public IActionResult UnActive(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contact = _itemRepository.Get(id).FirstOrDefault();
            if (contact == null)
            {
                return NotFound();
            }

            contact.isActive = false;

            _itemRepository.Update(contact);


            return RedirectToAction(nameof(Index));
        }


        // GET: HomePages/Edit/5
        public IActionResult DeleteItem(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contact = _itemRepository.Get(id).FirstOrDefault();

            if (contact == null)
            {
                return NotFound();
            }


            contact.isDelete = true;
            contact.isActive = false;
            _itemRepository.Update(contact);


            return RedirectToAction(nameof(Index));
        }
    }
}
