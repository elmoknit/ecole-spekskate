﻿   using System;
using System.Collections.Generic;
   using System.ComponentModel;
   using System.ComponentModel.DataAnnotations;
   using System.Linq;
using System.Threading.Tasks;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Team.Models;

namespace SpekSkates.Admin.Web.Modules.Contact
{
    public class Contact : BaseEntity
    {
        [Required(ErrorMessage = "Le nom de la page est obligatoire")]
        [DisplayName("Nom de la page :")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Le contenu de la page est obligatoire")]
        [DisplayName("Contenu :")]
        public string Content { get; set; }

        public bool isDefault { get; set; }

        public bool isActive { get; set; }


        //[DisplayName("Langue :")]
        //public int Language { get; set; }
        public bool isDelete { get; set; }
    }
}
