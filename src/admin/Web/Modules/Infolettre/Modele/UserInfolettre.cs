﻿using SpekSkates.Admin.Web.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.Infolettre.Modele
{
    public class UserInfolettre :BaseEntity
    {

        public string Nom { get; set; }
        public string email { get; set; }
    }
}
