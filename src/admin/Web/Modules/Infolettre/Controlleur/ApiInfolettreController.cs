﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Modules.Infolettre.Modele;
using SpekSkates.Admin.Web.Base;

namespace SpekSkates.Admin.Web.Modules.Infolettre.Controlleur
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ApiInfolettreController : Controller
    {
        IRepository<UserInfolettre> _info;
        public ApiInfolettreController(IRepository<UserInfolettre> info)
        {
            _info = info;
        }

        [HttpPost]
        public string Post([FromForm]string title, [FromForm] string email)
        {
            if(title == null || email == null || title ==string.Empty || email == string.Empty)
            {
                return "";
            }
            _info.Update(new UserInfolettre { email = email, Nom = title });
            return ("Vous avez été ajouté à notre infolettre");
        }
    }
}