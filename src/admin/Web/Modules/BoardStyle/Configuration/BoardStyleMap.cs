﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SpekSkates.Admin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.BoardStyle
{
    public class BoardStyleMap
    {

        public BoardStyleMap(EntityTypeBuilder<BoardStyle> entityBuilder)
        {
            entityBuilder.HasKey(t => t.Id);
            entityBuilder.Property(e => e.Name).IsRequired().HasMaxLength(100);

        }
    }
}
