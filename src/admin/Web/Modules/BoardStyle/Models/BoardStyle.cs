﻿using SpekSkates.Admin.Web.Base;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SpekSkates.Admin.Web.Modules.BoardStyle
{
    public class BoardStyle : BaseEntity
    {
        [Required(ErrorMessage = "Le nom du style de planche est obligatoire")]
        [DisplayName("Nom du style :")]
        public string Name { get; set; }


        [DisplayName("Image :")]
        public string ImagePath { get; set; }

        public bool isDelete { get; set; }
    }

    
}
