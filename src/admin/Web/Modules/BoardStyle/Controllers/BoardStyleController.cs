﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Modules.BoardStyle.ViewModels;

namespace SpekSkates.Admin.Web.Modules.BoardStyle.Controllers
{
    [Authorize]
    public class BoardStyleController : Controller
    {

        private IRepository<BoardStyle> _itemRepository;
        private IRepository<Modules.Product.Product> _productRepository;
        private readonly IAzureBlobStorage _blobStorage;

        public BoardStyleController(IRepository<BoardStyle> boardStyleRepository, IAzureBlobStorage blobStorage, IRepository<Modules.Product.Product> productRepository)
        {
            _itemRepository = boardStyleRepository;
            _productRepository = productRepository;
            _blobStorage = blobStorage;
        }

        // GET: BoardStyle
        public ActionResult Index()
        {
            return View("~/views/BoardStyle/index.cshtml", _itemRepository.GetAll().Where(s => !s.isDelete).ToList());
        }

        // GET: BoardStyle/Create
        public ActionResult Create()
        {
            var mv = new ViewModel();

            mv.BoardStyle = new BoardStyle
            {
                Name = "",
            };
            return View(mv);
        }

        // POST: BoardStyle/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ViewModel mv)
        {

            if (ModelState.IsValid)
            {
                var fileName = "";
                if (mv.File != null && mv.File.Length != 0)
                { fileName = mv.File.GetFilename(); }
                if (UploadFile(mv.File, fileName))
                {
                    mv.BoardStyle.ImagePath = IFromFileExtensions.ImagePath(fileName);
                }
                if (_itemRepository.GetAll().Where(s => s.Name == mv.BoardStyle.Name && !s.isDelete).Any())
                {
                    mv.ErrorMessage = "Ce nom de catégorie existe déjà";
                    return View(mv);
                }
                _itemRepository.Update(mv.BoardStyle);
                _itemRepository.Save(mv.BoardStyle);

                return RedirectToAction(nameof(Index));
            }
            return View(mv);

        }

        // GET: BoardStyle/Edit/5
        public IActionResult Edit(int? id)
        {
            var mv = new ViewModel();

            if (id == null)
            {
                return NotFound();
            }

            mv.BoardStyle = _itemRepository.Get(id).FirstOrDefault();

            if (mv.BoardStyle == null)
            {
                return NotFound();
            }

           return View(mv);
        }

        // POST: BoardStyle/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ViewModel mv)
        {
            if (id != mv.BoardStyle.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var fileName = "";
                if (mv.File != null && mv.File.Length != 0)
                { fileName = mv.File.GetFilename(); }
                if (UploadFile(mv.File, fileName))
                {
                    mv.BoardStyle.ImagePath = IFromFileExtensions.ImagePath(fileName);
                }
                if (_itemRepository.GetAll().Where(s => s.Name == mv.BoardStyle.Name && s.Id != mv.BoardStyle.Id && !s.isDelete).Any())
                {
                    mv.ErrorMessage = "Ce nom de catégorie existe déjà";
                    return View(mv);
                }

                try
                {
                    _itemRepository.Update(mv.BoardStyle);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BoardStyleExists(mv.BoardStyle.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(mv);
        }


        // GET: BoardStyle/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var mv = new ViewModelSupress();

            mv.BoardStyle = _itemRepository.Get(id).FirstOrDefault();

            if (mv.BoardStyle == null)
            {
                return NotFound();
            }
            mv.CanBeDeleted = true;
            mv.Products = _productRepository.GetAll().Where(s => s.BoardStyleId == id && !s.isDelete).ToList();
            if (mv.Products.Any())
            {
                mv.CanBeDeleted = false;
            }
            return View(mv);
        }

        // POST: BoardStyle/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var item = _itemRepository.Get(id).FirstOrDefault();
            item.isDelete = true;
            _itemRepository.Update(item);
            return RedirectToAction(nameof(Index));
        }



        private bool BoardStyleExists(int id)
        {
            return _itemRepository.Get(id) != null;
        }

        public IActionResult Deleteblob(string blobName, int id)
        {
            if (!string.IsNullOrEmpty(blobName))
            {
                var imageName = blobName.Split('/');
                _blobStorage.DeleteAsync(imageName[imageName.Count() - 1]);
                var boardStyle = _itemRepository.Get(id).FirstOrDefault();
                boardStyle.ImagePath = "";
                _itemRepository.Save(boardStyle);

                return RedirectToAction(nameof(Index));
            }

            return NotFound();


        }

        private bool UploadFile(IFormFile file, string blobName)
        {
            if (file == null || file.Length == 0)
            { return false; }

            //source : https://github.com/TahirNaushad/Fiver.Azure.Blob/tree/master/Fiver.Azure.Blob.Client
            var fileStream = file.GetFileStream();

            _blobStorage.UploadAsync(blobName, fileStream);
            return true;
        }

    }
}