﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SpekSkates.Admin.Web.Modules.BoardStyle.ViewModels
{
    public class ViewModelSupress
    {
        public BoardStyle BoardStyle { get; set; }

        public bool CanBeDeleted { get; set; }

        public List<Modules.Product.Product> Products { get; set; }
    }
}
