﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Team.Models;
using SpekSkates.Admin.Web.AzureBlob;
using Newtonsoft.Json;

namespace SpekSkates.Admin.Web.Modules.Team.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ApiEmployeeController : Controller
    {

        IRepository<Employee> _employeeRepository;
        private readonly IAzureBlobStorage _blobStorage;
        public ApiEmployeeController(IRepository<Employee> employeeRepository, IAzureBlobStorage blobStorage)
        {
            _employeeRepository = employeeRepository;
            _blobStorage = blobStorage;
        }

        // GET api/Employee
        [HttpGet]
        public IActionResult Get()
        {
            //faire des requetes en linq pour aller chercher les infos 
            // a parler avec l'equipe pour savoir comment gerer la bd
            // mon clavier a pas d'accent alors desole pour les fautes

            //var aboutModelFound = _employeeRepository.GetAll().ToList();
            //return JsonConvert.SerializeObject(aboutModelFound);

            var result = (_employeeRepository.GetAll().ToList());
            var returnResult = Json("");
            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);

    }


        //// GET: api/Employee/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/Employee
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/Employee/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
