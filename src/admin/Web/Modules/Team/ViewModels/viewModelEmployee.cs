﻿using Microsoft.AspNetCore.Http;
using SpekSkates.Admin.Web.Modules.Team.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.Team.ViewModels
{
    public class ViewModelEmployee
    {

        public Employee Employee { get; set; }

        [DisplayName("Téléverser une image (Taille suggérée : 350px x 350px)")]
        public IFormFile EmployeePicture { get; set; }

        public int aboutUsId { get; set; }
    }
}
