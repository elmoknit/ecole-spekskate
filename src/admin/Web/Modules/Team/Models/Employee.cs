﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpekSkates.Admin.Web.Base;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace SpekSkates.Admin.Web.Modules.Team.Models
{
    public class Employee : BaseEntity
    {
        [Required(ErrorMessage = "Le nom est obligatoire")]
        [DisplayName("Nom: ")]
        public string Name { get; set; }

        public string Description { get; set; }

        [DisplayName("Photo: ")]
        public string ImagePath { get; set; }
    }
}
