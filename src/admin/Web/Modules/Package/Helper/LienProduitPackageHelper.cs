﻿using Microsoft.EntityFrameworkCore;
using SpekSkates.Admin.Web.Modules.Package.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.Package.Helper
{
    public static class LienProduitPackageHelper
    {

        public static LienProduitPackage GetLienProduitPackageWithInclude(IQueryable<LienProduitPackage> lien)
        {
            return lien
                .Include(i => i.Product)
                .FirstOrDefault();
        }

    }
}
