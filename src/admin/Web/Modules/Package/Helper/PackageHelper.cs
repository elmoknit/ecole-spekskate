﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.Package.Helper
{
    public static class PackageHelper
    {
        public static Package GetPackageWithInclude(IQueryable<Package> package)
        {
            return package
                .Include(i => i.lstLienProduitPackage)
                .FirstOrDefault();
        }
    }
}
