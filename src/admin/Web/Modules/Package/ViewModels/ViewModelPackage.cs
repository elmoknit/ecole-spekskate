﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Package.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SpekSkates.Admin.Web.Modules.Package.ViewModels
{
    public class ViewModelPackage
    {
        public Package Package { get; set; }

        public List<SelectListProduct> SelectListProduct { set; get; }

        [MinLength(2, ErrorMessage ="Au moins deux produits doivent être ajoutés. ")]
        [Range(1, double.MaxValue, ErrorMessage="Un produit doit être sélectionné")]
        public List<LienProduitPackageShort> NewLiensProduitPackage { get { return _newLiensProduitPackage; } }

        [MinLength(2, ErrorMessage = "Au moins deux produits doivent être ajoutés. ")]
        [Range(1, double.MaxValue, ErrorMessage = "Un produit doit être sélectionné")]
        private List<LienProduitPackageShort> _newLiensProduitPackage = new List<LienProduitPackageShort>();

        public List<LienProduitPackage> LiensInPackage { get; set; }

        public List<Product.Product> ProductsInPackage { get; set; }

        public List<SelectListProduct> HiddenSelectListProductName { set; get; }

        public List<SelectListProduct> HiddenSelectListProductId { set; get; }

        public List<SelectListProduct> HiddenSelectListProductNameIncludingDeactivated { set; get; }

        //public List<int> selectedProductsIndexes { get; set; }

        [DisplayName("Image du package")]
        public IFormFile File { get; set; }

        public string errorMessage { get; set; }

        public bool discountApplied { get; set; }

    }

    public class PackageShort : BaseEntity
        {

        [DisplayName("Nom :")]
        [Required(ErrorMessage ="Le nom du package est obligatoire. ")]
        public string Name { get; set; }

        [DisplayName("Prix:")]
        [Required(ErrorMessage = "Le prix est obligatoire. ")]
        public decimal price { get; set; }

        [Required(ErrorMessage = "La description du paquet est obligatoire. ")]
        [DisplayName("Description du produit : ")]
        public string Description { get; set; }
    }

    public class LienProduitPackageShort
    {
        [Required(ErrorMessage = "Un produit doit être sélectionné")]
        [Range(1, int.MaxValue, ErrorMessage = "Un produit doit être sélectionné")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Le produit est obligatoire. ")]
        [Range(1, int.MaxValue, ErrorMessage = "Un produit doit être sélectionné. ")]
        public int ProductId {get; set;}

        [Required(ErrorMessage = "La quantité doit être un nombre supérieur à 0. ")]
        [Range(1, int.MaxValue, ErrorMessage = "La quantité doit être un nombre supérieur à 0. ")]
        public int Quantity{ get; set; }
    }

    public class SelectListProduct : SelectListItem
    {
        public decimal price { get; set; }
    }
}
