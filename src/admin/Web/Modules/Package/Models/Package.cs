﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Team.Models;
using SpekSkates.Admin.Web.Modules.Product;
using SpekSkates.Admin.Web.Modules.Package.Models;

namespace SpekSkates.Admin.Web.Modules.Package
{
    public class Package : BaseEntity
    {
        [Required(ErrorMessage = "Le nom est obligatoire. ")]
        [DisplayName("Nom :")]
        public string Name { get; set; }

        [Required()]
        [DisplayName("Liste des produits dans le paquet")]
        [MinLength(2, ErrorMessage = "Au moins 2 produit doit être ajouté. ")]
        public List<LienProduitPackage> lstLienProduitPackage { get; set; }

        [Required(ErrorMessage = "Le prix est obligatoire. ")]
        [DisplayName("Prix : ")]
        public decimal Price { get; set; }

        [DisplayName("Rabais sur l'ensemble (en %): ")]
        [Range(0, 100, ErrorMessage = "Le rabais doit se situer entre 0 et 100%")]
        public decimal? DiscountPercentage { get; set; }

        [DisplayName("Image")]
        public string ImagePath { get; set; }

        [DisplayName("Histoire du paquet : ")]
        public string History { get; set; }


        public string Description { get; set; }

        public bool isActive { get; set; }

        public bool isDelete { get; set; }

        [DisplayName("Package en promotion: ")]
        public bool isEnPromotion { get; set; }

        [DisplayName("Package indisponible :")]
        public bool isNonDisponible { get; set; }

        public string url { get; set; }


        [Required(ErrorMessage = "Le code est obligatoire")]
        [DisplayName("Code du package : ")]
        public string codeProduit { get; set; }


        public void UpdatePackagePrice()
        {
            decimal price = 0;
            foreach (LienProduitPackage lien in lstLienProduitPackage)
            {
                price += lien.Product.Price * lien.Quantity;
            }
            decimal discount = 0;
            if (DiscountPercentage != 0 && DiscountPercentage != null)
            {
                discount = (decimal)DiscountPercentage;
            }
            this.Price = price - (discount * price);
        }





    }

}
