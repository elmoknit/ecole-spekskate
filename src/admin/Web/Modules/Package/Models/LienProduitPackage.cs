﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpekSkates.Admin.Web.Modules.Product;
using SpekSkates.Admin.Web.Base;
using System.ComponentModel.DataAnnotations;

namespace SpekSkates.Admin.Web.Modules.Package.Models
{
    public class LienProduitPackage : BaseEntity
    {
        [Required(ErrorMessage ="Le id du package est obligatoire. ")]
        public int PackageId { get; set; }

        [Required(ErrorMessage = "Le produit est obligatoire. ")]
        public int ProductId { get; set; }

        public virtual Product.Product Product { get; set; }

        [Required(ErrorMessage = "La quantité doit être un nombre supérieur à 0. ")]
        [Range(1, int.MaxValue, ErrorMessage = "La quantité doit être un nombre supérieur à 0. ")]
        public int Quantity { get; set; }

    }
}
