﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using SpekSkates.Admin.Web.Modules.Package.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using SpekSkates.Admin.Web.Modules.Package.Models;
using SpekSkates.Admin.Web.Modules.Package.Helper;
using SpekSkates.Admin.Web.AzureBlob;

namespace SpekSkates.Admin.Web.Modules.Package.Controllers
{
    [Authorize]
    public class PackageController : Controller
    {

        private IRepository<Package> _packageRepository;
        private IRepository<Product.Product> _productRepository;
        private IRepository<LienProduitPackage> _lienRepository;
        private readonly IAzureBlobStorage _blobStorage;

        public PackageController(IRepository<Package> packageRepositoryRepository, IRepository<Product.Product> productRepository,
            IRepository<LienProduitPackage> lienProduitRepository, IAzureBlobStorage blobStorage)
        {
            _packageRepository = packageRepositoryRepository;
            _productRepository = productRepository;
            _lienRepository = lienProduitRepository;
            _blobStorage = blobStorage;
        }

        public ActionResult Index()
        {
            return View("~/views/Package/index.cshtml", _packageRepository.GetAll().Where(x=>!x.isDelete).ToList());
        }

        public ActionResult Details(int id)
        {
            return View();
        }

        public IActionResult Create()
        {
            var mv = new ViewModelPackage() { };
            fillDropDownList(mv);
            mv.discountApplied = false;
            return View(mv);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ViewModelPackage mv, IFormCollection formCollection)
        {
            //var test = mv.NewLiensProduitPackage.Count;
            bool quantitiesAndProductIdsAreValid = newLiensHaveValidProductIdsAndQuantities(mv, formCollection);
            var productsQuantity = CountProductsQuantityInPackage(mv.NewLiensProduitPackage);
            GeneratePackageDescription(mv.Package, mv);
            // test, felete me
            mv.HiddenSelectListProductId = new List<SelectListProduct>();
            // additional validation because modelState.IsValid is ignored (validationState = skipped), can't figure out why
            if (!ModelState.IsValid || productsQuantity < 2)
            {
                mv.errorMessage = "Un package doit contenir au moins 2 produits. ";
                fillDropDownList(mv);
                return View(mv);
            }

            if (!quantitiesAndProductIdsAreValid)
            {
                mv.errorMessage = "Une erreur s'est produite, veuillez vérifier les produits sélectionnés et que leur quantité. ";
                fillDropDownList(mv);
                return View(mv);
            }

            Package newPackage = new Package() { lstLienProduitPackage = new List<LienProduitPackage>() };
            if (mv.Package.Name == null)
            {
                mv.Package.Name = "";
                fillDropDownList(mv);
                return View(mv);
            }

            if (mv.Package.codeProduit == null)
            {
                mv.Package.codeProduit = "";
                fillDropDownList(mv);
                return View(mv);
            }

            var trimmedName = mv.Package.Name.Trim();
            var PackageWithIdenticalName = _packageRepository.GetAll().Where(q => q.Name == trimmedName && !q.isDelete).ToList();
            if (PackageWithIdenticalName.Count > 0)
            {
                mv.errorMessage = "Un package avec un nom identique existe déjà. ";
                mv.Package.Name = trimmedName;
                fillDropDownList(mv);
                return View(mv);
            }
            mv.errorMessage = "";

            var listLiens = new List<LienProduitPackage>();

            foreach (var lien in mv.NewLiensProduitPackage)
            {
                Product.Product product = _productRepository.Get(lien.ProductId).FirstOrDefault();
                if (product != null && lien.Quantity > 0)
                {
                    if (listLiens.Any(x => x.ProductId == lien.ProductId))
                    {
                        var indice = listLiens.IndexOf(listLiens.Find(x => x.ProductId == lien.ProductId));
                        listLiens[indice].Quantity += lien.Quantity;
                    }
                    else
                    {
                        LienProduitPackage nouvLien = new LienProduitPackage
                        {
                            Quantity = lien.Quantity,
                            ProductId = lien.ProductId
                        };
                        listLiens.Add(nouvLien);
                    }
                }
                else if (product == null)
                {
                    return NotFound();
                }
            }

            //newPackage.lstLienProduitPackage.AddRange(listLiens);
            if (mv.Package.lstLienProduitPackage == null)
            {
                mv.Package.lstLienProduitPackage = new List<LienProduitPackage> ();
            }

            if (mv.Package.DiscountPercentage != 0 && mv.discountApplied == false)
            {
                mv.Package.DiscountPercentage = 0;
            }
            mv.Package.lstLienProduitPackage.AddRange(listLiens);

            var fileName = "";
            if (mv.File != null && mv.File.Length != 0)
            { fileName = mv.File.GetFilename(); }
            if (UploadFile(mv.File, fileName))
            {
                mv.Package.ImagePath = IFromFileExtensions.ImagePath(fileName);
            }
            mv.Package.isActive = true;
            _packageRepository.Update(mv.Package);
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Edit(int? id)
        {
            var package = PackageHelper.GetPackageWithInclude( _packageRepository.Get(id).Where(q => !q.isDelete));

            if (package == null)
            {
                return NotFound();
            }

            var mv = new ViewModelPackage() { Package = package };

            fillDropDownList(mv);
            fillListLiensInPackage(mv);

            if (package.DiscountPercentage != 0 && package.DiscountPercentage != null)
            {
                mv.discountApplied = true;
            }

            return View(mv);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ViewModelPackage mv, IFormCollection formCollection)
        {

            if (id != mv.Package.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {

                var test = mv.NewLiensProduitPackage;
                var test2 = mv.NewLiensProduitPackage.Count;

                var totalLiensCount = CountProductsQuantityInPackage(mv.NewLiensProduitPackage, mv.LiensInPackage);
                if (totalLiensCount < 2)
                    {
                    mv.errorMessage = "Un package doit contenir au moins 2 produits. ";
                    fillDropDownList(mv);
                    fillHiddenListsInModel(mv);
                    fillProductsInPackageFromProductIds(mv);
                    removeDeletedProductsFromModelView(mv);
                    return View(mv);
                }

                var trimmedName = mv.Package.Name.Trim();
                if (_packageRepository.GetAll().Where(q => q.Name == trimmedName && q.Id != mv.Package.Id && !q.isDelete).ToList().Count > 0)
                {
                    mv.errorMessage = "Un package avec un nom identique existe déjà. ";
                    mv.Package.Name = trimmedName;
                    fillDropDownList(mv);
                    fillProductsInPackageFromProductIds(mv);
                    fillHiddenListsInModel(mv);
                    removeDeletedProductsFromModelView(mv);
                    return View(mv);
                }

                if (mv.Package.codeProduit == null)
                {
                    mv.Package.codeProduit = "";
                    fillDropDownList(mv);
                    return View(mv);
                }

                mv.errorMessage = "";

                if (mv.Package.Name == null || mv.Package.Name == "")
                {
                    fillDropDownList(mv);
                    fillProductsInPackageFromProductIds(mv);
                    fillHiddenListsInModel(mv);
                    removeDeletedProductsFromModelView(mv);
                    return View(mv);
                }
                
                bool result = UpdateExistingLiensProduitPackage(mv.Package, formCollection, mv);

                bool result2 = true;
                if (mv.NewLiensProduitPackage != null && mv.NewLiensProduitPackage.Count > 0)
                {
                    result2 = AddOrMergeNewLiens(id, mv, mv.Package);
                }
           
                if (!result || !result2)
                {
                    fillDropDownList(mv);
                    fillProductsInPackageFromProductIds(mv);
                    fillHiddenListsInModel(mv);
                    removeDeletedProductsFromModelView(mv);
                    return View(mv);
                }

                if (mv.Package.DiscountPercentage != 0 && mv.discountApplied == false)
                {
                    mv.Package.DiscountPercentage = 0;
                }


                var fileName = "";
                if (mv.File != null && mv.File.Length != 0)
                { fileName = mv.File.GetFilename(); }
                if (UploadFile(mv.File, fileName))
                {
                    mv.Package.ImagePath = IFromFileExtensions.ImagePath(fileName);
                }
                GeneratePackageDescription(mv.Package, mv);

                _packageRepository.Update(mv.Package);

                return RedirectToAction(nameof(Index));
            }
            return View(mv);
        }

        private void fillProductsInPackageFromProductIds(ViewModelPackage mv)
        {
            foreach (var lien in mv.LiensInPackage)
            {
                //var product = _productRepository.Get(lien.ProductId).FirstOrDefault();
                var index = mv.LiensInPackage.IndexOf(lien);
                mv.LiensInPackage[index].Product = _productRepository.Get(lien.ProductId).FirstOrDefault();
            }
        }

        private bool UpdateExistingLiensProduitPackage(Package pack, IFormCollection formCollection, ViewModelPackage mv)
        {
            //var j = 0;
            var listUpdatedLiens = new List<LienProduitPackage>();
            var listOldLiens = _lienRepository.GetAll().Where(q => q.PackageId == pack.Id).ToList();

            // _packageRepository.Update(mv.Package);
            if (mv.LiensInPackage != null)
            {
                foreach (LienProduitPackage lien in mv.LiensInPackage)
                {
                    var quantity = lien.Quantity;
                    var lienId = lien.Id;

                    var newLien = _lienRepository.Get(lienId).FirstOrDefault();
                    if (newLien != null)
                    {
                        newLien.Quantity = quantity;

                        listUpdatedLiens.Add(newLien);
                    }
                }
            }

            // update quantities
            foreach (var modifiedLien in listUpdatedLiens)
            {
                for (var i = 0; i < listOldLiens.Count; i++)
                {
                    if (modifiedLien.ProductId == listOldLiens[i].ProductId)
                    {
                        if (modifiedLien.Quantity != listOldLiens[i].Quantity)
                        {
                            listOldLiens[i].Quantity = modifiedLien.Quantity;
                            _lienRepository.Update(listOldLiens[i]);
                        }
                        listOldLiens.RemoveAt(i);
                        i--;
                    }
                }
            }

            //delete deleted liens
            foreach (var lienSupprime in listOldLiens)
            {
                _lienRepository.Delete(lienSupprime.Id);
            }

            foreach (LienProduitPackageShort newLien in mv.NewLiensProduitPackage)
            {
                var quantity = newLien.Quantity;
                var productId = newLien.ProductId;

                if (quantity == 0 && productId != 0 || quantity != 0 && productId == 0)
                {
                    return false;
                }

            }
            return true;
        }

        private bool AddOrMergeNewLiens(int id, ViewModelPackage mv, Package package)
        {
            var listNewLiens = new List<LienProduitPackage>();
            if (package.lstLienProduitPackage == null)
            {
                package.lstLienProduitPackage = new List<LienProduitPackage> ();
            }

            foreach (LienProduitPackageShort newLien in mv.NewLiensProduitPackage)
            {

                if (newLien.Quantity == 0 && newLien.ProductId != 0 || newLien.Quantity > 0 && newLien.ProductId == 0 || newLien.Quantity < 0 )
                {
                    return false;
                }

                // validates if quantity input is an integer
                var index = mv.NewLiensProduitPackage.IndexOf(newLien);
                if (Request != null && Request.Form != null)
                {
                    var rawValue = Request.Form["NewLiensProduitPackage[" + index + "].Quantity"];
                    if (rawValue != "")
                    {
                        bool result = int.TryParse(rawValue, out int parsedValue);
                        if (!result)
                        {
                            return false;
                        }
                    }
                }

                var quantity = newLien.Quantity;
                var productId = newLien.ProductId;
                if (quantity >= 0 && productId > 0)
                {
                    var lienDejaExistant = _lienRepository.GetAll().Where(w => w.ProductId == productId).Where(w => w.PackageId == id).FirstOrDefault();
                    if (lienDejaExistant != null)
                    {
                        lienDejaExistant.Quantity += quantity;
                        _lienRepository.Update(lienDejaExistant);
                    }
                    else
                    {
                        var newLienToSave = new LienProduitPackage() { PackageId = id, ProductId = productId, Quantity = quantity };
                        _lienRepository.Update(newLienToSave);
                        //listNewLiens.Add(newLienToSave);
                    }
                }
                
                //package.lstLienProduitPackage.AddRange(listNewLiens);
            }
            return true;
        }
        
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pack = _packageRepository.Get(id).FirstOrDefault();

            if (pack == null)
            {
                return NotFound();
            }

            return View(pack);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var listeLiens = _lienRepository.GetAll().Where(w => w.PackageId.Equals(id)).ToList();

            foreach (LienProduitPackage lien in listeLiens)
            {
                _lienRepository.Delete(lien.Id);
            }

            _packageRepository.Delete(id);
            return RedirectToAction(nameof(Index));
        }

        private bool ProductCategoryExists(int id)
        {
            return _packageRepository.Get(id).FirstOrDefault() != null;
        }

        private void fillListLiensInPackage(ViewModelPackage mv)
        {
            var lienList = _lienRepository.GetAll().Where(w => w.PackageId == mv.Package.Id).ToList();
            mv.LiensInPackage = new List<LienProduitPackage>();

            //huh
            if (mv.ProductsInPackage == null)
            {
                mv.ProductsInPackage = new List<Product.Product>();
            }

            foreach (var lien in lienList)
            {
                var productInLien = _productRepository.Get(lien.ProductId).FirstOrDefault();
                if (mv.LiensInPackage.Any(x => x.ProductId == lien.ProductId))
                {
                    var index = mv.LiensInPackage.IndexOf(mv.LiensInPackage.Find(x => x.ProductId == lien.ProductId));
                    mv.LiensInPackage[index].Quantity += lien.Quantity;
                    mv.LiensInPackage[index].Product = productInLien;
                    // liste null ProductsInPackage... 
                    mv.ProductsInPackage.Add(lien.Product);
                }
                else
                {                    
                    mv.LiensInPackage.Add(new LienProduitPackage() { Product = productInLien, Quantity = lien.Quantity, PackageId = mv.Package.Id, Id = lien.Id, ProductId = lien.ProductId });
                }
            }

        }

        private void fillDropDownList(ViewModelPackage mv)
        {
            mv.HiddenSelectListProductName = new List<SelectListProduct>();
            mv.SelectListProduct = new List<SelectListProduct>();
            mv.HiddenSelectListProductId = new List<SelectListProduct>();

            var productList = _productRepository.GetAll().Where(t => t.isActive && !t.isDelete).ToList();

            mv.SelectListProduct = new List<SelectListProduct>();
            foreach (var product in productList)
            {
                mv.SelectListProduct.Add(new SelectListProduct { Text = product.Name + " - " + product.Price + "$", Value = product.Id.ToString()});
            }

            fillHiddenListsInModel(mv);
        }

        private void fillHiddenListsInModel(ViewModelPackage mv)
        {
            mv.HiddenSelectListProductName = new List<SelectListProduct>();
            mv.HiddenSelectListProductId = new List<SelectListProduct>();
            mv.HiddenSelectListProductNameIncludingDeactivated = new List<SelectListProduct>();

            var productList = _productRepository.GetAll().Where(t => !t.isDelete).ToList();

            foreach (var product in productList)
            {
                if (product.isActive)
                {
                    mv.HiddenSelectListProductId.Add(new SelectListProduct { Text = product.Price.ToString(), Value = product.Id.ToString() });
                    mv.HiddenSelectListProductName.Add(new SelectListProduct { Text = product.Price.ToString(), Value = product.Name });
                }
                mv.HiddenSelectListProductNameIncludingDeactivated.Add(new SelectListProduct { Text = product.Price.ToString(), Value = product.Name });
            }
        }

        private void removeDeletedProductsFromModelView(ViewModelPackage mv)
        {
            for (int i = 0; i < mv.LiensInPackage.Count; i++)
            {
                if (mv.LiensInPackage[i].Id == 0 && mv.LiensInPackage[i].Quantity == 0)
                {
                    mv.LiensInPackage.RemoveAt(i);
                    i--;
                }
            }
        }

        private bool newLiensHaveValidProductIdsAndQuantities(ViewModelPackage mv, IFormCollection formCollection)
        {
            foreach (LienProduitPackageShort lien in mv.NewLiensProduitPackage)
            {
                int newProductQty = lien.Quantity;
                int newProductId = lien.ProductId;

                if (newProductId == 0 && newProductQty != 0 || newProductId != 0 && newProductQty <= 0)
                {
                    return false;
                }

                if (lien.Quantity == 0)
                {
                    var indexOfLien = mv.NewLiensProduitPackage.IndexOf(lien);
                    var rawValue = Request.Form["NewLiensProduitPackage[" + indexOfLien + "].Quantity"][0];
                    if (rawValue.Count() > 0)
                    {
                        var parseResult = int.TryParse(rawValue, out int throwAwayQty);
                        if (!parseResult)
                        {
                            return false;
                        }

                    }
                }
            }

            return true;
        }

        private int CountProductsQuantityInPackage(List<LienProduitPackageShort> list1, List<LienProduitPackage> list2 = null)
        {
            var productQuantity = 0;

            foreach (var lien in list1)
            {
                productQuantity += lien.Quantity;
            }

            if (productQuantity < 2)
            {
                if (list2 != null)
                {
                    foreach (var lien in list2)
                    {
                        productQuantity += lien.Quantity;
                    }
                }
            }

            return productQuantity;
        }

        private bool UploadFile(IFormFile file, string blobName)
        {
            if (file == null || file.Length == 0)
            { return false; }

            //source : https://github.com/TahirNaushad/Fiver.Azure.Blob/tree/master/Fiver.Azure.Blob.Client
            var fileStream = file.GetFileStream();

            _blobStorage.UploadAsync(blobName, fileStream);
            return true;
        }

        public IActionResult DeleteImage(string blobName, int id)
        {
            if (!string.IsNullOrEmpty(blobName))
            {
                var imageName = blobName.Split('/');
                _blobStorage.DeleteAsync(imageName[imageName.Count() - 1]);
                var package = _packageRepository.Get(id).FirstOrDefault();
                if (package == null)
                {
                    return NotFound();
                }
                package.ImagePath = "";
                _packageRepository.Save(package);
                return RedirectToAction(nameof(Edit), new { id });
            }
            else
            {
                return NotFound();
            }


        }

        private void GeneratePackageDescription(Package package, ViewModelPackage mv)
        {
            var listNames = new List<string>();
            var listIds = new List<int>();
            var listQuantities = new List<int>();


            if (package.lstLienProduitPackage != null)
            {
                foreach (var lien in package.lstLienProduitPackage)
                {
                    var product = _productRepository.Get(lien.ProductId).FirstOrDefault();
                    if (product != null)
                    {
                        if (!listIds.Contains(product.Id))
                        {
                            listIds.Add(product.Id);
                            listNames.Add(product.Name);
                            listQuantities.Add(lien.Quantity);
                        }
                        else
                        {
                            listQuantities[listIds.IndexOf(product.Id)] += lien.Quantity;
                        }
                    }

                }
            }

            if (mv.NewLiensProduitPackage != null)
            {
                foreach (var lien in mv.NewLiensProduitPackage)
                {
                    var product = _productRepository.Get(lien.ProductId).FirstOrDefault();
                    if (product != null)
                    {
                        if (!listIds.Contains(product.Id))
                        {
                            listIds.Add(product.Id);
                            listNames.Add(product.Name);
                            listQuantities.Add(lien.Quantity);
                        }
                        else
                        {
                            listQuantities[listIds.IndexOf(product.Id)] += lien.Quantity;
                        }
                    }

                }
            }

            if (mv.LiensInPackage != null)
            {
                foreach (var lien in mv.LiensInPackage)
                {
                    var product = _productRepository.Get(lien.ProductId).FirstOrDefault();
                    if (product != null)
                    {
                        if (!listIds.Contains(product.Id))
                        {
                            listIds.Add(product.Id);
                            listNames.Add(product.Name);
                            listQuantities.Add(lien.Quantity);
                        }
                        else
                        {
                            listQuantities[listIds.IndexOf(product.Id)] += lien.Quantity;
                        }
                    }

                }
            }

            package.Description = "Produits inclus: ";
            foreach (string name in listNames)
            {
                package.Description += name + " (x" + listQuantities[listNames.IndexOf(name)] + "), ";
            }

            package.Description = package.Description.Remove(package.Description.Length - 2);
        }

        public IActionResult Active(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var package = _packageRepository.Get(id).FirstOrDefault();
            if (package == null)
            {
                return NotFound();
            }

            package.isActive = true;
            _packageRepository.Update(package);

            return RedirectToAction(nameof(Index));
        }


        public IActionResult UnActive(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var package = _packageRepository.Get(id).FirstOrDefault();
            if (package == null)
            {
                return NotFound();
            }

            package.isActive = false;
            _packageRepository.Update(package);

            return RedirectToAction(nameof(Index));
        }




    }
}