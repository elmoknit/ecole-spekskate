﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.Package.Controllers
{
    [Route("api/[controller]")]
    public class ApiDetailPackageController : Controller
    {
        IRepository<Package> _packageRepository;
        IRepository<Product.Product> _productRepository;

        public ApiDetailPackageController(IRepository<Package> packageRepository, IRepository<Product.Product> productRepository)
        {
            _packageRepository = packageRepository;
            _productRepository = productRepository;
        }

        [HttpGet("{name}")]
        public IActionResult GetDetailPackage([FromRoute] string name)
        {

            var returnResult = Json("");
            if (name == null || name == string.Empty)
            {
                returnResult = Json("Bad Request");
                returnResult.StatusCode = 400;
                return BadRequest(returnResult);
            }
            var result = GetPackagetWithInclude(_packageRepository.GetAll()).Where(p => p.Name == name && p.isActive && !p.isDelete).FirstOrDefault();


            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }

            foreach (var lien in result.lstLienProduitPackage)
            {
                lien.Product = _productRepository.Get(lien.ProductId).FirstOrDefault();
            }

            // result.url = Uri.EscapeUriString(PathSnip.path + result.Name);
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }

        private static IQueryable<Package> GetPackagetWithInclude(IQueryable<Package> package)
        {
            return package
                .Include(i => i.lstLienProduitPackage);

        }
    }
}
