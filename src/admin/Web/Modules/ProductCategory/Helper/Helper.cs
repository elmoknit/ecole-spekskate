﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.ProductCategory
{ 
    public static class Helper
    {


        public static ProductCategory GetHomePageWithInclude(IQueryable<ProductCategory> productCategories)
        {
            return productCategories
                .Include(i => i.RelatedCategories)
                .ThenInclude(x=>x.ProductCategory2)

                .Include(i => i.ProductCategoriesProductSizes)
                .ThenInclude(y => y.ProductSize)

                .Include(it=>it.ProductCategoriesProductShapes)
                .ThenInclude(z=>z.ProductShape)

                .FirstOrDefault();
                
        }


    }
}
