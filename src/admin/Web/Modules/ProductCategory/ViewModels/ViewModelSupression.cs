﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SpekSkates.Admin.Web.Modules.ProductCategory.ViewModels
{
    public class ViewModelSupression
    {
        public ProductCategory ProductCategory { get; set; }
        public bool categoryHasProduct { get; set; }
        public string NameConfirm { get; set; }
        public string ErrorMessage { get; set; }
    }
}
