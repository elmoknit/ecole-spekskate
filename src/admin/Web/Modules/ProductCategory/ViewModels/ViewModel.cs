﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SpekSkates.Admin.Web.Modules.ProductCategory.ViewModels
{
    public class ViewModel
    {
        public ProductCategory ProductCategory { get; set; }

        public List<SelectListItem> CategoryTypes { get; set; }

        public int[] ProductSizeIds { get; set; }
        public List<SelectListItem> ProductSizeSelectList { set; get; }

        public int[] ProductShapeIds { get; set; }
        public List<SelectListItem> ProductShapeSelectList { set; get; }

        public int[] ProductCategoriesIds { get; set; }
        public List<SelectListItem> ProductCategoriesSelectList { get; set; }


        public string ErrorMessage { get; set; }

    }
}
