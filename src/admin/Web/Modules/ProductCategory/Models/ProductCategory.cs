﻿   using System;
using System.Collections.Generic;
   using System.ComponentModel;
   using System.ComponentModel.DataAnnotations;
   using System.Linq;
using System.Threading.Tasks;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Team.Models;

namespace SpekSkates.Admin.Web.Modules.ProductCategory
{
    public enum CategoryType { Board,  Wheel, Truck, GripTape, Beiring, Accessory }

    public class ProductCategory : BaseEntity
    {
        [Required(ErrorMessage = "Le nom de la catégorie est obligatoire")]
        [DisplayName("Nom de la catégorie :")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Le type de catégorie est obligatoire")]
        [DisplayName("Type de la catégorie :")]
        public CategoryType CategoryType { get; set; }


        public bool isDelete { get; set; }

        public virtual ICollection<ProductCategoryProductSize> ProductCategoriesProductSizes { get; set; }

        public virtual ICollection<ProductCategoryProductShape> ProductCategoriesProductShapes { get; set; }

        public virtual ICollection<RelationCatCat> RelatedCategories { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    public class ProductCategoryProductSize
    {
        public int ProductCategoryId { get; set; }
        public ProductCategory ProductCategory { get; set; }

        public int ProductSizeId { get; set; }
        public ProductSize.ProductSize ProductSize { get; set; }


    }

    public class ProductCategoryProductShape
    {
        public int ProductCategoryId { get; set; }
        public ProductCategory ProductCategory { get; set; }

        public int ProductShapeId { get; set; }
        public ProductShape.ProductShape ProductShape { get; set; }
    }

    public class RelationCatCat
    {
        public int ProductCategory1Id { get; set; }

        public int ProductCategory2Id { get; set; }

        public ProductCategory ProductCategory1 { get; set; }

        public ProductCategory ProductCategory2 { get; set; }


    }
}
