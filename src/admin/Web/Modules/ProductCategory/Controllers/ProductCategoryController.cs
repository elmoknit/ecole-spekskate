﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using SpekSkates.Admin.Web.Modules.ProductCategory.ViewModels;

namespace SpekSkates.Admin.Web.Modules.ProductCategory.Controllers
{
    [Authorize]
    public class ProductCategoryController : Controller
    {

        private IRepository<ProductCategory> _itemRepository;
        private IRepository<Product.Product> _productRepository;
        private IRepository<ProductSize.ProductSize> _productSizeRepository;
        private IRepository<ProductShape.ProductShape> _productShapeRepository;

        public ProductCategoryController(IRepository<ProductCategory> productCategoryRepository,
            IRepository<ProductSize.ProductSize> productSizeRepository, IRepository<ProductShape.ProductShape> productShapeRepository, IRepository<Product.Product> productRepository)
        {
            _itemRepository = productCategoryRepository;
            _productSizeRepository = productSizeRepository;
            _productShapeRepository = productShapeRepository;
            _productRepository = productRepository;
        }

        // GET: ProductCategory
        public ActionResult Index()
        {
            return View("~/views/ProductCategory/index.cshtml", _itemRepository.GetAll().Where(s => !s.isDelete).ToList());
        }

        // GET: ProductCategory/Create
        public ActionResult Create()
        {
            var mv = new ViewModel();

            mv.ProductCategory = new ProductCategory
            {
                Name = "",
            };
            return View(mv);
        }

        // POST: ProductCategory/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ViewModel mv)
        {

            if (ModelState.IsValid)
            {
                if (_itemRepository.GetAll().Where(s => s.Name == mv.ProductCategory.Name && !s.isDelete).Any())
                {
                    mv.ErrorMessage = "Ce nom de catégorie existe déjà";
                    return View(mv);
                }
                _itemRepository.Update(mv.ProductCategory);
                _itemRepository.Save(mv.ProductCategory);

                return RedirectToAction("Edit", new { id = mv.ProductCategory.Id });
            }
            return View(mv);
        }


        // GET: ProductCategory/Edit/5
        public IActionResult Edit(int? id)
        {
            var mv = new ViewModel();
            if (id == null)
            {
                return NotFound();
            }

            mv.ProductCategory = Helper.GetHomePageWithInclude(_itemRepository.Get(id));

            if (mv.ProductCategory == null)
            {
                return NotFound();
            }
            mv.CategoryTypes = new List<SelectListItem>{
                new SelectListItem
                {
                    Text = "Planche",
                    Value = CategoryType.Board.ToString()
                },
                new SelectListItem
                {
                    Text = "Roue",
                    Value = CategoryType.Wheel.ToString()
                },
                new SelectListItem
                {
                    Text = "Truck",
                    Value = CategoryType.Truck.ToString()
                },
                new SelectListItem
                {
                    Text = "Grip Tape",
                    Value = CategoryType.GripTape.ToString()
                },
                new SelectListItem
                {
                    Text = "Beiring",
                    Value = CategoryType.Beiring.ToString()
                },
                new SelectListItem
                {
                    Text = "Accessoire",
                    Value = CategoryType.Accessory.ToString()
                }
            };

            mv.ProductSizeSelectList = new List<SelectListItem>();
            mv.ProductShapeSelectList = new List<SelectListItem>();
            mv.ProductCategoriesSelectList = new List<SelectListItem>();
            PrepMultiSelect(mv);

            return View(mv);

        }

        // POST: ProductCategory/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ViewModel mv)
        {
            if (id != mv.ProductCategory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (_itemRepository.GetAll().Where(s => s.Name == mv.ProductCategory.Name && s.Id != mv.ProductCategory.Id && !s.isDelete).Any())
                {
                    mv.ErrorMessage = "Ce nom de catégorie existe déjà";
                    return View(mv);
                }

                try
                {
                    _itemRepository.Update(mv.ProductCategory);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductCategoryExists(mv.ProductCategory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                MultiSelectProcess(mv);
                return RedirectToAction(nameof(Index));
            }
            return View(mv);
        }


        // GET: ProductCategory/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productCategory = _itemRepository.Get(id).FirstOrDefault();

            if (productCategory == null)
            {
                return NotFound();
            }
            var mv = new ViewModelSupression { ProductCategory = productCategory };
            if (_productRepository.GetAll().Where(s => s.ProductCategoryId == mv.ProductCategory.Id).Any())
            {
                mv.categoryHasProduct = true;
            }
            else
            {
                mv.categoryHasProduct = false;
            }
            return View(mv);
        }

        // POST: ProductCategory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(ViewModelSupression mv)
        {
            if (mv.ProductCategory.Name != mv.NameConfirm && mv.categoryHasProduct)
            {
                mv.ErrorMessage = "Le nom de confirmation ne correspond pas. La supression a été annulé.";
                return View("~/views/ProductCategory/delete.cshtml", mv);
            }
            mv.ProductCategory.isDelete = true;
            _itemRepository.Update(mv.ProductCategory);
            foreach (var product in _productRepository.GetAll().Where(s => s.ProductCategoryId == mv.ProductCategory.Id).ToList())
            {
                product.isDelete = true;
                _productRepository.Update(product);
            }
            return RedirectToAction(nameof(Index));
        }



        private bool ProductCategoryExists(int id)
        {
            return _itemRepository.Get(id) != null;


        }

        //TODO rendre généric
        private void PrepMultiSelect(ViewModel mv)
        {
            var productSizeList = _productSizeRepository.GetAll().Where(x => !x.isDelete).ToList();
            mv.ProductSizeSelectList = new List<SelectListItem>();
            foreach (var productSize in productSizeList)
            {
                mv.ProductSizeSelectList.Add(new SelectListItem
                {
                    Text = productSize.Name,
                    Value = productSize.Id.ToString()
                });
            }

            mv.ProductSizeIds = mv.ProductCategory.ProductCategoriesProductSizes.Select(x => x.ProductSizeId).ToArray();

            var productShapeList = _productShapeRepository.GetAll().Where(x=>!x.isDelete).ToList();
            mv.ProductShapeSelectList = new List<SelectListItem>();
            foreach (var productShape in productShapeList)
            {
                mv.ProductShapeSelectList.Add(new SelectListItem
                {
                    Text = productShape.Name,
                    Value = productShape.Id.ToString()
                });
            }

            mv.ProductShapeIds = mv.ProductCategory.ProductCategoriesProductShapes.Select(x => x.ProductShapeId).ToArray();

            var productCategoriesList = _itemRepository.GetAll().Where(x => x.Id != mv.ProductCategory.Id && !x.isDelete).ToList();
            mv.ProductCategoriesSelectList = new List<SelectListItem>();
            foreach (var productCategory in productCategoriesList)
            {
                mv.ProductCategoriesSelectList.Add(new SelectListItem
                {
                    Text = productCategory.Name,
                    Value = productCategory.Id.ToString()
                });
            }

            mv.ProductCategoriesIds = mv.ProductCategory.RelatedCategories.Where(x=>!x.ProductCategory2.isDelete).Select(x => x.ProductCategory2Id).ToArray();

        }

        //TODO rendre généric
        private void MultiSelectProcess(ViewModel mv)
        {

            mv.ProductCategory = Helper.GetHomePageWithInclude(_itemRepository.Get(mv.ProductCategory.Id));

            var productSizes = _productSizeRepository.GetAll().Where(s => mv.ProductSizeIds.Contains(s.Id)).ToList();
            var productCategortproductSizeSupress =
                mv.ProductCategory.ProductCategoriesProductSizes.Where(
                    s =>
                        !productSizes.Select(c => c.Id).Contains(s.ProductSizeId) &&
                        s.ProductCategoryId == mv.ProductCategory.Id).ToList();

            foreach (var productSize in productSizes)
            {
                if (
                    !mv.ProductCategory.ProductCategoriesProductSizes.Select(s => s.ProductSizeId)
                        .Contains(productSize.Id))
                {
                    mv.ProductCategory.ProductCategoriesProductSizes.Add(new ProductCategoryProductSize()
                    {
                        ProductCategory = mv.ProductCategory,
                        ProductCategoryId = mv.ProductCategory.Id,
                        ProductSize = productSize,
                        ProductSizeId = productSize.Id
                    });
                }
            }

            foreach (var toSupress in productCategortproductSizeSupress)
            {
                mv.ProductCategory.ProductCategoriesProductSizes.Remove(toSupress);
            }

            var productShapes = _productShapeRepository.GetAll().Where(s => mv.ProductShapeIds.Contains(s.Id)).ToList();
            var productCategoryproductShapeSupress =
                mv.ProductCategory.ProductCategoriesProductShapes.Where(
                    s =>
                        !productShapes.Select(c => c.Id).Contains(s.ProductShapeId) &&
                        s.ProductCategoryId == mv.ProductCategory.Id).ToList();

            foreach (var productShape in productShapes)
            {
                if (
                    !mv.ProductCategory.ProductCategoriesProductShapes.Select(s => s.ProductShapeId)
                        .Contains(productShape.Id))
                {
                    mv.ProductCategory.ProductCategoriesProductShapes.Add(new ProductCategoryProductShape
                    {
                        ProductCategory = mv.ProductCategory,
                        ProductCategoryId = mv.ProductCategory.Id,
                        ProductShape = productShape,
                        ProductShapeId = productShape.Id
                    });
                }
            }

            foreach (var toSupress in productCategoryproductShapeSupress)
            {
                mv.ProductCategory.ProductCategoriesProductShapes.Remove(toSupress);
            }

            var productCategory = _itemRepository.GetAll().Where(s => mv.ProductCategoriesIds.Contains(s.Id)).ToList();
            var productCatCatSupress =
                mv.ProductCategory.RelatedCategories.Where(
                    s =>
                        !productCategory.Select(c => c.Id).Contains(s.ProductCategory2Id) &&
                        s.ProductCategory1Id == mv.ProductCategory.Id).ToList();

            foreach (var ProductCat in productCategory)
            {
                if (!mv.ProductCategory.RelatedCategories.Select(s => s.ProductCategory2Id)
                    .Contains(ProductCat.Id))
                {
                    mv.ProductCategory.RelatedCategories.Add(new RelationCatCat
                    {
                        ProductCategory1 = mv.ProductCategory,
                        ProductCategory1Id = mv.ProductCategory.Id,
                        ProductCategory2 = ProductCat,
                        ProductCategory2Id = ProductCat.Id
                    });
                    if (ProductCat.RelatedCategories == null)
                    {
                        ProductCat.RelatedCategories = new List<RelationCatCat>();
                    }
                    ProductCat.RelatedCategories.Add(new RelationCatCat
                    {
                        ProductCategory1 = ProductCat,
                        ProductCategory1Id = ProductCat.Id,
                        ProductCategory2 = mv.ProductCategory,
                        ProductCategory2Id = mv.ProductCategory.Id

                    });
                    _itemRepository.Update(ProductCat);
                }
            }
            int i = 0;

            foreach (var toSupress in productCatCatSupress)
            {
                mv.ProductCategory.RelatedCategories.Remove(toSupress);
                var rcc = Helper.GetHomePageWithInclude(_itemRepository.Get(toSupress.ProductCategory2Id));

                var productCatCatSupress2 =
                rcc.RelatedCategories.Where(
                    s => s.ProductCategory1Id == toSupress.ProductCategory2Id && s.ProductCategory2Id == toSupress.ProductCategory1Id).FirstOrDefault();

                rcc.RelatedCategories.Remove(productCatCatSupress2);
            }


            _itemRepository.Update(mv.ProductCategory);

        }


        private RelationCatCat switchARoo(RelationCatCat rcc)
        {
            ProductCategory c = rcc.ProductCategory1;
            rcc.ProductCategory1 = rcc.ProductCategory2;
            rcc.ProductCategory2 = c;
            return rcc;
        }
    }
}