﻿using SpekSkates.Admin.Web.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.Page
{

    public class Page : BaseEntity
    {

        public string Name { get; set; }
    }
}
