﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SpekSkates.Admin.Web;
using SpekSkates.Admin.Web.Models;
using SpekSkates.Admin.Web.Base;
using Microsoft.AspNetCore.Authorization;

namespace SpekSkates.Admin.Web.Modules.Page
{
    [Authorize]
    public class PageController : Controller
    {
        private IRepository<Page> _itemRepository;


        public PageController(IRepository<Page> pageRepository)
        {
            _itemRepository = pageRepository;
        }


        // GET: Pages
        public IActionResult Index()
        {  
            return View("~/views/page/index.cshtml", _itemRepository.GetAll().ToList());
        }

        // GET: Pages/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var page = _itemRepository.Get(id).FirstOrDefault();
            if (page == null)
            {
                return NotFound();
            }

            return View(page);
        }

        // GET: Pages/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Pages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,Name")] Page page)
        {
            if (ModelState.IsValid)
            {
                _itemRepository.Update(page);
                _itemRepository.Save(page);
                return RedirectToAction(nameof(Index));
            }
            return View(page);
        }

        // GET: Pages/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var page = _itemRepository.Get(id).FirstOrDefault();

            if (page == null)
            {
                return NotFound();
            }
            return View(page);
        }

        // POST: Pages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Name")] Page page)
        {
            if (id != page.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _itemRepository.Update(page);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PageExists(page.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(page);
        }

        // GET: Pages/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var page = _itemRepository.Get(id).FirstOrDefault();

            if (page == null)
            {
                return NotFound();
            }

            return View(page);
        }

        // POST: Pages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            _itemRepository.Delete(id);
            return RedirectToAction(nameof(Index));
        }

        private bool PageExists(int id)
        {
            return _itemRepository.Get(id) != null;
        }
    }
}
