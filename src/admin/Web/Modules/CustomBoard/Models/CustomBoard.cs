﻿using SpekSkates.Admin.Web.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.CustomBoard
{

    public class CustomBoard : BaseEntity
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Desc { get; set; }
        public int BoardId { get; set; }
        public int GripTapeId { get; set; }
        public int TruckId { get; set; }
        public int Truck2Id { get; set; }
        public int WheelId { get; set; }
        public int Wheel2Id { get; set; }
        public int Wheel3Id { get; set; }
        public int Wheel4Id { get; set; }
        public int BeiringId { get; set; }
    }
}