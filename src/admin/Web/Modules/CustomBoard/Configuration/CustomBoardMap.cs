﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SpekSkates.Admin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.CustomBoard
{
    public class CustomBoardClientMap
    {

        public CustomBoardClientMap(EntityTypeBuilder<CustomBoard> entityBuilder)
        {
            entityBuilder.HasKey(t => t.Id);
            entityBuilder.Property(e => e.Desc).IsRequired();
            entityBuilder.Property(e => e.Price).IsRequired();
        }
    }
}
