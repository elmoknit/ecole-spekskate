﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Modules.Product;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Product.Models;
using SpekSkates.Admin.Web.Modules.ProductSize;
using SpekSkates.Admin.Web.Modules.ProductCategory;
using SpekSkates.Admin.Web.Modules.ProductShape;
using SpekSkates.Admin.Web.Modules.BoardStyle;
using SpekSkates.Admin.Web.Modules.Board.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace temp.Controllers
{
    [Route("api/[controller]")]
    public class ApiProductRechercheAvancee : Controller
    {

        IRepository<Product> _productRepository;
        private IRepository<ProductCategory> _productCategoryRepository;
        private IRepository<ProductSize> _productSizeRepository;
        private IRepository<ProductShape> _productShapeRepository;
        private IRepository<BoardStyle> _itemRepository;
        private IRepository<Board> _BoardRepo;

        public ApiProductRechercheAvancee(IRepository<Board> BoardRepo, IRepository<BoardStyle> itemRepository, IRepository<ProductShape> productShapeRepository, IRepository<Product> productRepository, IRepository<ProductCategory> productCategoryRepository, IRepository<ProductSize> productSizeRepository)
        {
            _productRepository = productRepository;
            _productCategoryRepository = productCategoryRepository;
            _productSizeRepository = productSizeRepository;
            _productShapeRepository = productShapeRepository;
            _itemRepository = itemRepository;
            _BoardRepo = BoardRepo;
        }
        public IActionResult RechercheAvancee()
        {
            List<Recherche> result = new List<Recherche>();

            result.Add(new Recherche { nom = "Kicktail", critere = new List<string> { "Kicktail" } });
            result.Add(new Recherche { nom = "Size", critere = _productRepository.GetAll().Where(x=>x.isActive && !x.isDelete).Select(c => c.ProductSize.ToString()).ToList().Distinct().ToList() });
            result.Add(new Recherche { nom = "Categorie", critere = _productRepository.GetAll().Where(x => x.isActive && !x.isDelete).Select(c => c.ProductCategory.ToString()).ToList().Distinct().ToList() });
            result.Add(new Recherche { nom = "Promotion", critere = new List<string> { "Promotion" } });
            result.Add(new Recherche { nom = "Shape", critere = _productShapeRepository.GetAll().Where(x =>!x.isDelete).Select(c => c.Name.ToString()).ToList().Distinct().ToList() });
            result.Add(new Recherche { nom = "Style", critere = _itemRepository.GetAll().Where(x =>!x.isDelete).Select(c => c.Name.ToString()).ToList().Distinct().ToList() });
            //result.Add(new Recherche { nom = "Type", critere = _BoardRepo.GetAll().Select(c => c..ToString()).ToList().Distinct().ToList() });


            var returnResult = Json("");
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }

        [Route("[action]")]
        [HttpGet("{recherche}")]
        public IActionResult RechercheAvancee(string recherche)
        {

            var returnResult = Json("");
            if (recherche == null || recherche == string.Empty)
            {
                returnResult = Json("Bad Request");
                returnResult.StatusCode = 400;
                return BadRequest(returnResult);
            }
            //teset

            IQueryable<Product> result = GetProductWithInclude(_productRepository.GetAll());
            var test = result.ToList();
            var data = (JObject)JsonConvert.DeserializeObject(recherche);
            if(recherche.Contains("texte"))
            {
                string s = data["texte"].Value<string>();
                if (s != string.Empty)
                {
                    result = result.Where(r => r.Name.Contains(s) || r.Description.Contains(s));
                }
            }
            if (recherche.Contains("Kicktail"))
            {
                result = result.Where(r => r.isKicktail);
            }

            if(recherche.Contains("Size"))
            {
                var size = data["Size"].GetType();
                if (data["Size"].GetType() == typeof(JValue))
                {
                    string listeSize = data["Size"].Value<string>();
                    result = result.Where(r => listeSize.Contains(r.ProductSize.Name));

                }
                else
                {
                    //var l = data["Size[]"].Values<IEnumerable<string>>();
                    var f = data["Size"].Values<string>();
                    List<string> listeSize = data["Size"].Values<string>().ToList();
                    result = result.Where(r => listeSize.Contains(r.ProductSize.Name));

                }
            }

            if(recherche.Contains("Prix"))
            {
               // string prix = data["Prix[]"].Value<string>();
                List<string> listePrix = new List<string>();
                for (int i = 0; i < data["Prix"].Count(); i++)
                {
                    listePrix.Add(data["Prix"][i].Value<string>().Split("-")[0]);
                }
                List<int> listePrixVoulu = new List<int>();
                foreach (string elem in listePrix){
                    int j =(int) double.Parse(elem.Replace("$",""));

                    for (int i = 0; i < 10; i++)
                    {
                        listePrixVoulu.Add(j+i);
                    }
                }

                result = result.Where(r => listePrixVoulu.Contains(Convert.ToInt32( r.Price)));
            }

            if(recherche.Contains("prixMin"))
            {
                string s = data["prixMin"].Value<string>();
                if (s != string.Empty)
                {
                    result = result.Where(r => r.Price > decimal.Parse(s));
                }
            }

            if (recherche.Contains("prixMax"))
            {
                string s = data["prixMax"].Value<string>();
                if (s != string.Empty)
                {
                    result = result.Where(r => r.Price < decimal.Parse(s));
                }
            }

            //var test3 = result.ToList();
            if(recherche.Contains("Categorie"))
            {
                if (data["Categorie"].GetType() == typeof(JValue))
                {
                    string listeCat = data["Categorie"].Value<string>();
                    result = result.Where(r => listeCat.Contains(r.ProductCategory.Name));

                }
                else
                {
                    List<string> listeCat = data["Categorie"].Values<string>().ToList();
                    result = result.Where(r => listeCat.Contains(r.ProductCategory.Name));

                }
            }
            if (recherche.Contains("Promotion"))
            {
                result = result.Where(r => r.isEnPromotion);
            }
            if(recherche.Contains("Shape"))
            {
                if (data["Shape"].GetType() == typeof(JValue))
                {
                    string listeShape = data["Shape"].Value<string>();
                    result = result.Where(r => listeShape.Contains(r.ProductShape.Name));

                }
                else
                {
                    List<string> listeShape = data["Shape"].Values<string>().ToList();
                    result = result.Where(r => listeShape.Contains(r.ProductShape.Name));

                }
            }
            if (recherche.Contains("Style"))
            {
                if (data["Style"].GetType() == typeof(JValue))
                {
                    string listeStyle = data["Style"].Value<string>();
                    result = result.Where(r => listeStyle.Contains(r.BoardStyle.Name));

                }
                else
                {
                    List<string> listeStyle = data["Style"].Values<string>().ToList();
                    result = result.Where(r => listeStyle.Contains(r.BoardStyle.Name));

                }
            }

            result = result.Where(r => r.isActive && !r.isDelete);

            if (result == null || result.Count()==0)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }

            var test2 = result.ToList();
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }

        private static IQueryable<Product> GetProductWithInclude(IQueryable<Product> product)
        {
            return product
                .Include(i => i.BoardStyle)
                .Include(j => j.ProductCategory)
                .Include(k => k.ProductShape)
                .Include(l => l.ProductSize);

        }

    }
}
