﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Page;
using SpekSkates.Admin.Web.Modules.Product;
using SpekSkates.Admin.Web.Modules.Package;

namespace temp.Controllers
{
    [Route("api/[controller]")]
    public class ApiProductController : Controller
    {
        IRepository<Product> _productRepository;
        IRepository<Package> _packageRepository;

        public ApiProductController(IRepository<Product> productRepository, IRepository<Package> packageRepository)
        {
            _productRepository = productRepository;
            _packageRepository = packageRepository;
        }
        // GET api/About
        [HttpGet("{recherche}/{page}")]
        public IActionResult GetPageProduit([FromRoute]string recherche, [FromRoute] string page)
        {
            var returnResult = Json("");
            if (recherche == null || recherche == string.Empty || page == null || page == string.Empty)
            {
                returnResult = Json("Bad Request");
                returnResult.StatusCode = 400;
                return BadRequest(returnResult);
            }

            recherche = recherche != "undefined" ? recherche : string.Empty;
            int i;
            if(int.TryParse(page,out i ))
            {
                page = i.ToString();
            }
            page = page != "undefined" ? page : "1";


            return getProduits(recherche,int.Parse(page));
        }

        public IActionResult getProduits(string recherche,int page)
        {
            //IQueryable<BaseEntity> result = null;
            //IQueryable<BaseEntity> result2 = null;
            //if (recherche != string.Empty)
            //{
            //    result = _productRepository.GetAll().Where(x => x.Name.Contains(recherche) || x.Description.Contains(recherche));
            //    result2 = _packageRepository.GetAll().Where(x => x.Name.Contains(recherche) || x.Description.Contains(recherche));
            //}
            //else
            //{
            //    result = _productRepository.GetAll();
            //    result2 = _packageRepository.GetAll();
            //}

            //var resultFinal = (result.ToList().Concat(result2.ToList())).Skip((page - 1) * 20).Take(20);

            //if (result2 != null)
            //{
            //    result.Concat(result2);
            //}
            //var test = result.ToList();
            //var returnResult = Json("");
            //if (result == null || result.ToList().Count() ==0)
            //{
            //    returnResult = Json("Not found");
            //    returnResult.StatusCode = 404;
            //    return NotFound(returnResult);
            //}
            //returnResult = Json(resultFinal);
            //returnResult.StatusCode = 200;
            //return Ok(returnResult);

            IQueryable<Product> result = null;
            IQueryable<Package> result2 = null;
            //NE PAS EFFACER 
            var result3 = GetProductWithInclude(_productRepository.GetAll()).ToList();
            result2 = _packageRepository.GetAll().Where((x => (x.Name.Contains(recherche) || x.Description.Contains(recherche)) && x.isActive == true && !x.isDelete));

            var listPackages = GetPackagetWithInclude(result2).ToList();

            if (recherche != string.Empty)
            {
                result = _productRepository.GetAll().Where((x => (x.Name.Contains(recherche) || x.Description.Contains(recherche) || x.ProductCategory.Name.Contains(recherche) || x.ProductShape.Name.Contains(recherche) || x.ProductSize.Name.Contains(recherche)) && x.isActive && !x.isDelete));
            }
            else
            {
                result = _productRepository.GetAll().Where(x => x.isActive && !x.isDelete);
            }
            

            var listProduits = GetProductWithInclude(result).ToList();
            
            List<BaseEntity> combinedResults = new List<BaseEntity>();
            


            combinedResults.AddRange(listProduits);
            combinedResults.AddRange(listPackages);

            var returnResult = Json("");
            if (combinedResults == null || combinedResults.Count() == 0)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(combinedResults.Skip((page - 1) * 20).Take(20));
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }

        public IActionResult GetNbPage()
        {
            var result = _productRepository.GetAll().Count() / 20;
            var returnResult = Json("");
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }


        private static IQueryable<Product> GetProductWithInclude(IQueryable<Product> product)
        {
            return product
                .Include(i => i.BoardStyle)
                .Include(j => j.ProductCategory)
                .Include(k => k.ProductShape)
                .Include(l => l.ProductSize);

        }

        private static IQueryable<Package> GetPackagetWithInclude(IQueryable<Package> package)
        {
            return package
                .Include(i => i.lstLienProduitPackage);

        }

    }
}
