﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Product;
using SpekSkates.Admin.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace temp.Controllers
{
    [Route("api/[controller]")]
    public class ApiDetailProduitController : Controller
    {
        IRepository<Product> _productRepository;
        public ApiDetailProduitController(IRepository<Product> productRepository)
        {
            _productRepository = productRepository;
        }
        // GET api/About
        [HttpGet("{name}")]
        public IActionResult GetDetailProduit([FromRoute] string name)
        {

            var returnResult = Json("");
            if (name == null || name == string.Empty)
            {
                returnResult = Json("Bad Request");
                returnResult.StatusCode = 400;
                return BadRequest(returnResult);
            }
            var result = GetProductWithInclude(_productRepository.GetAll()).Where(p => p.Name == name && p.isActive && ! p.isDelete).FirstOrDefault();

            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            result.url = Uri.EscapeUriString(PathSnip.path + result.Name);
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }

        [HttpGet("[action]/{id}")]
        public IActionResult GetProduct([FromRoute] int id)
        {

            var returnResult = Json("");

            var result = _productRepository.Get(id).FirstOrDefault();

            if (result == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            result.url = Uri.EscapeUriString(PathSnip.path + result.Name);
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }

        private static IQueryable<Product> GetProductWithInclude(IQueryable<Product> product)
        {
            return product
                .Include(i => i.BoardStyle)
                .Include(j => j.ProductCategory)
                .Include(k => k.ProductShape)
                .Include(l => l.ProductSize);

        }
    }
}
