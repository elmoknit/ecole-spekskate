﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Base;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SpekSkates.Admin.Web.Utilities;
using SpekSkates.Admin.Web.Modules.CustomBoard;

namespace SpekSkates.Admin.Web.Modules.Product.Controllers
{
    [Produces("application/json")]
    [Route("api/ApiSnip")]
    public class ApiSnipController : Controller
    {
        IRepository<Product> _productRepository;
        IRepository<Modules.CustomBoard.CustomBoard> _customBoardRepository;
        IRepository<Package.Package> _packageRepository;

        public ApiSnipController(IRepository<Product> productRepository, IRepository<Modules.CustomBoard.CustomBoard> customBoardRepository,IRepository<Package.Package> packageRepository)
        {
            _productRepository = productRepository;
            _customBoardRepository = customBoardRepository;
            _packageRepository = packageRepository;
        }
        // GET api/About
        [HttpGet("{name}")]
        public Object GetDetailProduit([FromRoute] string name)
        {
            var returnResult = Json("");
            if (name == null || name == string.Empty)
            {
                returnResult = Json("Bad Request");
                returnResult.StatusCode = 400;
                return BadRequest(returnResult);
            }

            var result =_productRepository.GetAll().Where(p => p.Name == name && p.isActive && !p.isDelete).FirstOrDefault();
            if (result != null && result.isEnPromotion)
            {
                result.Price = result.promotionPrice;
            }
            if (result == null)
            {
                var result2 = _packageRepository.GetAll().Where(p => p.Name == name).FirstOrDefault();

                if (result == null && result2 == null)
                {
                    return "NotFound";
                }

                result2.url = Uri.EscapeUriString(PathSnip.path + result2.Name);
                //JsonResult json = new JsonResult(result);
                return ConvertPackageToSnipItem(result2);

            }

            result.url = Uri.EscapeUriString(PathSnip.path + result.Name);
           // result.Id = result.Id + result.codeProduit;
            //JsonResult json = new JsonResult(result);
            return ConvertProductToSnipItem(result);
        }


        [Route("[action]/{name}")]
        public Object GetPackageJson([FromRoute]string name)
        {
            var returnResult = Json("");
            if (name == null || name == string.Empty)
            {
                returnResult = Json("Bad Request");
                returnResult.StatusCode = 400;
                return BadRequest(returnResult);
            }

            var result = _packageRepository.GetAll().Where(p => p.Name == name).FirstOrDefault();

            if (result == null)
            {
                return "NotFound";
            }

           // result.url = Uri.EscapeUriString(PathSnip.path + "/GetPackageJson/" + result.Name);
            //JsonResult json = new JsonResult(result);
            return result;
        }

        [Route("[action]/{name}")]
        public Object GetDetailCustom([FromRoute] string name)
        {
            var returnResult = Json("");
            if (name == null || name == string.Empty)
            {
                returnResult = Json("Bad Request");
                returnResult.StatusCode = 400;
                return BadRequest(returnResult);
            }
            var result = _customBoardRepository.GetAll().Where(p => p.Name == name).FirstOrDefault();

            if (result == null)
            {
                returnResult = Json("Not found");
            }


            return result;
        }

        [HttpPost]
        [Route("[action]")]
        public Object Custom(decimal price, string desc, int boardId, int gripTapeId, int truckId, int truck2Id, int wheelId,int wheel2Id, int wheel3Id, int wheel4Id, int beiringId)
        {        

        var customBoard = new Modules.CustomBoard.CustomBoard { Price = price, Desc = desc, BoardId= boardId, GripTapeId= gripTapeId, TruckId= truckId, Truck2Id= truck2Id, WheelId= wheelId, Wheel2Id = wheel2Id, Wheel3Id= wheel3Id, Wheel4Id= wheel4Id, BeiringId= beiringId };
            _customBoardRepository.Save(customBoard);
            _customBoardRepository.Update(customBoard);
            customBoard.Name = "skate" + customBoard.Id;

            _customBoardRepository.Update(customBoard);

            var returnResult = Json(customBoard);
            returnResult.StatusCode = 200;
            return returnResult;
        }

        private static IQueryable<Product> GetProductWithInclude(IQueryable<Product> product)
        {
            return product
                .Include(i => i.BoardStyle)
                .Include(j => j.ProductCategory)
                .Include(k => k.ProductShape)
                .Include(l => l.ProductSize);

        }

        public class SnipItem
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public decimal Price { get; set; }
            public string url { get; set; }
            public string Description { get; set; }
        }


        private SnipItem ConvertProductToSnipItem(Product obj)
        {
            return new SnipItem { Id = obj.Id + "product", Name = obj.Name, Price = obj.Price, url = obj.url, Description = obj.Description };
        }

        private SnipItem ConvertPackageToSnipItem(Package.Package obj)
        {
            return new SnipItem { Id = obj.Id + "package", Name = obj.Name, Price = obj.Price, url = obj.url, Description = obj.Description };
        }
    }
}