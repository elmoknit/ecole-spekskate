﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SpekSkates.Admin.Web;
using SpekSkates.Admin.Web.Models;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.AzureBlob;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using SpekSkates.Admin.Web.Modules.Product.ViewModels;

namespace SpekSkates.Admin.Web.Modules.Product
{
    [Authorize]
    public class ProductController : Controller
    {
        private IRepository<Product> _itemRepository;
        private readonly IAzureBlobStorage _blobStorage;
        private IRepository<ProductCategory.ProductCategory> _productCategoryRepository;
        private IRepository<ProductSize.ProductSize> _productSizeRepository;
        private IRepository<ProductShape.ProductShape> _productShapeRepository;
        private IRepository<BoardStyle.BoardStyle> _boardStyleRepository;
        private IRepository<Package.Package> _packageRepository;
        private IRepository<HomePage.HomePage> _homePageRepository;

        public ProductController(IRepository<Product> productRepository, IRepository<ProductCategory.ProductCategory> productCategoryRepository, 
            IRepository<ProductSize.ProductSize> productSizeRepository, IRepository<ProductShape.ProductShape> productShapeRepository, 
            IRepository<BoardStyle.BoardStyle> boardStyleRepository, IAzureBlobStorage blobStorage, IRepository<Package.Package> packageRepository,
            IRepository<HomePage.HomePage> homePageRepository = null)
        {
            _itemRepository = productRepository;
            _productCategoryRepository = productCategoryRepository;
            _productSizeRepository = productSizeRepository;
            _blobStorage = blobStorage;
            _productShapeRepository = productShapeRepository;
            _boardStyleRepository = boardStyleRepository;
            _packageRepository = packageRepository;
            if (homePageRepository != null)
            {
                _homePageRepository = homePageRepository;
            }
        }


        // GET: Product
        public IActionResult Index()
        {
            return View("~/views/product/index.cshtml", _itemRepository.GetAll().Where(s => !s.isDelete).ToList());
        }


        // GET: Product/Create
        public IActionResult Create()
        {
            var mv = new ViewModelDetail
            {
                Product = new Product
                {
                    isDelete = false,
                    isActive = true,

                }
            };
            prepMultiSelect(mv);
            return View(mv);
        }

        // POST: Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ViewModelDetail mv)
        {
            if (string.IsNullOrEmpty(mv.SizeSelectId))
            {
                mv.ErrorMessageSize = "La taille est obligatoire";
            }
            else
            {
                mv.Product.ProductSizeId = int.Parse(mv.SizeSelectId.Split('_')[1]);
            }


            if (string.IsNullOrEmpty(mv.ShapeSelectId))
            {
                mv.ErrorMessageShape = "La forme est obligatoire";
            }
            else
            {
                mv.Product.ProductShapeId = int.Parse(mv.ShapeSelectId.Split('_')[1]);
            }


            if (_productCategoryRepository.Get(mv.Product.ProductCategoryId).FirstOrDefault().CategoryType == ProductCategory.CategoryType.Board)
            {
                if (string.IsNullOrEmpty(mv.BoardSelectId))
                {

                    mv.ErrorMessageBoard = "Veuillez choisir le style de planche";
                    prepMultiSelect(mv);
                    return View(mv);
                }
                else
                {
                    var board = _boardStyleRepository.Get(int.Parse(mv.BoardSelectId)).FirstOrDefault();
                    mv.Product.BoardStyleId = int.Parse(mv.BoardSelectId);
                    mv.Product.BoardStyleImagePath = board.ImagePath;
                }
            }
            else
            {
                mv.Product.BoardStyleId = null;
            }
            if (ModelState.IsValid && string.IsNullOrEmpty(mv.ErrorMessageSize) && string.IsNullOrEmpty(mv.ErrorMessageShape))
            {
                if (ModelState.IsValid)
                {
                    if (_itemRepository.GetAll().Where(s => s.Name == mv.Product.Name && !s.isDelete).Any())
                    {
                        mv.ErrorMessageNom = "Ce nom de produit existe déjà";
                        prepMultiSelect(mv);
                        return View(mv);
                    }

                    var fileName = "";
                    if (mv.File != null && mv.File.Length != 0)
                    { fileName = mv.File.GetFilename(); }
                    if (UploadFile(mv.File, fileName))
                    {
                        mv.Product.ImagePath = IFromFileExtensions.ImagePath(fileName);
                    }
                    _itemRepository.Update(mv.Product);
                    _itemRepository.Save(mv.Product);
                    return RedirectToAction(nameof(Index));
                }

            }
            prepMultiSelect(mv);
            return View(mv);
        }

        // GET: Product/Edit/5
        public IActionResult Edit(int? id)
        {
            var mv = new ViewModelDetail();
            if (id == null)
            {
                return NotFound();
            }
            mv.Product = _itemRepository.Get(id).FirstOrDefault();

            if (mv.Product == null)
            {
                return NotFound();
            }

            prepMultiSelect(mv);
            return View(mv);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, ViewModelDetail mv)
        {
            if (id != mv.Product.Id)
            {
                return NotFound();
            }
            if (string.IsNullOrEmpty(mv.SizeSelectId))
            {
                mv.ErrorMessageSize = "La taille est obligatoire";
            }
            else
            {
                mv.Product.ProductSizeId = int.Parse(mv.SizeSelectId.Split('_')[1]);
            }


            if (string.IsNullOrEmpty(mv.ShapeSelectId))
            {
                mv.ErrorMessageShape = "La forme est obligatoire";
            }
            else
            {
                mv.Product.ProductShapeId = int.Parse(mv.ShapeSelectId.Split('_')[1]);
            }


            if (_productCategoryRepository.Get(mv.Product.ProductCategoryId).FirstOrDefault().CategoryType == ProductCategory.CategoryType.Board)
            {
                if (string.IsNullOrEmpty(mv.BoardSelectId))
                {

                    mv.ErrorMessageBoard = "Veuillez choisir le style de planche";
                    prepMultiSelect(mv);
                    return View(mv);
                }
                else
                {
                    var board = _boardStyleRepository.Get(int.Parse(mv.BoardSelectId)).FirstOrDefault();
                    mv.Product.BoardStyleId = int.Parse(mv.BoardSelectId);
                    mv.Product.BoardStyleImagePath = board.ImagePath;
                }
            }
            else
            {
                mv.Product.BoardStyleId = null;
            }


            if (ModelState.IsValid && string.IsNullOrEmpty(mv.ErrorMessageSize) && string.IsNullOrEmpty(mv.ErrorMessageShape))
            {
                if (_itemRepository.GetAll().Where(s => s.Name == mv.Product.Name && s.Id != mv.Product.Id && !s.isDelete).Any())
                {
                    mv.ErrorMessageNom = "Ce nom de produit existe déjà";
                    prepMultiSelect(mv);
                    return View(mv);
                }
                var fileName = "";
                if (mv.File != null && mv.File.Length != 0)
                { fileName = mv.File.GetFilename(); }
                if (UploadFile(mv.File, fileName))
                {
                    mv.Product.ImagePath = IFromFileExtensions.ImagePath(fileName);
                }
                try
                {
                    _itemRepository.Update(mv.Product);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(mv.Product.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            prepMultiSelect(mv);
            return View(mv);
        }

        public IActionResult DeletePage(int? id, ViewModelSuppression vms)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = _itemRepository.Get(id).FirstOrDefault();

            if (product == null)
            {
                return NotFound();
            }

            vms.Product = product;
            var listPackage = _packageRepository.GetAll().Where(wu => !wu.isDelete).Include(x=>x.lstLienProduitPackage).ToList(); //.ToList().Contains(page)).ToList();
            foreach (var item in listPackage)
            {
                foreach (var paquet in item.lstLienProduitPackage)
                {
                    if (paquet.Product == product)
                    {
                        if (vms.listPackage == null)
                        {
                            vms.listPackage = new List<Package.Package>();
                        }
                        vms.listPackage.Add(item);
                    }
                }
            }

            var listHomepage = _homePageRepository.GetAll().Include(x => x.HomePageProducts).ToList(); //.ToList().Contains(page)).ToList();
            foreach (var homepage in listHomepage)
            {
                foreach (var productHomePage in homepage.HomePageProducts)
                {
                    if (productHomePage.ProductId == id)
                    {
                        if (vms.listHomepageNames == null)
                        {
                            vms.listHomepageNames = new List<string>();
                        }
                        vms.listHomepageNames.Add(homepage.Name);
                    }
                }
            }

            return View(vms);
        }

        // POST: Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            _itemRepository.Delete(id);

            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(int id)
        {
            return _itemRepository.Get(id) != null;
        }

        public IActionResult Deleteblob(string blobName, int id)
        {
            if (!string.IsNullOrEmpty(blobName))
            {
                var imageName = blobName.Split('/');
                _blobStorage.DeleteAsync(imageName[imageName.Count() - 1]);
                var product = _itemRepository.Get(id).FirstOrDefault();
                if (product == null)
                {
                    return NotFound();
                }
                product.ImagePath = "";
                _itemRepository.Save(product);
                return RedirectToAction("Edit", new { id = product.Id });
            }
            else
            {
                return NotFound();
            }


        }

        private bool UploadFile(IFormFile file, string blobName)
        {
            if (file == null || file.Length == 0)
            { return false; }

            //source : https://github.com/TahirNaushad/Fiver.Azure.Blob/tree/master/Fiver.Azure.Blob.Client
            var fileStream = file.GetFileStream();

            _blobStorage.UploadAsync(blobName, fileStream);
            return true;
        }


        public IActionResult Active(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = _itemRepository.Get(id).FirstOrDefault();
            if (product == null)
            {
                return NotFound();
            }

            product.isActive = true;

            _itemRepository.Update(product);


            return RedirectToAction(nameof(Index));
        }


        public IActionResult UnActive(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = _itemRepository.Get(id).FirstOrDefault();
            if (product == null)
            {
                return NotFound();
            }

            product.isActive = false;

            _itemRepository.Update(product);


            return RedirectToAction(nameof(Index));
        }


        public IActionResult DeleteItem(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = _itemRepository.Get(id).FirstOrDefault();

            if (product == null)
            {
                return NotFound();
            }


            product.isDelete = true;
            suppressionPackage(id);
            removeProductFromHomepages(id);
            _itemRepository.Update(product);


            return RedirectToAction(nameof(Index));
        }

        private void removeProductFromHomepages(int? id)
        {
            if (_homePageRepository != null)
            {
                var homepages = _homePageRepository.GetAll().Include(x => x.HomePageProducts).ThenInclude(y => y.Product).ToList(); //.ToList().Contains(page)).ToList();
                foreach (var homepage in homepages)
                {
                    if (homepage.HomePageProducts != null)
                    {
                        for (int i = 0; i < homepage.HomePageProducts.Count; i++)
                        {
                            var product = homepage.HomePageProducts.ElementAt(i);
                            if (product.ProductId == id)
                            {
                                homepage.HomePageProducts.Remove(product);
                                i--;
                            }
                        }
                        _homePageRepository.Update(homepage);
                    }
                }
            }
        }

        private void prepMultiSelect(ViewModelDetail mv)
        {
            mv.CategorySelectList = new List<DropDownSelect>();
            foreach (var category in _productCategoryRepository.GetAll().Where(s => !s.isDelete).ToList())
            {
                mv.CategorySelectList.Add(new DropDownSelect { Text = category.Name, Value = category.Id.ToString(), Board = category.CategoryType.ToString() });
            }

            mv.SizeSelectList = new List<DropDownSelect>();
            mv.SizeSelectId = string.Concat(mv.Product.ProductCategoryId, "_", mv.Product.ProductSizeId);
            foreach (var size in _productSizeRepository.GetAll().Include(a => a.ProductCategoriesProductSizes).Where(s => !s.isDelete).ToList())
            {
                foreach (var item in size.ProductCategoriesProductSizes)
                {
                    mv.SizeSelectList.Add(new DropDownSelect { Text = size.Name, Value = string.Concat(item.ProductCategoryId, "_", size.Id), CategoryId = item.ProductCategoryId.ToString() });
                }

            }

            mv.ShapeSelectList = new List<DropDownSelect>();
            mv.ShapeSelectId = string.Concat(mv.Product.ProductCategoryId, "_", mv.Product.ProductShapeId);
            foreach (var shape in _productShapeRepository.GetAll().Include(a => a.ProductCategoriesProductShapes).Where(s => !s.isDelete).ToList())
            {
                foreach (var item in shape.ProductCategoriesProductShapes)
                {
                    mv.ShapeSelectList.Add(new DropDownSelect { Text = shape.Name, Value = string.Concat(item.ProductCategoryId, "_", shape.Id), CategoryId = item.ProductCategoryId.ToString() });
                }
            }

            mv.BoardStyleSelectList = new List<SelectListItem>();
            mv.BoardSelectId = mv.Product.BoardStyleId.ToString();
            foreach (var boardStyle in _boardStyleRepository.GetAll().Where(s => !s.isDelete).ToList())
            {
                mv.BoardStyleSelectList.Add(new SelectListItem
                {
                    Text = boardStyle.Name,
                    Value = boardStyle.Id.ToString()
                });
            }


        }

        private void suppressionPackage(int? id)
        {
            var listPackage = _packageRepository.GetAll().Where(Q => !Q.isDelete).Include(x => x.lstLienProduitPackage).ToList(); //.ToList().Contains(page)).ToList();
            foreach (var item in listPackage)
            {
                foreach (var paquet in item.lstLienProduitPackage)
                {
                    if (paquet.ProductId == id)
                    {
                        item.isDelete = true;
                        _packageRepository.Update(item);
                    }
                }
            }
        }
    }
   
}
