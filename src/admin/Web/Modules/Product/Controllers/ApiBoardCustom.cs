﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpekSkates.Admin.Web.Modules.ProductCategory;

namespace temp.Controllers
{
    [Route("api/[controller]")]
    public class ApiBoardCustomController : Controller
    {
        IRepository<Product> _productRepository;
        public ApiBoardCustomController(IRepository<Product> productRepository)
        {
            _productRepository = productRepository;
        }
        // GET api/About
        [Route("[action]")]
        public IActionResult GetListProduct()
        {

            var boards = _productRepository.GetAll().Where(b => b.ProductCategory.CategoryType == CategoryType.Board && !b.isDelete && !b.isNonDisponible && b.isActive).ToList();
            //var trucks = _productRepository.GetAll().Where(t => t.ProductCategory.CategoryType == CategoryType.Truck).ToList();
            //var wheels = _productRepository.GetAll().Where(w => w.ProductCategory.CategoryType == CategoryType.Wheel).ToList();
            //var accessories =_productRepository.GetAll().Where(a => a.ProductCategory.CategoryType == CategoryType.Accessory).ToList();
            //AllProducts allproducts = new AllProducts
            //{
            //    Accessories = accessories,
            //    Boards = boards,
            //    Trucks = trucks,
            //    Wheels = wheels
            //};
            var returnResult = Json("");
            if (boards == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(boards);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }

        [Route("[action]/{id}")]
        public IActionResult GetListProductRelatedToBoardCategory(int id)
        {
            var gripTapes = _productRepository.GetAll().Where(t => t.ProductCategory.CategoryType == CategoryType.GripTape && !t.isDelete && !t.isNonDisponible && t.isActive && (t.ProductCategory.RelatedCategories.Select(s => s.ProductCategory2Id).Contains(id) ||
            t.ProductCategory.RelatedCategories.Select(s => s.ProductCategory1Id).Contains(id))).ToList();
            var trucks = _productRepository.GetAll().Where(t => t.ProductCategory.CategoryType == CategoryType.Truck && !t.isDelete &&  !t.isNonDisponible && t.isActive &&(t.ProductCategory.RelatedCategories.Select(s => s.ProductCategory2Id).Contains(id) ||
            t.ProductCategory.RelatedCategories.Select(s => s.ProductCategory1Id).Contains(id))).ToList();
            var wheels = _productRepository.GetAll().Where(w => w.ProductCategory.CategoryType == CategoryType.Wheel && !w.isDelete && !w.isNonDisponible && w.isActive && (w.ProductCategory.RelatedCategories.Select(s => s.ProductCategory2Id).Contains(id) ||
            w.ProductCategory.RelatedCategories.Select(s => s.ProductCategory1Id).Contains(id))).ToList();
            var accessories = _productRepository.GetAll().Where(a => a.ProductCategory.CategoryType == CategoryType.Accessory && !a.isDelete && !a.isNonDisponible && a.isActive && (a.ProductCategory.RelatedCategories.Select(s => s.ProductCategory2Id).Contains(id) ||
            a.ProductCategory.RelatedCategories.Select(s => s.ProductCategory1Id).Contains(id))).ToList();

            AllProducts allproducts = new AllProducts
            {
                Accessories = accessories,
                Trucks = trucks,
                Wheels = wheels,
                GripTapes = gripTapes
            };
            var returnResult = Json("");
            if (trucks == null || wheels == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(allproducts);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }

        [Route("[action]/{id}")]
        public IActionResult GetListBeiringsRelatedToWheel(int id)
        {
            var beirings = _productRepository.GetAll().Where(t => t.ProductCategory.CategoryType == CategoryType.Beiring && !t.isDelete && !t.isNonDisponible && t.isActive && (t.ProductCategory.RelatedCategories.Select(s => s.ProductCategory2Id).Contains(id) ||
            t.ProductCategory.RelatedCategories.Select(s => s.ProductCategory1Id).Contains(id))).ToList();

            
             var returnResult = Json("");
            if (beirings == null)
            {
                returnResult = Json("Not found");
                returnResult.StatusCode = 404;
                return NotFound(returnResult);
            }
            returnResult = Json(beirings);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }


        private static IQueryable<Product> GetProductWithInclude(IQueryable<Product> product)
        {
            return product
                .Include(i => i.BoardStyle)
                .Include(j => j.ProductCategory)
                .Include(k => k.ProductShape)
                .Include(l => l.ProductSize);

        }
    }

    public class AllProducts
    {
        public List<Product> Trucks { get; set; }
        public List<Product> GripTapes { get; set; }
        public List<Product> Wheels { get; set; }
        public List<Product> Accessories { get; set; }
    }
}
