﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Modules.Product;
using SpekSkates.Admin.Web.Base;
using SpekSkates.Admin.Web.Modules.Package;

namespace temp.Controllers
{
    [Route("api/[controller]")]
    public class ApiProduct2Controller : Controller
    {

        IRepository<Product> _productRepository;
        IRepository<Package> _packageRepository;
        public ApiProduct2Controller(IRepository<Product> productRepository, IRepository<Package> packageRepository)
        {
            _productRepository = productRepository;
            _packageRepository = packageRepository;
        }
        public IActionResult GetNbPage()
        {
            var productCount = _productRepository.GetAll().Count();
            var packageCount = _packageRepository.GetAll().Count();
            var pagesCountProducts = productCount / (double)20;
            var pagesCountPackages = packageCount / (double)20;

            var result = Math.Ceiling(pagesCountProducts + pagesCountPackages);

            var returnResult = Json("");
            returnResult = Json(result);
            returnResult.StatusCode = 200;
            return Ok(returnResult);
        }

    }
}
