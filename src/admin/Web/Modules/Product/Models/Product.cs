﻿using SpekSkates.Admin.Web.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.Product
{

    public class Product : BaseEntity
    {
        [Required(ErrorMessage = "Le nom du produit est obligatoire")]
        [DisplayName("Nom du produit : ")]
        public string Name { get; set; }

 
        [Required(ErrorMessage = "Le prix est obligatoire")]
        [DisplayName("Prix : ")]
        public decimal Price { get; set; }

        [DisplayName("Prix de la promotion : ")]
        public decimal promotionPrice { get; set; }

        [DisplayName("Image du produit : ")]
        public string ImagePath { get; set; }

        [Required(ErrorMessage = "L'histoire du produit est obligatoire")]
        [DisplayName("Histoire du produit : ")]
        public string History { get; set; }

        [Required(ErrorMessage = "La description du produit est obligatoire")]
        [DisplayName("Description du produit : ")]
        public string Description { get; set; }



        [Required(ErrorMessage = "La taille est obligatoire")]
        [DisplayName("Taille du produit : ")]
        public int ProductSizeId { get; set; }

        public ProductSize.ProductSize ProductSize { get; set; }


        [Required(ErrorMessage = "La catégorie de produit est obligatoire")]
        [DisplayName("Catégorie du produit : ")]
        public int? ProductCategoryId { get; set; }
        public ProductCategory.ProductCategory ProductCategory { get; set; }


        [DisplayName("Forme du produit : ")]
        public int? ProductShapeId { get; set; }

        public ProductShape.ProductShape ProductShape { get; set; }

        [DisplayName("Style de planche : ")]
        public int? BoardStyleId { get; set; }
        public string BoardStyleImagePath { get; set; }
        public BoardStyle.BoardStyle  BoardStyle { get; set; }

        [DisplayName("Lien vers le produit : ")]
        public string url { get; set; }

        public virtual ICollection<HomePage.HomePageProduct> HomePageProducts { get; set; }

        public bool isActive { get; set; }

        public bool isDelete { get; set; }

        [DisplayName("Possède un kicktail : ")]
        public bool isKicktail { get; set; }

        [DisplayName("Produit en promotion :")]
        public bool isEnPromotion { get; set; }

        [DisplayName("Produit indisponible :")]
        public bool isNonDisponible { get; set; }

        [Required(ErrorMessage = "Le code est obligatoire")]
        [DisplayName("Code du produit : ")]
        public string codeProduit { get; set; }

    }
}
