﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.Product.ViewModels
{
    public class ViewModelSuppression
    {
        public Product Product { get; set; }
        public List<Package.Package> listPackage {get;set;}
        public List<string> listHomepageNames { get; set; }
    }
}
