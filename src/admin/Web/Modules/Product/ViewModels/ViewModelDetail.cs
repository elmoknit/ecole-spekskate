﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using SpekSkates.Admin.Web.AzureBlob;
using Microsoft.AspNetCore.Mvc.Rendering;
using SpekSkates.Admin.Web.Modules.ProductCategory;

namespace SpekSkates.Admin.Web.Modules.Product
{
    public class ViewModelDetail
    {


        public Product Product { get; set; }

        [DisplayName("Image du produit (Taille suggérée : 350px x 350px) : ")]
        public IFormFile File { get; set; }

        public List<DropDownSelect> CategorySelectList { set; get; }

        [DisplayName("Grandeur du produit : ")]
        public string SizeSelectId { get; set; }
        public List<DropDownSelect> SizeSelectList { set; get; }

        [DisplayName("Forme du produit : ")]
        public string ShapeSelectId { get; set; }
        public List<DropDownSelect> ShapeSelectList { set; get; }

        [DisplayName("Style de planche : ")]
        public string BoardSelectId { get; set; }
        public List<SelectListItem> BoardStyleSelectList { set; get; }


        public string ErrorMessageSize { set; get; }
        public string ErrorMessageShape { set; get; }
        public string ErrorMessageBoard { set; get; }

        public string ErrorMessageNom { get; set; }

    }

    public class DropDownSelect
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public string CategoryId { get; set; }
        public string Board { get; set; }
    }
}
