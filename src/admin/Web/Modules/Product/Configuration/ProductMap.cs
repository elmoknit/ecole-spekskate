﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SpekSkates.Admin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Modules.Product
{
    public class CustomBoardMap
    {

        public CustomBoardMap(EntityTypeBuilder<Product> entityBuilder)
        {
            entityBuilder.HasKey(t => t.Id);
            entityBuilder.Property(e => e.Name).IsRequired().HasMaxLength(100);
            //entityBuilder.Property(e => e.Category).IsRequired();
            entityBuilder.Property(e => e.Price).IsRequired();
            entityBuilder.Property(e => e.Description).IsRequired();
        }
    }
}
