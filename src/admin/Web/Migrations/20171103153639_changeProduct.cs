﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SpekSkates.Admin.Web.Migrations
{
    public partial class changeProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BoardStyleId",
                table: "Product",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "ProductShapeId",
                table: "Product",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<bool>(
                name: "isKicktail",
                table: "Product",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Product_BoardStyleId",
                table: "Product",
                column: "BoardStyleId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_ProductShapeId",
                table: "Product",
                column: "ProductShapeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Product_BoardStyle_BoardStyleId",
                table: "Product",
                column: "BoardStyleId",
                principalTable: "BoardStyle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Product_ProductShape_ProductShapeId",
                table: "Product",
                column: "ProductShapeId",
                principalTable: "ProductShape",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_BoardStyle_BoardStyleId",
                table: "Product");

            migrationBuilder.DropForeignKey(
                name: "FK_Product_ProductShape_ProductShapeId",
                table: "Product");

            migrationBuilder.DropIndex(
                name: "IX_Product_BoardStyleId",
                table: "Product");

            migrationBuilder.DropIndex(
                name: "IX_Product_ProductShapeId",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "BoardStyleId",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "ProductShapeId",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "isKicktail",
                table: "Product");
        }
    }
}
