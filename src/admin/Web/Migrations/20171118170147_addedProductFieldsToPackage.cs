﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SpekSkates.Admin.Web.Migrations
{
    public partial class addedProductFieldsToPackage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Package",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "History",
                table: "Package",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "Package",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "Package",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "RebateOnPrice",
                table: "Package",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "isActive",
                table: "Package",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isEnPromotion",
                table: "Package",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isNonDisponible",
                table: "Package",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Package");

            migrationBuilder.DropColumn(
                name: "History",
                table: "Package");

            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "Package");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "Package");

            migrationBuilder.DropColumn(
                name: "RebateOnPrice",
                table: "Package");

            migrationBuilder.DropColumn(
                name: "isActive",
                table: "Package");

            migrationBuilder.DropColumn(
                name: "isEnPromotion",
                table: "Package");

            migrationBuilder.DropColumn(
                name: "isNonDisponible",
                table: "Package");
        }
    }
}
