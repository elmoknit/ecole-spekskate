﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SpekSkates.Admin.Web.Migrations
{
    public partial class testt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RelationCatCat",
                columns: table => new
                {
                    ProductCategory1Id = table.Column<int>(nullable: false),
                    ProductCategory2Id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RelationCatCat", x => new { x.ProductCategory1Id, x.ProductCategory2Id });
                    table.ForeignKey(
                        name: "FK_RelationCatCat_ProductCategory_ProductCategory1Id",
                        column: x => x.ProductCategory1Id,
                        principalTable: "ProductCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RelationCatCat_ProductCategory_ProductCategory2Id",
                        column: x => x.ProductCategory2Id,
                        principalTable: "ProductCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RelationCatCat_ProductCategory2Id",
                table: "RelationCatCat",
                column: "ProductCategory2Id");
        }
    }
}
