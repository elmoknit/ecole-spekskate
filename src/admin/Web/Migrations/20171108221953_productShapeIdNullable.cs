﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SpekSkates.Admin.Web.Migrations
{
    public partial class productShapeIdNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_ProductShape_ProductShapeId",
                table: "Product");

            migrationBuilder.AlterColumn<int>(
                name: "ProductShapeId",
                table: "Product",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Product_ProductShape_ProductShapeId",
                table: "Product",
                column: "ProductShapeId",
                principalTable: "ProductShape",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_ProductShape_ProductShapeId",
                table: "Product");

            migrationBuilder.AlterColumn<int>(
                name: "ProductShapeId",
                table: "Product",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Product_ProductShape_ProductShapeId",
                table: "Product",
                column: "ProductShapeId",
                principalTable: "ProductShape",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
