﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SpekSkates.Admin.Web.Migrations
{
    public partial class discountFieldInPackage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RebateOnPrice",
                table: "Package");

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountPercentage",
                table: "Package",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiscountPercentage",
                table: "Package");

            migrationBuilder.AddColumn<decimal>(
                name: "RebateOnPrice",
                table: "Package",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
