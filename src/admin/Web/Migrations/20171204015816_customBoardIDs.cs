﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SpekSkates.Admin.Web.Migrations
{
    public partial class customBoardIDs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BeiringId",
                table: "CustomBoard",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BoardId",
                table: "CustomBoard",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "GripTapeId",
                table: "CustomBoard",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Truck2Id",
                table: "CustomBoard",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TruckId",
                table: "CustomBoard",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Wheel2Id",
                table: "CustomBoard",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Wheel3Id",
                table: "CustomBoard",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Wheel4Id",
                table: "CustomBoard",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "WheelId",
                table: "CustomBoard",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BeiringId",
                table: "CustomBoard");

            migrationBuilder.DropColumn(
                name: "BoardId",
                table: "CustomBoard");

            migrationBuilder.DropColumn(
                name: "GripTapeId",
                table: "CustomBoard");

            migrationBuilder.DropColumn(
                name: "Truck2Id",
                table: "CustomBoard");

            migrationBuilder.DropColumn(
                name: "TruckId",
                table: "CustomBoard");

            migrationBuilder.DropColumn(
                name: "Wheel2Id",
                table: "CustomBoard");

            migrationBuilder.DropColumn(
                name: "Wheel3Id",
                table: "CustomBoard");

            migrationBuilder.DropColumn(
                name: "Wheel4Id",
                table: "CustomBoard");

            migrationBuilder.DropColumn(
                name: "WheelId",
                table: "CustomBoard");
        }
    }
}
