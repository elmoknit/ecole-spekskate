﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SpekSkates.Admin.Web.Migrations
{
    public partial class boardstylenullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_BoardStyle_BoardStyleId",
                table: "Product");

            migrationBuilder.AlterColumn<int>(
                name: "BoardStyleId",
                table: "Product",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Product_BoardStyle_BoardStyleId",
                table: "Product",
                column: "BoardStyleId",
                principalTable: "BoardStyle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_BoardStyle_BoardStyleId",
                table: "Product");

            migrationBuilder.AlterColumn<int>(
                name: "BoardStyleId",
                table: "Product",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Product_BoardStyle_BoardStyleId",
                table: "Product",
                column: "BoardStyleId",
                principalTable: "BoardStyle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
