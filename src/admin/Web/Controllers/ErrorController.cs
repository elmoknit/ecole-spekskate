﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Http404()
        {
            Response.StatusCode = 404;
            return View();
        }

        [Route("/Error/{statusCode}")]
        public IActionResult ErrorRoute(string statusCode)
        {
            if (statusCode == "404")
            {
                return View(statusCode);
            }

            return View("~/Views/Errors/Default.cshtml");
        }
    }
}
