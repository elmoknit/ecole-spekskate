﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace temp.Controllers
{
    [Route("api/[controller]")]
    public class AboutController : Controller
    {
        // GET api/About
        [HttpGet]
        public string Get()
        {
            //faire des requetes en linq pour aller chercher les infos 
            // a parler avec l'equipe pour savoir comment gerer la bd
            // mon clavier a pas d'accent alors desole pour les fautes
            return "valeur completement arbitraire";
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
