﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SpekSkates.Admin.Web.Models;
using Microsoft.EntityFrameworkCore;

namespace SpekSkates.Admin.Web.Controllers
{
    public class HomeController : Controller
    {

        private readonly SpekSkatesDbContext _context;

        public HomeController(SpekSkatesDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
                return View("~/views/Utilisateur/index.cshtml",null);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
