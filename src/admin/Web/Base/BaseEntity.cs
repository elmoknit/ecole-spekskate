﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Base
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
