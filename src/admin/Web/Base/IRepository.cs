﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Base
{
    public interface IRepository<T> where T:BaseEntity
    {
        void Save(T item);
        IQueryable<T> GetAll();
        IQueryable<T> Get(int? id);
        void Delete(int id);
        void Update(T item);
    }
}
