﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Base
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {

        private readonly SpekSkatesDbContext _db;
        protected DbSet<T> entities;
        string errorMessage = string.Empty;

        public Repository(SpekSkatesDbContext db)
        {
            _db = db;
            entities = _db.Set<T>();
        }


        private IQueryable<T> Get()
        {
            return entities
                    .AsQueryable();
        }

        public IQueryable<T> Get(int? id)
        {
            return Get().Where(w => w.Id == id).AsQueryable();
        }


        public IQueryable<T> GetAll()
        {
            return Get().AsQueryable();
        }

        public void Update(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            _db.Update(item);
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            var item = Get(id).FirstOrDefault();
            if (item != null)
            {
                _db.Remove(item);
                _db.SaveChanges();
            }
        }

        public void Save(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            
            _db.SaveChanges();
        }


    }
}
