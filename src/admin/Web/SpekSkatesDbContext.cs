﻿using Microsoft.EntityFrameworkCore;

using SpekSkates.Admin.Web.Modules.Page;
using SpekSkates.Admin.Web.Modules.HomePage;
using SpekSkates.Admin.Web.Modules.Slider;
using SpekSkates.Admin.Web.Modules.About;
using SpekSkates.Admin.Web.Modules.Product;
using SpekSkates.Admin.Web.Modules.Contact;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using SpekSkates.Admin.Web.Modules.ProductCategory;
using SpekSkates.Admin.Web.Modules.ProductSize;
using SpekSkates.Admin.Web.Modules.Package;
using SpekSkates.Admin.Web.Modules.Team.Models;
using SpekSkates.Admin.Web.Modules.Board.Models;
using SpekSkates.Admin.Web.Modules.BoardStyle;
using SpekSkates.Admin.Web.Modules.ProductShape;
using SpekSkates.Admin.Web.Modules.History;
using SpekSkates.Admin.Web.Modules.Infolettre.Modele;
using SpekSkates.Admin.Web.Modules.Infolettre.Configuration;
using SpekSkates.Admin.Web.Modules.CustomBoard;

namespace SpekSkates.Admin.Web
{
    public class SpekSkatesDbContext : IdentityDbContext<IdentityUser>
    {

        public SpekSkatesDbContext(DbContextOptions<SpekSkatesDbContext> options) : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            //skate
            base.OnModelCreating(modelBuilder);
    
            //homepage
            new HomePageMap(modelBuilder.Entity<HomePage>());

            new HistoryMap(modelBuilder.Entity<History>());

            new SliderMap(modelBuilder.Entity<Slider>());

            new InfolettreMap(modelBuilder.Entity<UserInfolettre>());

            new Modules.CustomBoard.CustomBoardClientMap(modelBuilder.Entity<CustomBoard>());

            modelBuilder.Entity<HomePageSlider>()
            .HasKey(hs => new { hs.HomePageId, hs.SliderId });

            modelBuilder.Entity<HomePageSlider>()
                .HasOne(hs => hs.HomePage)
                .WithMany(h => h.HomePageSliders)
                .HasForeignKey(hs => hs.HomePageId);

            modelBuilder.Entity<HomePageSlider>()
                .HasOne(hs => hs.Slider)
                .WithMany(s => s.HomePageSliders)
                .HasForeignKey(hs => hs.SliderId);



            modelBuilder.Entity<HomePageProduct>()
            .HasKey(hp => new { hp.HomePageId, hp.ProductId });

            modelBuilder.Entity<HomePageProduct>()
                .HasOne(hp => hp.HomePage)
                .WithMany(h => h.HomePageProducts)
                .HasForeignKey(hs => hs.HomePageId);

            modelBuilder.Entity<HomePageProduct>()
                .HasOne(hp => hp.Product)
                .WithMany(p => p.HomePageProducts)
                .HasForeignKey(hp => hp.ProductId);

            new CustomBoardMap(modelBuilder.Entity<Product>());

            new AboutMap(modelBuilder.Entity<AboutUs>());

            new ContactMap(modelBuilder.Entity<Contact>());

            new ProductCategoryMap(modelBuilder.Entity<ProductCategory>());

            new ProductSizeMap(modelBuilder.Entity<ProductSize>());

            new ProductShapeMap(modelBuilder.Entity<ProductShape>());

            new BoardStyleMap(modelBuilder.Entity<BoardStyle>());

            modelBuilder.Entity<ProductCategoryProductSize>()
           .HasKey(hs => new { hs.ProductSizeId, hs.ProductCategoryId });

            modelBuilder.Entity<ProductCategoryProductSize>()
               .HasOne(hs => hs.ProductSize)
               .WithMany(s => s.ProductCategoriesProductSizes)
               .HasForeignKey(hs => hs.ProductSizeId);
            modelBuilder.Entity<ProductCategoryProductSize>()
              .HasOne(hs => hs.ProductCategory)
              .WithMany(s => s.ProductCategoriesProductSizes)
              .HasForeignKey(hs => hs.ProductCategoryId);

            modelBuilder.Entity<ProductCategoryProductShape>()
           .HasKey(hs => new { hs.ProductShapeId, hs.ProductCategoryId });

            modelBuilder.Entity<ProductCategoryProductShape>()
               .HasOne(hs => hs.ProductShape)
               .WithMany(s => s.ProductCategoriesProductShapes)
               .HasForeignKey(hs => hs.ProductShapeId);
            modelBuilder.Entity<ProductCategoryProductShape>()
              .HasOne(hs => hs.ProductCategory)
              .WithMany(s => s.ProductCategoriesProductShapes)
              .HasForeignKey(hs => hs.ProductCategoryId);

            new PackageMap(modelBuilder.Entity<Package>());

            modelBuilder.Entity<RelationCatCat>()
                .HasKey(RCC => new { RCC.ProductCategory1Id, RCC.ProductCategory2Id });

            modelBuilder.Entity<RelationCatCat>()
                .HasOne(RCC => RCC.ProductCategory1)
                .WithMany(n => n.RelatedCategories)
                .HasForeignKey(hs => hs.ProductCategory1Id)
                .OnDelete(DeleteBehavior.Restrict);

            //modelBuilder.Entity<RelationCatCat>()
            //    .HasOne(RCC => RCC.ProductCategory2)
            //    .WithMany(n => n.RelatedCategories)
            //    .HasForeignKey(hs => hs.ProductCategory2Id);

        }
        public DbSet<History> History { get; set; }

        public DbSet<AboutUs> AboutUs { get; set; }

        public DbSet<Employee> Employee { get; set; }

        public DbSet<Package> Package { get; set; }

        public DbSet<Contact> Contact { get; set; }

        public DbSet<HomePage> HomePage { get; set; }
    
        public DbSet<HomePageProduct> HomePageProduct { get; set; }

        public DbSet<HomePageSlider> HomePageSlider { get; set; }

        public DbSet<Product> Product { get; set; }

        public DbSet<ProductCategory> ProductCategory { get; set; }

        public DbSet<ProductSize> ProductSize { get; set; }
        public DbSet<ProductShape> ProductShape { get; set; }
        public DbSet<BoardStyle> BoardStyle { get; set; }
        public DbSet<Slider> Slider { get; set; }
        public DbSet<SpekSkates.Admin.Web.Modules.Board.Models.Board> Board { get; set; }
        public DbSet<ProductCategoryProductSize> ProductCategoryProductSize { get; set; }
        public DbSet<SpekSkates.Admin.Web.Modules.Package.Models.LienProduitPackage> LienProduitPackage { get; set; }

        public DbSet<ProductCategoryProductShape> ProductCategoryProductShape { get; set; }
    }
}
