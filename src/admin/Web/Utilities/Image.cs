﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.Utilities
{
    public class Image
    {
        public int Id { get; set; }


        public string Title { get; set; }

        public string AltText { get; set; }

        public string Caption { get; set; }

        public string ImageUrl { get; set; }

        public DateTime? createdDate { get; set; }

    }
}
