﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SpekSkates.Admin.Web;
using Microsoft.EntityFrameworkCore;
using SpekSkates.Admin.Web.AzureBlob;
using SpekSkates.Admin.Web.Base;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity;
using SpekSkates.Admin.Web.Modules.Utilisateur.Email;
using SpekSkates.Admin.Web.Seed;
using SpekSkates.Admin.Web.Utilities;

namespace SpekSkates.Admin.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Register EF services
            var connectionString = Configuration.GetConnectionString("Default");
            services.AddDbContext<SpekSkatesDbContext>(options => options.UseSqlServer(connectionString));
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            // Ajoute L'initialiseru d'user 
            services.AddScoped<IDbInitializer, DbInitializer>();
            services.AddIdentity<IdentityUser, IdentityRole>()
            .AddEntityFrameworkStores<SpekSkatesDbContext>()
            .AddDefaultTokenProviders();

            services.AddScoped<IAzureBlobStorage>(factory =>
            {
                return new AzureBlobStorage(new AzureBlobSettings(
                    storageAccount: Configuration["Blob_StorageAccount"],
                    storageKey: Configuration["Blob_StorageKey"],
                    containerName: Configuration["Blob_ContainerName"]));
            });

            services.ConfigureApplicationCookie(options => options.LoginPath = "/Utilisateur/login");

            services.AddCors();
            services.AddTransient<IMessageService, FileMessageService>();
            services.AddMvc();
            services.AddAuthentication();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IServiceProvider serviceProvider, IDbInitializer dbInitializer)
        {
            loggerFactory.AddConsole();
            app.UseAuthentication();

            //Problement à modifier si on veut améliorer la page 404 d'un url de style http://localhost:9000/Package/Edit/97f
            app.UseStatusCodePages();

            if (env.IsDevelopment())
            {
                PathSnip.path = "http://d8c4f73d.ngrok.io/api/ApiSnip/";
                app.UseDeveloperExceptionPage();
            }
            else
            {
                PathSnip.path = "http://garneau-arbitrium-func.azurewebsites.net/";
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseCors(config => config
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod());

            app.UseStaticFiles();



            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute("Error",
                "{*url}",
                new { controller = "Error", action = "Http404" });

            });





            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                if (!serviceScope.ServiceProvider.GetService<SpekSkatesDbContext>().AllMigrationsApplied())
                {
                    serviceScope.ServiceProvider.GetService<SpekSkatesDbContext>().Database.Migrate();
                    serviceScope.ServiceProvider.GetService<SpekSkatesDbContext>().EnsureSeeded();
                }
            }

            dbInitializer.Initialize(app);

            app.UseDeveloperExceptionPage();
        }
    }
}
