﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SpekSkates.Admin.Web.AzureBlob
{
    //SOURCE : https://github.com/TahirNaushad/Fiver.Azure.Blob/tree/master/Fiver.Azure.Blob
    public interface IAzureBlobStorage
    {
        Task UploadAsync(string blobName, Stream stream);
        Task DeleteAsync(string blobName);

    }
}
