﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Net.Http.Headers;

namespace SpekSkates.Admin.Web.AzureBlob
{
    //Source :https://github.com/TahirNaushad/Fiver.Azure.Blob/tree/master/Fiver.Azure.Blob.Client
    public static class IFromFileExtensions
    {
        public static string GetFilename(this IFormFile file)
        {
            if (file != null)
            {
                return DateTime.Now.Ticks + ContentDispositionHeaderValue.Parse(
                                file.ContentDisposition).FileName.ToString().Trim('"');
            }
            return "";
        }

        public static Stream GetFileStream(this IFormFile file)
        {
            Stream filestream = new MemoryStream();
            file.CopyToAsync(filestream);
            return filestream;
        }

        public static string ImagePath(string blobName)
        {
            return string.Concat("https://garneauarbitrium.blob.core.windows.net/uploads/", blobName);
        }

    }
}
