var clientUrl;

if (process.env.NODE_ENV === 'production') {
    clientUrl = "http://garneau-arbitrium-func.azurewebsites.net";
} else {
    clientUrl = "http://localhost:8999"
}

exports.clientUrl = clientUrl;