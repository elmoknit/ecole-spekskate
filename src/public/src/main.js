// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import VueConfig from 'vue-config';

const configs = {
    baseUrl: 'd8c4f73d.ngrok.io/'
};

import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
Vue.use(VueFormWizard)
Vue.use(BootstrapVue);
Vue.use(VueConfig, configs)

Vue.config.productionTip = false
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/dist/css/bootstrap.css'
import callApi from '@/components/config.js'

import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

Vue.component('my-component', {
    template: `<form id="contact_form" class="txt" v-on:submit="submitForm" action="` + callApi.callApi + `/api/apiContact" method="POST">
      <div class="form-group">
          <label for="title">Titre:</label>
              <input v-model="title" v-on:blur="isValidTitle" class="form-control" name="title" type="text" />
              <p id="ErreurTitre"></p>
      </div>
      <div class="form-group">
          <label for="email">Email</label>
          <input v-model="email" v-on:blur="isValidEmail" class="form-control" name="email" type="email" />
          <p id="ErreurEmail"></p>
      </div>
      <div class="form-group">
          <label for="message">Message <small>(<span>{{ message.length }}</span>)</small></label>
          <textarea v-model="message" v-on:blur="isValidMessage" class="form-control" name="message"></textarea>
          <p id="ErreurMessage"></p>
      </div>
      <button type="submit" class="btn btn-primary right">Envoyer</button>
  </form>
`,
    data() {
        return {
            title: '',
            email: '',
            message: ''
        }
    },
    methods: { // all the actions our app can do
        isValidTitle: function() {
            var valid = this.title.trim().length > 0;
            console.log('checking for a valid title:' + valid);
            return valid;
        },
        isValidEmail: function() {
            var valid = this.email.indexOf('@') > 0 && this.email.trim().length > 3;
            console.log('checking for a valid email: ' + valid);
            return valid;
        },
        isValidMessage: function() {
            var valid = (this.message.trim().length > 0);
            console.log('checking for a valid message: ' + valid);
            return valid;
        },
        submitForm: function(event) {
            if (this.message.trim().length == 0 || this.title.trim().length == 0 || (this.email.indexOf('@') == 0 && this.email.trim().length < 3)) {
                if (this.message.trim().length == 0) {
                    document.getElementById('ErreurMessage').innerText = "Ecrivez plus d'un caractere SVP"
                    event.preventDefault();
                } else {
                    document.getElementById('ErreurMessage').innerText = ""
                }
                if (this.title.trim().length == 0) {
                    document.getElementById('ErreurTitre').innerText = "Ecrivez plus d'un caractere SVP"
                    event.preventDefault();
                } else {
                    document.getElementById('ErreurMessage').innerText = ""
                }
                if (this.email.indexOf('@') < 1 && this.email.trim().length < 3) {
                    document.getElementById('ErreurEmail').innerText = "Ecrivez un courriel sous la forme a@a.a"
                    event.preventDefault();
                } else {
                    document.getElementById('ErreurEmail').innerText = ""
                }

            }
        }
    }
});

Vue.component('infolettre', {
    template: `<form id="contact_form"  v-on:submit="submitForm" action="` + callApi.callApi + `/api/apiInfolettre" method="POST">
          <div class="form-group">
              <label for="title">Nom:</label>
                  <input v-model="title" v-on:blur="isValidTitle" class="form-control" name="title" type="text" />
                  <p id="ErreurTitre"></p>
          </div>
          <div class="form-group">
              <label for="email">Email</label>
              <input v-model="email" v-on:blur="isValidEmail" class="form-control" name="email" type="email" />
              <p id="ErreurEmail"></p>
          </div>

          <button type="submit" class="btn btn-primary">Send</button>
      </form>
    `,
    data() {
        return {
            title: '',
            email: '',
        }
    },
    methods: { // all the actions our app can do
        isValidTitle: function() {
            var valid = this.title.trim().length > 0;
            console.log('checking for a valid title:' + valid);
            return valid;
        },
        isValidEmail: function() {
            var valid = this.email.indexOf('@') > 0 && this.email.trim().length > 3;
            console.log('checking for a valid email: ' + valid);
            return valid;
        },
        submitForm: function(event) {
            if (this.title.trim().length == 0 || (this.email.indexOf('@') == 0 && this.email.trim().length < 3)) {
                if (this.title.trim().length == 0) {
                    document.getElementById('ErreurTitre').innerText = "Ecrivez plus d'un caractere SVP"
                    event.preventDefault();
                } else {
                    document.getElementById('ErreurMessage').innerText = ""
                }
                if (this.email.indexOf('@') < 1 && this.email.trim().length < 3) {
                    document.getElementById('ErreurEmail').innerText = "Ecrivez un courriel sous la forme a@a.a"
                    event.preventDefault();
                } else {
                    document.getElementById('ErreurMessage').innerText = ""
                }

            }
        }
    }
});


/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App }
})

Vue.prototype.$ngrok = process.env.NODE_ENV === 'production' ? "http://garneau-arbitrium.azurewebsites.net" : "http://d8c4f73d.ngrok.io";

Vue.mixin({
    data: function() {
        return {
            get Ngrok() {
                if (process.env.NODE_ENV === 'production') {
                    return "http://garneau-arbitrium.azurewebsites.net";
                } else {
                    return "http://319d33d2.ngrok.io";
                }
            }
        }
    }
})
