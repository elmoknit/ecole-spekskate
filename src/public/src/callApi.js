var callApi;
if (process.env.NODE_ENV === 'production') {
    callApi = "http://garneau-arbitrium.azurewebsites.net";
} else {
    callApi = "http://localhost:9000"
}

exports.callApi = callApi;