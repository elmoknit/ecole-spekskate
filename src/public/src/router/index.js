import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import About from '@/components/About'
import Contact from '@/components/Contact'
import History from '@/components/History'
import listeProduit from '@/components/listeProduit'
import DetailsProduit from '@/components/DetailsProduit'
import DetailsPackage from '@/components/DetailsPackage'
import BoardCustom from '@/components/BoardCustom'
import RechercheAvancee from '@/components/RechercheAvancee'
import infolettre from '@/components/infolettre'
import BoardCustomComplete from '@/components/BoardCustomComplete'


Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/RechercheAvancee',
            name: 'RechercheAvancee',
            component: RechercheAvancee
        },
        {
            path: '/detailsProduit/:name',
            name: 'detailsProduit',
            component: DetailsProduit
        },
        {
            path: '/detailsPackage/:name',
            name: 'detailsPackage',
            component: DetailsPackage
        },
        {
            path: '/boardCustom',
            name: 'boardCustom',
            component: BoardCustom
        },
        {
            path: '/boardCustom/:id',
            name: 'boardCustom',
            component: BoardCustom
        },
        {
            path: '/completeboard/:id',
            name: 'completeboard',
            component: BoardCustomComplete
        },
        {
            path: '/about',
            name: 'About',
            component: About
        },
        {
            path: '/contact',
            name: 'contact',
            component: Contact
        },
        {
            path: '/history',
            name: 'history',
            component: History
        },
        {
            path: '/listeProduit',
            name: 'listeProduit',
            component: listeProduit
        },
        {
            path: '/listeProduit/:recherche',
            name: 'listeProduit',
            component: listeProduit
        },
        {
            path: '/infolettre',
            name: 'infolettre',
            component: infolettre
        },
        {
            path: '/listeProduit/:page',
            name: 'listeProduit',
            component: listeProduit
        },
        {
            path: '/listeProduit/:recherche/:page',
            name: 'listeProduit',
            component: listeProduit
        },
        {
            path: '/home/:name',
            name: 'homeCustom',
            component: Home
        }
    ],
    mode: 'history' //regle un bug ou vue.js rajoute des # a change page :/

})