var callApi;
if (process.env.NODE_ENV === 'production') {
    callApi = "http://garneau-arbitrium.azurewebsites.net";
} else {
    callApi = "http://localhost:9000"
}

exports.callApi = callApi;

var clientUrl;

if (process.env.NODE_ENV === 'production') {
    clientUrl = "http://garneau-arbitrium-func.azurewebsites.net";
} else {
    clientUrl = "http://localhost:8999"
}

exports.clientUrl = clientUrl;