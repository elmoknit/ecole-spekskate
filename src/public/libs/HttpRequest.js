import Settings from 'settings'

const sendRequest = function(method, path, data = null) {
    let xhr = new XMLHttpRequest(),
        url = 'http://' + Settings.hostname + path;
    return new Promise((resolve, reject) => {
        xhr.open(method, url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                let obj = null;
                let value = null;
                try{
                    obj = JSON.parse(xhr.responseText);
                    value = obj.value;
                }
                catch(e) {}
                if (xhr.status.toString()[0] === '2') {
                    resolve(value, obj);
                }
                else {
                    reject(value, obj);
                }
            }
        }
        if(data !== null) {
            xhr.send(JSON.stringify(data));
        }
        else{
            xhr.send(null);
        }
        
    });
};


export const HttpRequest = {
    GET: (path) => {
        return sendRequest('GET', path);
    },
    POST: (path, data = null) => {
        return sendRequest('POST', path, data);
    }
};

